package de.mewel.chess.engine.evaluator;

import de.mewel.chess.common.MaterialCollector;
import de.mewel.chess.common.Move;
import de.mewel.chess.common.PieceUtil;
import de.mewel.chess.common.PositionUtil;
import de.mewel.chess.model.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static de.mewel.chess.model.PieceNotation.*;

public class PiecePositionEvaluator implements PositionEvaluator {

    private static final int[] PIECE_VALUE = {10000, 900, 500, 300, 300, 100, 10000, 900, 500, 300, 300, 100};

    private static final int ENDGAME_TOTAL_VALUE = (PIECE_VALUE[0] * 2) + 1800;

    private static final int ENDGAME_PIECES_THRESHOLD = 4;

    private static final int[] PAWN_EARLY_ADVANCEMENT_MULTIPLIER = {1, 2, 3, 4, 5, 6};

    private static final int[] PAWN_LATE_ADVANCEMENT_MULTIPLIER = {1, 2, 4, 8, 16, 32};

    private static final int[] PAWN_X_POSITION_MODIFIER = {-10, -5, 5, 10, 10, 5, -5, -10};

    private static final int[] PAWN_EARLY_GAME_CENTRAL_Y = {3, 6, 12, 12, 6, 3};

    private static final int[] PAWN_EARLY_GAME_CENTRAL_X = {1, 0, 2, 3, 3, 0, 0, 1};

    private static final int[] KING_EARLY_GAME_X_POSITION = {25, 40, 10, 0, 0, 0, 40, 25};

    private static final int[] KNIGHT_OUTPOST_CENTRAL_MULTIPLIER = {0, 1, 2, 3, 3, 2, 1, 0};

    private static final int[] PIECE_ATTACK_ARRAY = {50, 35, 20, 10, 10, 0, 50, 35, 20, 10, 10, 0};

    private static final int[] PIECE_DEFEND_ARRAY = {0, 10, 20, 40, 40, 10, 0, 10, 20, 40, 40, 10};

    private static final int[] PAWN_DEFEND_ARRAY = {0, 5, 10, 20, 20, 10, 0, 5, 10, 20, 20, 10};

    private static final int[] PINNED_PIECE_VALUE = {0, 350, 150, 80, 80, 15, 0, 350, 150, 80, 80, 15};

    // TODO defend

    @Override
    public int analyze(Position position) {
        int[] data = analyzePosition(position);
        int value = 0;
        for (int y = 0; y < 8; y++) {
            for (int x = 0; x < 8; x++) {
                byte piece = position.get(x, y);
                if (piece == PieceNotation.EMPTY) {
                    continue;
                }
                int pieceValue = data[x + y * 8];
                value += PieceUtil.isWhite(piece) ? pieceValue : -pieceValue;
            }
        }
        return value;
    }

    public int[] analyzePosition(Position position) {
        boolean white = position.isWhite();
        MaterialCollector materialCollector = new MaterialCollector();
        materialCollector.collect(position);
        List<Piece> material = materialCollector.getMaterial();
        boolean endGame = isEndgame(material);

        int[] pieceEvaluation = new int[8 * 8];
        int[] bishopPair = new int[]{-1, -1, -1, -1};

        Map<Piece, Piece> pinnedWhitePieces = new HashMap<>();
        Map<Piece, Piece> pinnedBlackPieces = new HashMap<>();

        for (Piece piece : material) {
            position.setWhite(piece.isWhite());
            switch (piece.toByte()) {
                case 0, 6 -> {
                    King king = (King) piece;
                    king(king, position, pieceEvaluation, endGame);
                    if (piece.toByte() == 0) {
                        pinnedWhitePieces = PositionUtil.pinnedPieces(position, king);
                    } else {
                        pinnedBlackPieces = PositionUtil.pinnedPieces(position, king);
                    }
                }
                case 1, 7 -> queen((Queen) piece, position, pieceEvaluation, endGame);
                case 2, 8 -> rook((Rook) piece, position, pieceEvaluation);
                case 3 -> {
                    bishop((Bishop) piece, position, pieceEvaluation);
                    bishopPair[1] = bishopPair[0] == -1 ? -1 : (piece.x() + piece.y() * 8);
                    bishopPair[0] = bishopPair[0] == -1 ? (piece.x() + piece.y() * 8) : bishopPair[0];
                }
                case 9 -> {
                    bishop((Bishop) piece, position, pieceEvaluation);
                    bishopPair[3] = bishopPair[2] == -1 ? -1 : (piece.x() + piece.y() * 8);
                    bishopPair[2] = bishopPair[2] == -1 ? (piece.x() + piece.y() * 8) : bishopPair[2];
                }
                case 4, 10 -> knight((Knight) piece, position, pieceEvaluation);
                case 5, 11 -> pawn((Pawn) piece, position, pieceEvaluation, endGame);
            }
        }
        // bishop pair
        if (bishopPair[0] != -1 && bishopPair[1] != -1) {
            pieceEvaluation[bishopPair[0]] += 15;
            pieceEvaluation[bishopPair[1]] += 15;
        }
        if (bishopPair[2] != -1 && bishopPair[3] != -1) {
            pieceEvaluation[bishopPair[2]] += 15;
            pieceEvaluation[bishopPair[3]] += 15;
        }
        // pinned pieces
        for (Map.Entry<Piece, Piece> entry : pinnedWhitePieces.entrySet()) {
            Piece pinned = entry.getKey();
            Piece pinnedBy = entry.getValue();
            pieceEvaluation[pinned.x() + pinned.y() * 8] -= PINNED_PIECE_VALUE[pinned.toByte()];
        }
        for (Map.Entry<Piece, Piece> entry : pinnedBlackPieces.entrySet()) {
            Piece pinned = entry.getKey();
            Piece pinnedBy = entry.getValue();
            pieceEvaluation[pinned.x() + pinned.y() * 8] -= PINNED_PIECE_VALUE[pinned.toByte()];
        }

        // reset white
        position.setWhite(white);
        return pieceEvaluation;
    }

    public void king(King king, Position position, int[] pieceEvaluation, boolean endGame) {
        int value = PIECE_VALUE[0];
        int x = king.x();
        int y = king.y();
        boolean white = king.isWhite();
        int offset = white ? 1 : -1;

        // default
        value += captureMoves(king, position);

        if (endGame) {
            // centralization
            if (((x >= 2 && x <= 5) && (y >= 2 && y <= 5))) {
                value += 10;
            }
        } else {
            value += KING_EARLY_GAME_X_POSITION[x];
            value += KingSafetyEvaluatorHelper.evaluate(king, position);
        }
        pieceEvaluation[x + y * 8] += value;
    }

    public void queen(Queen queen, Position position, int[] pieceEvaluation, boolean endGame) {
        int value = PIECE_VALUE[1];
        int x = queen.x();
        int y = queen.y();
        boolean white = queen.isWhite();
        int offset = white ? 1 : -1;

        // default
        value += moves(queen, position, 3, PIECE_ATTACK_ARRAY, PIECE_DEFEND_ARRAY);
        value += preventsCastling(queen, position);

        boolean open = true;
        boolean semiOpen = false;
        boolean possibleKingAttack = false;

        // early game penalty
        if (!endGame) {
            int penaltyMultiplier = Math.max(0, 16 - position.getTurn());
            if (penaltyMultiplier > 0) {
                // apply penalty if not on starting position
                if (queen.x() != 3 || (white ? queen.y() != 0 : queen.y() != 7)) {
                    value += penaltyMultiplier * -5;
                }
            }
        }

        // x
        for (int nx = x + 1; nx < 8; nx++) {
            byte otherPiece = position.get(nx, y);
            if (otherPiece == EMPTY) {
                continue;
            }
            if (otherPiece == (white ? B_KING : W_KING)) {
                possibleKingAttack = true;
            }
        }
        for (int nx = x - 1; nx >= 0; nx--) {
            byte otherPiece = position.get(nx, y);
            if (otherPiece == EMPTY) {
                continue;
            }
            if (otherPiece == (white ? B_KING : W_KING)) {
                possibleKingAttack = true;
            }
        }
        // y
        for (int ny = y + 1; ny < 8; ny++) {
            byte otherPiece = position.get(x, ny);
            if (otherPiece == EMPTY) {
                continue;
            }
            if (white && otherPiece == W_PAWN) {
                open = false;
            } else if (white && otherPiece == B_PAWN && open) {
                open = false;
                semiOpen = true;
            }
            if (otherPiece == (white ? B_KING : W_KING)) {
                possibleKingAttack = true;
            }
        }
        for (int ny = y - 1; ny >= 0; ny--) {
            byte otherPiece = position.get(x, ny);
            if (otherPiece == EMPTY) {
                continue;
            }
            if (!white && otherPiece == B_PAWN) {
                open = false;
            } else if (!white && otherPiece == W_PAWN && open) {
                open = false;
                semiOpen = true;
            }
            if (otherPiece == (white ? B_KING : W_KING)) {
                possibleKingAttack = true;
            }
        }
        value += semiOpen ? 8 : 0;
        value += open ? 15 : 0;
        value += possibleKingAttack ? 15 : 0;

        pieceEvaluation[x + y * 8] += value;
    }

    public void rook(Rook rook, Position position, int[] pieceEvaluation) {
        int value = PIECE_VALUE[2];

        int x = rook.x();
        int y = rook.y();
        boolean white = rook.isWhite();
        int offset = white ? 1 : -1;

        // default
        value += moves(rook, position, 3, PIECE_ATTACK_ARRAY, PIECE_DEFEND_ARRAY);
        value += preventsCastling(rook, position);

        boolean pieceInTheWayForConnectedRooks = false;
        boolean connected = false;

        boolean doubleRook = false;
        boolean semiOpen = false;
        boolean open = true;
        boolean possibleQueenAttack = false;
        boolean possibleKingAttack = false;

        // connected
        for (int nx = x + 1; nx < 8; nx++) {
            byte otherPiece = position.get(nx, y);
            if (otherPiece == EMPTY) {
                continue;
            }
            if (otherPiece == rook.toByte() && !pieceInTheWayForConnectedRooks) {
                connected = true;
            }
            pieceInTheWayForConnectedRooks = true;
            if (otherPiece == (white ? B_QUEEN : W_QUEEN)) {
                possibleQueenAttack = true;
            }
            if (otherPiece == (white ? B_KING : W_KING)) {
                possibleKingAttack = true;
            }
        }
        pieceInTheWayForConnectedRooks = false;
        for (int nx = x - 1; nx >= 0; nx--) {
            byte otherPiece = position.get(nx, y);
            if (otherPiece == EMPTY) {
                continue;
            }
            if (otherPiece == rook.toByte() && !pieceInTheWayForConnectedRooks) {
                connected = true;
            }
            pieceInTheWayForConnectedRooks = true;
            if (otherPiece == (white ? B_QUEEN : W_QUEEN)) {
                possibleQueenAttack = true;
            }
            if (otherPiece == (white ? B_KING : W_KING)) {
                possibleKingAttack = true;
            }
        }
        // open line
        pieceInTheWayForConnectedRooks = false;
        for (int ny = y + 1; ny < 8; ny++) {
            byte otherPiece = position.get(x, ny);
            if (otherPiece == EMPTY) {
                continue;
            }
            if (white && otherPiece == W_ROOK && !pieceInTheWayForConnectedRooks) {
                doubleRook = true;
            }
            pieceInTheWayForConnectedRooks = true;
            if (white && otherPiece == W_PAWN) {
                open = false;
            } else if (white && otherPiece == B_PAWN && open) {
                open = false;
                semiOpen = true;
            }
            if (otherPiece == (white ? B_QUEEN : W_QUEEN)) {
                possibleQueenAttack = true;
            }
            if (otherPiece == (white ? B_KING : W_KING)) {
                possibleKingAttack = true;
            }
        }
        pieceInTheWayForConnectedRooks = false;
        for (int ny = y - 1; ny >= 0; ny--) {
            byte otherPiece = position.get(x, ny);
            if (otherPiece == EMPTY) {
                continue;
            }
            if (!white && otherPiece == B_ROOK && !pieceInTheWayForConnectedRooks) {
                doubleRook = true;
            }
            pieceInTheWayForConnectedRooks = true;
            if (!white && otherPiece == B_PAWN) {
                open = false;
            } else if (!white && otherPiece == W_PAWN && open) {
                open = false;
                semiOpen = true;
            }
            if (otherPiece == (white ? B_QUEEN : W_QUEEN)) {
                possibleQueenAttack = true;
            }
            if (otherPiece == (white ? B_KING : W_KING)) {
                possibleKingAttack = true;
            }
        }
        value += connected ? 15 : 0;
        value += semiOpen ? 8 : 0;
        value += open ? 15 : 0;
        value += doubleRook ? 15 : 0;
        value += possibleQueenAttack ? 15 : 0;
        value += possibleKingAttack ? 15 : 0;

        pieceEvaluation[x + y * 8] += value;
    }

    public void bishop(Bishop bishop, Position position, int[] pieceEvaluation) {
        int value = PIECE_VALUE[3];

        int x = bishop.x();
        int y = bishop.y();
        boolean white = bishop.isWhite();
        int offset = white ? 1 : -1;

        // default
        value += moves(bishop, position, 5, PIECE_ATTACK_ARRAY, PIECE_DEFEND_ARRAY);
        value += preventsCastling(bishop, position);

        // center control
        // endgame stronger with no pawn's

        pieceEvaluation[x + y * 8] += value;
    }

    public void knight(Knight knight, Position position, int[] pieceEvaluation) {
        int value = PIECE_VALUE[4];

        int x = knight.x();
        int y = knight.y();
        boolean white = knight.isWhite();
        int offset = white ? 1 : -1;

        // default
        value += moves(knight, position, 7, PIECE_ATTACK_ARRAY, PIECE_DEFEND_ARRAY);
        value += preventsCastling(knight, position);

        // central position
        if ((x >= 2 && x <= 5) && (y >= 2 && y <= 5)) {
            value += 20; // central position bonus
        } else if ((x >= 1 && x <= 6) && (y >= 1 && y <= 6)) {
            value += 10; // semi-central position bonus
        }

        // outpost
        boolean hasOppositeLeftPawn = hasOppositePawn(white, x - 1, y, position);
        boolean hasOppositeRightPawn = hasOppositePawn(white, x + 1, y, position);
        boolean advanced = ((white && y >= 3 && y <= 6) || (!white && y >= 1 && y <= 4));
        boolean protectedByLeftPawn = hasPawn(position, white, x - 1, white ? y - 1 : y + 1);
        boolean protectedByRightPawn = hasPawn(position, white, x + 1, white ? y - 1 : y + 1);
        boolean outpost = !hasOppositeLeftPawn && !hasOppositeRightPawn && advanced && (protectedByLeftPawn || protectedByRightPawn);
        int centralMultiplier = KNIGHT_OUTPOST_CENTRAL_MULTIPLIER[x];
        value += outpost ? centralMultiplier * 20 : 0;

        // endgame activity - less valuable if less pieces
        // better in closed positions
        // coordination with Other Pieces

        pieceEvaluation[x + y * 8] += value;
    }

    public void pawn(Pawn pawn, Position position, int[] pieceEvaluation, boolean endGame) {
        int value = PIECE_VALUE[5];

        int x = pawn.x();
        int y = pawn.y();
        boolean white = pawn.isWhite();
        int offset = white ? 1 : -1;

        // default stuff
        value += captureMoves(pawn, position);
        value += preventsCastling(pawn, position);
        defend(white, x + 1, y + offset, position, pieceEvaluation, PAWN_DEFEND_ARRAY);
        defend(white, x - 1, y + offset, position, pieceEvaluation, PAWN_DEFEND_ARRAY);

        // opposite & pushed pawn
        boolean hasOppositePawn = hasOppositePawn(white, x, y, position);
        boolean hasOppositeLeftPawn = hasOppositePawn(white, x - 1, y, position);
        boolean hasOppositeRightPawn = hasOppositePawn(white, x + 1, y, position);

        int oppositeMultiplier = 1 + (hasOppositePawn ? 0 : 1) + (hasOppositeLeftPawn ? 0 : 1) + (hasOppositeRightPawn ? 0 : 1);
        int advancementMultiplier = endGame ?
                PAWN_LATE_ADVANCEMENT_MULTIPLIER[white ? (y - 1) : (6 - y)] :
                PAWN_EARLY_ADVANCEMENT_MULTIPLIER[white ? (y - 1) : (6 - y)];
        value += oppositeMultiplier * advancementMultiplier * (endGame ? 2 : 1);

        // if blocked
        boolean canAdvance = position.get(x, y + offset) == PieceNotation.EMPTY;
        if (!canAdvance) {
            value -= 15;
        }

        // central control
        if (!endGame) {
            value += PAWN_EARLY_GAME_CENTRAL_Y[white ? (y - 1) : 6 - y] * PAWN_EARLY_GAME_CENTRAL_X[x];
        }
        value += PAWN_X_POSITION_MODIFIER[x];

        // multiple own pawn's in front
        for (int otherY = y + offset; otherY >= 1 && otherY <= 6; otherY += offset) {
            byte otherPiece = position.get(x, otherY);
            if ((white && otherPiece != W_PAWN) || (!white && otherPiece != B_PAWN)) {
                continue;
            }
            value += -15;
        }
        pieceEvaluation[x + y * 8] += value;
    }

    private void defend(boolean white, int x, int y, Position position, int[] pieceEvaluation, int[] defendArray) {
        if (!Position.valid(x, y)) {
            return;
        }
        byte otherPiece = position.get(x, y);
        if (otherPiece == EMPTY || white != PieceUtil.isWhite(otherPiece)) {
            return;
        }
        pieceEvaluation[x + y * 8] += defendArray[otherPiece];
    }

    private boolean hasOppositePawn(boolean white, int startX, int startY, Position position) {
        if (startX < 0 || startX > 7) {
            return false;
        }
        int offset = white ? 1 : -1;
        for (int y = startY + offset; y >= 0 && y <= 7; y += offset) {
            byte otherPiece = position.get(startX, y);
            if ((white && otherPiece == B_PAWN) || (!white && otherPiece == W_PAWN)) {
                return true;
            }
        }
        return false;
    }

    private int moves(Piece piece, Position position, int moveValue, int[] attackArray, int[] defendArray) {
        int value = 0;
        List<Move> moves = piece.moves(position);
        for (Move move : moves) {
            if (move.getCapturedPiece() == PieceNotation.EMPTY) {
                value += moveValue;
            } else {
                value += attackArray[move.getCapturedPiece()];
            }
        }
        return value;
    }

    /*private int moves(Knight knight, Position position, int moveValue, int[] attackArray, int[] defendArray) {
        int x = knight.x();
        int y = knight.y();

        for(int i = 0; i < KNIGHT_POSITIONS.length; i++) {
            int posX = x + KNIGHT_POSITIONS[i][0];
            int posY = y + KNIGHT_POSITIONS[i][1];
            position.get(x, y);
        }
    }*/

    private int captureMoves(Piece piece, Position position) {
        int value = 0;
        List<Move> captures = piece.captures(position);
        for (Move move : captures) {
            if (move.getCapturedPiece() == -1) {
                piece.captures(position);
            }
            value += PIECE_ATTACK_ARRAY[move.getCapturedPiece()];
        }
        return value;
    }

    private int preventsCastling(Piece piece, Position position) {
        byte b = piece.preventsCastling(position);
        if (b == 0) {
            return 0;
        } else if (b == 1 || b == 2) {
            return 20;
        } else {
            return 40;
        }
    }

    private boolean hasPawn(Position position, boolean white, int x, int y) {
        if (!Position.valid(x, y)) {
            return false;
        }
        byte piece = position.get(x, y);
        return white ? piece == W_PAWN : piece == B_PAWN;
    }

    private boolean isEndgame(List<Piece> pieces) {
        int totalValue = 0;
        int nonPawnNorKingPiece = 0;

        for (Piece piece : pieces) {
            byte b = piece.toByte();
            totalValue += PIECE_VALUE[b];
            if (b != W_PAWN && b != B_PAWN && b != W_KING && b != B_KING) {
                nonPawnNorKingPiece++;
            }
        }
        return totalValue <= ENDGAME_TOTAL_VALUE || nonPawnNorKingPiece <= ENDGAME_PIECES_THRESHOLD;
    }

}
