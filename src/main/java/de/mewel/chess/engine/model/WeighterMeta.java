package de.mewel.chess.engine.model;

import de.mewel.chess.common.MaterialCollector;
import de.mewel.chess.common.Move;
import de.mewel.chess.model.Piece;
import de.mewel.chess.model.Position;

import java.util.List;

public class WeighterMeta {

    public Position position;

    public MaterialCollector material;

    public Piece piece;

    public byte pieceByte;

    public boolean white;

    public List<Move> moves;

}
