package de.mewel.chess.engine;

import de.mewel.chess.engine.model.PositionSearchResult;

public class PositionSearcherCanceledException extends Exception {

    private final PositionSearchResult result;

    public PositionSearcherCanceledException(PositionSearchResult result) {
        this.result = result;
    }

    public PositionSearchResult getResult() {
        return result;
    }
}
