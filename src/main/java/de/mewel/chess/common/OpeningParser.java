package de.mewel.chess.common;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class OpeningParser {

    public List<Opening> parse(InputStream inputStream) throws IOException {
        List<String> lines = new ArrayList<>();
        try (var reader = new java.io.BufferedReader(new java.io.InputStreamReader(inputStream))) {
            String line;
            while ((line = reader.readLine()) != null) {
                lines.add(line);
            }
        }
        return getOpenings(lines);
    }

    public List<Opening> parse(Path pathToCSV) throws IOException {
        List<String> lines = Files.readAllLines(pathToCSV);
        return getOpenings(lines);
    }

    private static List<Opening> getOpenings(List<String> lines) {
        List<Opening> openings = new ArrayList<>();
        for (int i = 1; i < lines.size(); i++) {
            String line = lines.get(i);
            String[] parts = line.split(",", 2);
            if (parts.length == 2) {
                String name = parts[0].trim();
                List<String> moves = Arrays.asList(parts[1].replaceAll("\"", "").split(",\\s*"));
                openings.add(new Opening(name, moves));
            }
        }
        return openings;
    }

}
