package de.mewel.chess.model;

import de.mewel.chess.common.Move;
import de.mewel.chess.common.MoveExecutor;
import de.mewel.chess.common.PieceUtil;
import de.mewel.chess.common.PositionUtil;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static de.mewel.chess.model.PieceNotation.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PawnTest {

    @Test
    public void moves() {
        Position p = PositionUtil.base();
        Pawn whitePawn = new Pawn(true, 0, 1);
        // base position
        assertEquals("At base position pawn can move two squares", 2, whitePawn.moves(p).size());

        // blocked by piece one field away
        p.set(PieceNotation.B_BISHOP, 0, 3);
        assertEquals("Blocked pawn can move one square", 1, whitePawn.moves(p).size());

        // blocked in front
        p.set(PieceNotation.W_QUEEN, 0, 2);
        assertEquals("Blocked pawn can't move", 0, whitePawn.moves(p).size());

        // already moved pawn can just move one field
        MoveExecutor moveExecutor = new MoveExecutor();
        moveExecutor.forward(p, p.move(7, 1, 7, 2));
        assertEquals("Already moved pawn can just move one square", 1, whitePawn.square(7, 2).moves(p).size());

        // promotion -> always two new positions due queen and knight
        p = PositionUtil.empty();
        p.set(PieceNotation.W_PAWN, 6, 6);
        assertEquals("Promotion should give two new position's", 2, whitePawn.square(6, 6).moves(p).size());

        // capture and promotion
        p = PositionUtil.empty();
        p.set(PieceNotation.W_PAWN, 1, 6).set(PieceNotation.B_QUEEN, 0, 7).set(PieceNotation.B_ROOK, 2, 7);
        List<Move> moves = whitePawn.square(1, 6).moves(p);
        assertEquals("two capture promotions and two normal promotions", 6, moves.size());

        p = PositionUtil.base();
        // one queen to beat
        p.set(PieceNotation.W_QUEEN, 2, 2);
        p.set(PieceNotation.B_ROOK, 3, 2);
        p.set(PieceNotation.B_QUEEN, 4, 2);
        assertEquals("pawn can just beat the black queen", 1, whitePawn.square(3, 1).moves(p).size());
        // two queen's to beat
        p.set(PieceNotation.B_QUEEN, 2, 2);
        assertEquals("pawn can beat both black queen's", 2, whitePawn.moves(p).size());
        // en passant
        p = PositionUtil.empty();
        p.set(PieceNotation.B_PAWN, 2, 4);
        p.set(PieceNotation.B_PAWN, 1, 5);
        p.set(PieceNotation.B_PAWN, 3, 5);
        p.setTwoSquarePawn(2);
        p.set(PieceNotation.W_PAWN, 1, 4);
        p.set(PieceNotation.W_PAWN, 3, 4);
        assertEquals("pawn can beat en passant", 1, whitePawn.square(1, 4).moves(p).size());
        assertEquals("pawn can beat en passant", 1, whitePawn.square(3, 4).moves(p).size());

        // complicated en passant
        p = PositionUtil.empty().set(B_KING, 3, 7).set(B_PAWN, 4, 5).set(W_PAWN, 5, 5).set(W_PAWN, 5, 1).set(W_QUEEN, 7, 3).set(W_KING, 0, 0);
        moveExecutor.forward(p, p.move("f2f4"));
        moves = PositionUtil.list(p);
        // TODO assertEquals(5, moves.size());

        p = PositionUtil.empty().set(B_KING, 3, 3).set(B_PAWN, 4, 3).set(W_PAWN, 5, 1).set(W_QUEEN, 7, 3).set(W_KING, 0, 0);
        moveExecutor.forward(p, p.move("f2f4"));
        moves = PositionUtil.list(p);
        // TODO assertEquals(7, moves.size());
    }

    @Test
    public void quiescenceMoves() {
        Position p = PositionUtil.empty();
        p.set(PieceNotation.B_PAWN, 1, 6);
        p.set(PieceNotation.W_BISHOP, 2, 5);
        p.setWhite(false);
        List<Move> moves = PieceUtil.fromByte(p.get(1, 6), 1, 6).quiescenceMoves(p);
        assertEquals(1, moves.size());

        p.clear().set(B_PAWN, 3, 1).set(W_ROOK, 4, 0);
        moves = PieceUtil.fromByte(p.get(3, 1), 3, 1).quiescenceMoves(p);
        assertEquals(4, moves.size());
    }

    @Test
    public void inCheckMoves() {
        Position p = PositionUtil.empty();
        Pawn pawn = set(p, 3, 1, 0, 2, B_QUEEN, 4, 2);
        assertTrue(pawn.inCheckMoves(p).contains(p.move("d2xe3")));
        assertTrue(pawn.inCheckMoves(p).contains(p.move("d2d3")));

        pawn = set(p, 3, 1, 0, 3, B_QUEEN, 4, 3);
        assertEquals(1, pawn.inCheckMoves(p).size());
        assertTrue(pawn.inCheckMoves(p).contains(p.move("d2d4")));
        p.set(B_PAWN, 3, 2);
        assertEquals(0, pawn.inCheckMoves(p).size());

        pawn = set(p, 3, 1, 1, 5, B_QUEEN, 4, 2);
        assertTrue(pawn.inCheckMoves(p).contains(p.move("d2xe3")));
        assertTrue(pawn.inCheckMoves(p).contains(p.move("d2d4")));
        p.set(W_PAWN, 3, 2);
        assertTrue(pawn.inCheckMoves(p).contains(p.move("d2xe3")));
        assertEquals(1, pawn.inCheckMoves(p).size());

        pawn = set(p, 3, 1, 1, 4, B_QUEEN, 4, 1);
        assertEquals(1, pawn.inCheckMoves(p).size());
        assertTrue(pawn.inCheckMoves(p).contains(p.move("d2d3")));

        pawn = set(p, 3, 1, 4, 4, B_QUEEN, 4, 1);
        assertEquals(0, pawn.inCheckMoves(p).size());

        pawn = set(p, 5, 1, 4, 4, B_QUEEN, 7, 1);
        assertEquals(1, pawn.inCheckMoves(p).size());
        assertTrue(pawn.inCheckMoves(p).contains(p.move("f2f4")));

        pawn = set(p, 5, 1, 4, 4, B_QUEEN, 7, 7);
        assertEquals(0, pawn.inCheckMoves(p).size());

        pawn = set(p, 3, 1, 4, 0, B_QUEEN, 0, 4);
        assertEquals(0, pawn.inCheckMoves(p).size());

        pawn = set(p, 0, 5, 4, 7, B_BISHOP, 2, 5);
        assertEquals(0, pawn.inCheckMoves(p).size());

        // black test
        p.setWhite(false);

        pawn = set(p, 2, 6, 2, 3, W_KNIGHT, 3, 5);
        assertTrue(pawn.inCheckMoves(p).contains(p.move("c7d6")));

        pawn = set(p, 2, 6, 2, 3, W_ROOK, 2, 5);
        assertEquals(0, pawn.inCheckMoves(p).size());
    }

    private Pawn set(Position p, int pawnX, int pawnY, int kingX, int kingY, byte attPiece, int attX, int attY) {
        p.clear();
        setCheckingPiece(p, attPiece, attX, attY);
        p.set(p.isWhite() ? W_KING : B_KING, kingX, kingY);
        return setPawn(p, pawnX, pawnY);
    }

    private Pawn setPawn(Position p, int x, int y) {
        p.set(p.isWhite() ? W_PAWN : B_PAWN, x, y);
        return new Pawn(p.isWhite(), x, y);
    }

    private void setCheckingPiece(Position p, byte piece, int x, int y) {
        p.set(piece, x, y);
        p.setCheckingPieces(Arrays.asList(Objects.requireNonNull(PieceUtil.fromByte(piece, x, y))));
    }

}
