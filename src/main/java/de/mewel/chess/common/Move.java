package de.mewel.chess.common;

import de.mewel.chess.model.PieceNotation;
import de.mewel.chess.model.Position;
import de.mewel.chess.model.PositionMeta;

import java.util.Objects;

public class Move {

    private byte x, y, toX, toY;

    private byte piece;

    private byte capturedPiece;

    private final boolean white;

    /**
     * null = no castle
     * true = short castle
     * false = long castle
     */
    private Boolean castle;

    private byte promote;

    private boolean enPassantCapture;

    private PositionMeta forwardMeta;

    public Move(boolean white, byte x, byte y, byte toX, byte toY) {
        this.white = white;
        this.x = x;
        this.y = y;
        this.toX = toX;
        this.toY = toY;
        this.promote = PieceNotation.EMPTY;
        this.piece = PieceNotation.EMPTY;
        this.capturedPiece = PieceNotation.EMPTY;
        this.enPassantCapture = false;
        this.castle = null;
        this.forwardMeta = null;
    }

    public Move(boolean white, boolean castle) {
        this.white = white;
        this.castle = castle;
        this.promote = PieceNotation.EMPTY;
        this.piece = PieceNotation.EMPTY;
        this.capturedPiece = PieceNotation.EMPTY;
    }

    public Move forward(Position position) {
        this.forwardMeta = position.getMetaCopy();
        return forwardPieces(position);
    }

    public Move forwardPieces(Position position) {
        this.piece = position.get(this.x, this.y);
        if (castle == null) {
            this.capturedPiece = enPassantCapture ? (this.white ? PieceNotation.B_PAWN : PieceNotation.W_PAWN) : position.get(this.toX, this.toY);
        }
        return this;
    }

    public Move clearForwardMeta() {
        this.forwardMeta = null;
        return this;
    }

    public Move backward() {
        this.clearForwardMeta();
        this.piece = PieceNotation.EMPTY;
        this.capturedPiece = PieceNotation.EMPTY;
        return this;
    }

    public Move enPassant() {
        this.enPassantCapture = true;
        return this;
    }

    public Move promote(byte piece) {
        this.promote = piece;
        return this;
    }

    public Move capture(byte piece) {
        this.capturedPiece = piece;
        return this;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getToX() {
        return toX;
    }

    public int getToY() {
        return toY;
    }

    public byte getPiece() {
        return piece;
    }

    public byte getCapturedPiece() {
        return capturedPiece;
    }

    public Boolean getCastle() {
        return castle;
    }

    public byte getPromotingPiece() {
        return promote;
    }

    public PositionMeta getForwardMetaCopy() {
        return forwardMeta.copy();
    }

    public boolean isWhite() {
        return this.white;
    }

    public boolean isBlack() {
        return !this.white;
    }

    public boolean isEnPassantCapture() {
        return enPassantCapture;
    }

    public boolean isPromoting() {
        return promote != PieceNotation.EMPTY;
    }

    public boolean isCastle() {
        return castle != null;
    }

    public boolean isExecuted() {
        return this.forwardMeta != null;
    }

    @Override
    public String toString() {
        if (castle == null) {
            String uci = String.valueOf((char) (x + 97)) + (y + 1) + ((char) (toX + 97)) + (toY + 1);
            if (promote != PieceNotation.EMPTY) {
                uci += (PieceUtil.isKnight(promote)) ? "k" : "q";
            }
            return uci;
        } else {
            return "e" + (white ? 1 : 8) + (castle ? "g" : "c") + (white ? 1 : 8);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Move move = (Move) o;
        return x == move.x &&
                y == move.y &&
                toX == move.toX &&
                toY == move.toY &&
                piece == move.piece &&
                capturedPiece == move.capturedPiece &&
                white == move.white &&
                promote == move.promote &&
                enPassantCapture == move.enPassantCapture &&
                Objects.equals(castle, move.castle) &&
                Objects.equals(forwardMeta, move.forwardMeta);
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y, toX, toY, piece, capturedPiece, white, castle, promote, enPassantCapture, forwardMeta);
    }

    public Move copy() {
        Move copy = new Move(white, x, y, toX, toY);
        copy.promote = promote;
        copy.piece = piece;
        copy.capturedPiece = capturedPiece;
        copy.enPassantCapture = enPassantCapture;
        copy.castle = castle;
        if (forwardMeta != null) {
            copy.forwardMeta = forwardMeta.copy();
        }
        return copy;
    }

}
