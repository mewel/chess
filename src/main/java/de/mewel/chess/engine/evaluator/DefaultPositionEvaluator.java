package de.mewel.chess.engine.evaluator;

import de.mewel.chess.common.MaterialCollector;
import de.mewel.chess.common.Move;
import de.mewel.chess.common.PieceUtil;
import de.mewel.chess.common.PositionUtil;
import de.mewel.chess.engine.model.WeighterMeta;
import de.mewel.chess.model.Piece;
import de.mewel.chess.model.PieceNotation;
import de.mewel.chess.model.Position;

import java.util.List;

public class DefaultPositionEvaluator implements PositionEvaluator {

    @Override
    public int analyze(Position position) {
        boolean white = position.isWhite();
        int weight = 0;

        // collect pieces
        MaterialCollector materialCollector = new MaterialCollector();
        materialCollector.collect(position);

        // meta
        WeighterMeta meta = new WeighterMeta();
        meta.position = position;
        meta.material = materialCollector;

        weight += rook(position, meta.material.getMaterial());

        for (Piece piece : materialCollector.getMaterial()) {
            position.setWhite(piece.isWhite());
            meta.piece = piece;
            meta.white = piece.isWhite();
            meta.pieceByte = piece.toByte();
            meta.moves = piece.moves(position);
            weight += piece(meta) * 100;
            weight += legalMoves(meta) * 2;
            weight += moves(meta);
            weight += development(meta);
            weight += pawn(meta);
            position.setWhite(white);
        }

        position.setWhite(white);
        return weight;
    }

    public int piece(WeighterMeta meta) {
        return meta.white ? meta.piece.getValue() : -meta.piece.getValue();
    }

    public int legalMoves(WeighterMeta meta) {
        if (PieceUtil.isBishop(meta.pieceByte)) {
            int weight = meta.moves.size();
            return meta.white ? weight : -weight;
        } else if (PieceUtil.isKnight(meta.pieceByte)) {
            int weight = meta.moves.size();
            return meta.white ? weight : -weight;
        } else if (PieceUtil.isRook(meta.pieceByte)) {
            int weight = meta.moves.size();
            return meta.white ? weight : -weight;
        } else if (PieceUtil.isQueen(meta.pieceByte)) {
            int weight = meta.moves.size();
            return meta.white ? weight : -weight;
        }
        return 0;
    }

    public int moves(WeighterMeta meta) {
        int weight = 0;
        for (Move move : meta.moves) {
            // special stuff
            if (move.isCastle()) {
                weight += meta.white ? 8 : -8;
            } else if (move.isPromoting()) {
                weight += meta.white ? 8 : -8;
            } else if (move.isEnPassantCapture()) {
                weight += meta.white ? 3 : -3;
            } else {
                byte targetPiece = meta.position.get(move.getToX(), move.getToY());
                if (targetPiece != PieceNotation.EMPTY) {
                    if (PieceUtil.isKing(targetPiece)) {
                        weight += meta.white ? 20 : -20;
                    } else {
                        Piece capture = PieceUtil.fromByte(targetPiece, move.getToX(), move.getToY());
                        weight += meta.white ? capture.getValue() : -capture.getValue();
                    }
                }
            }
        }
        return weight;
    }

    public int development(WeighterMeta meta) {
        // central pawn
        if (PieceUtil.isPawn(meta.pieceByte) && meta.material.getMaterial().size() >= 20) {
            if ((meta.piece.y() == 3 || meta.piece.y() == 4) && (meta.piece.x() == 3 || meta.piece.x() == 4)) {
                return meta.white ? 12 : -12;
            }
        }
        // don't leave bishop and knight on first row
        if (PieceUtil.isBishop(meta.pieceByte) || PieceUtil.isKnight(meta.pieceByte)) {
            if ((meta.white && meta.piece.y() == 0) || (!meta.white && meta.piece.y() == 7)) {
                return meta.white ? -8 : 8;
            }
        }
        // don't play queen in first twelve moves
        if (PieceUtil.isQueen(meta.pieceByte) && meta.position.getTurn() < 12) {
            if (meta.piece.x() == 3 && meta.piece.y() == (meta.white ? 0 : 7)) {
                return Math.max((12 - meta.position.getTurn()), 0) * (meta.white ? 20 : -20);
            }
        }
        return 0;
    }

    public int rook(Position position, List<Piece> material) {
        if (material.size() <= 16) {
            return 0;
        }
        int weight = 0;
        Piece wR1 = null;
        Piece wR2 = null;
        Piece bR1 = null;
        Piece bR2 = null;
        for (Piece p : material) {
            if (PieceUtil.isRook(p.toByte())) {
                if (p.isWhite()) {
                    if (wR1 == null) {
                        wR1 = p;
                    } else {
                        wR2 = p;
                        break;
                    }
                } else {
                    if (bR1 == null) {
                        bR1 = p;
                    } else {
                        bR2 = p;
                        break;
                    }
                }
            }
        }
        // connected - white
        if (wR1 != null && wR2 != null && ((wR1.x() == wR2.x()) || (wR1.y() == wR2.y())) && PositionUtil.freeHVPath(position, wR1.x(), wR1.y(), wR2.x(), wR2.y())) {
            weight += 5;
        }
        // connected - black
        if (bR1 != null && bR2 != null && ((bR1.x() == bR2.x()) || (bR1.y() == bR2.y())) && PositionUtil.freeHVPath(position, bR1.x(), bR1.y(), bR2.x(), bR2.y())) {
            weight += -5;
        }
        weight += openFile(position, wR1);
        weight += openFile(position, wR2);
        weight += openFile(position, bR1);
        weight += openFile(position, bR2);
        return weight;
    }

    protected int openFile(Position position, Piece rook) {
        if (rook == null) {
            return 0;
        }
        boolean white = rook.isWhite();
        boolean hasOwnPawn = false;
        boolean hasOppPawn = false;
        for (int y = 1; y <= 6; y++) {
            byte pieceByte = position.get(rook.x(), y);
            if (PieceUtil.isPawn(pieceByte)) {
                if (PieceUtil.isOpponent(white, pieceByte)) {
                    hasOppPawn = true;
                } else {
                    hasOwnPawn = true;
                    break;
                }
            }
        }
        return hasOwnPawn ? 0 : (hasOppPawn ? 15 : 40) * (white ? 1 : -1);
    }

    protected int pawn(WeighterMeta meta) {
        if (PieceUtil.isPawn(meta.pieceByte)) {
            return 0;
        }
        int weight = 0;
        // pawn progress to promote
        int materialSize = meta.material.getMaterial().size();
        int boost = materialSize <= 8 ? 8 : (materialSize <= 12 ? 4 : (materialSize <= 18 ? 2 : 1));
        if (materialSize <= 24) {
            weight += (meta.white ? meta.piece.y() : (7 - meta.piece.y()) * -1) * boost;
        }
        // double pawn & free pawn
        boolean isFreePawn = true;
        boolean isDoublePawn = false;
        int targetY = meta.white ? 7 : 0;
        int moveY = meta.white ? 1 : -1;
        int x = meta.piece.x();
        if (meta.piece.y() + moveY >= 1 && meta.piece.y() + moveY <= 6) {
            for (int y = meta.piece.y() + moveY; y != targetY; y += moveY) {
                byte yPiece = meta.position.get(x, y);
                byte yLeftPiece = x >= 1 ? meta.position.get(x - 1, y) : PieceNotation.EMPTY;
                byte yRightPiece = x <= 6 ? meta.position.get(x + 1, y) : PieceNotation.EMPTY;
                if (PieceUtil.isPawn(yPiece)) {
                    if (PieceUtil.isOpponent(meta.white, yPiece)) {
                        isFreePawn = false;
                    } else {
                        isDoublePawn = true;
                    }
                }
                if ((PieceUtil.isPawn(yLeftPiece) && PieceUtil.isOpponent(meta.white, yLeftPiece)) ||
                        (PieceUtil.isPawn(yRightPiece) && PieceUtil.isOpponent(meta.white, yRightPiece))) {
                    isFreePawn = false;
                }
            }
            if (isFreePawn) {
                weight += (meta.white ? 1 : -1) * boost;
            }
            if (isDoublePawn) {
                weight -= (meta.white ? 5 : -5);
            }
        }
        // connected pawns - seems to be disadvantage
        /*for (int y = 1; y <= 6; y += 1) {
            if (x - 1 >= 0) {
                byte piece = meta.position.get(x - 1, y);
                if (PieceUtil.isPawn(piece) && !PieceUtil.isOpponent(meta.white, piece)) {
                    weight += meta.white ? 10 : -10;
                }
            }
            if (x + 1 >= 0) {
                byte piece = meta.position.get(x + 1, y);
                if (PieceUtil.isPawn(piece) && !PieceUtil.isOpponent(meta.white, piece)) {
                    weight += meta.white ? 10 : -10;
                }
            }
        }*/
        return weight;
    }

}
