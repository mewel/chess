package de.mewel.chess.engine;

import de.mewel.chess.common.PositionUtil;
import de.mewel.chess.engine.evaluator.DefaultPositionEvaluator;
import de.mewel.chess.model.PieceNotation;
import de.mewel.chess.model.Position;
import org.junit.Assert;
import org.junit.Test;

public class DefaultPositionEvaluatorTest {

    @Test
    public void weight() {
        Position position = PositionUtil.base();
        DefaultPositionEvaluator analyzer = new DefaultPositionEvaluator();

        // base
        int advantage = analyzer.analyze(position);
        Assert.assertEquals(0, advantage);

        // d2-d4 - should give white an advantage
        position.set(PieceNotation.EMPTY, 3, 1).set(PieceNotation.W_PAWN, 3, 3);
        Assert.assertTrue(analyzer.analyze(position) > 0);

        // d7-d5
        position.setWhite(false);
        position.set(PieceNotation.EMPTY, 3, 6).set(PieceNotation.B_PAWN, 3, 4);
        Assert.assertEquals(0, analyzer.analyze(position));
    }

}
