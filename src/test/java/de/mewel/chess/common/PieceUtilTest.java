package de.mewel.chess.common;

import de.mewel.chess.model.PieceNotation;
import de.mewel.chess.model.Position;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class PieceUtilTest {

    @Test
    public void isOpponent() {
        Position p = PositionUtil.base();
        byte whiteTower = p.get(0, 0);
        byte blackTower = p.get(0, 7);
        byte empty = p.get(0, 4);

        // whites turn
        assertFalse(PieceUtil.isOpponent(p.isWhite(), whiteTower));
        assertTrue(PieceUtil.isOpponent(p.isWhite(), blackTower));
        assertFalse(PieceUtil.isOpponent(p.isWhite(), empty));

        // blacks turn
        p.setWhite(false);
        assertTrue(PieceUtil.isOpponent(p.isWhite(), whiteTower));
        assertFalse(PieceUtil.isOpponent(p.isWhite(), blackTower));
        assertFalse(PieceUtil.isOpponent(p.isWhite(), empty));
    }

    @Test
    public void captureDiagonal() {
        Position p = PositionUtil.empty();
        p.set(PieceNotation.W_BISHOP, 3, 3);
        p.set(PieceNotation.B_PAWN, 1, 5);
        p.set(PieceNotation.B_QUEEN, 0, 6);
        p.set(PieceNotation.B_ROOK, 7, 7);
        p.set(PieceNotation.B_BISHOP, 0, 0);
        p.set(PieceNotation.W_PAWN, 5, 1);
        p.set(PieceNotation.W_QUEEN, 6, 0);
        List<Move> moves = PositionUtil.captureDiagonal(p, 3, 3);
        assertEquals(3, moves.size());
    }

    @Test
    public void captureHorizontalAndVertical() {
        Position p = PositionUtil.empty();
        p.set(PieceNotation.W_ROOK, 3, 3);
        p.set(PieceNotation.B_PAWN, 3, 5);
        p.set(PieceNotation.B_QUEEN, 3, 6);
        p.set(PieceNotation.B_ROOK, 7, 3);
        p.set(PieceNotation.B_BISHOP, 0, 3);
        p.set(PieceNotation.W_PAWN, 3, 1);
        p.set(PieceNotation.W_QUEEN, 3, 0);
        List<Move> moves = PositionUtil.captureHorizontalAndVertical(p, 3, 3);
        assertEquals(3, moves.size());
    }

    @Test
    public void fromFEN() {
        Position position = PositionUtil.fen("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1");
        assertTrue(position.isWhite());
        assertEquals(-1, position.getTwoSquarePawn());
        assertEquals(3, position.getCastleWhite());
        assertEquals(3, position.getCastleBlack());
        assertEquals(0, position.getMetaCopy().getFiftyMoveRule());
        assertEquals(0, (int) position.getTurn());

        position = PositionUtil.fen("rnbqkbnr/pppppppp/8/8/3P4/8/PPP1PPPP/RNBQKBNR b KQkq - 0 1");
        assertFalse(position.isWhite());
        assertEquals(-1, position.getTwoSquarePawn());
        assertEquals(3, position.getCastleWhite());
        assertEquals(3, position.getCastleBlack());
        assertEquals(0, position.getMetaCopy().getFiftyMoveRule());
        assertEquals(1, (int) position.getTurn());

        position = PositionUtil.fen("r3brk1/6p1/2p2p1p/1p3R2/p2P4/P1PN4/6PP/4R1K1 b - - 1 26");
        assertFalse(position.isWhite());
        assertEquals(-1, position.getTwoSquarePawn());
        assertEquals(0, position.getCastleWhite());
        assertEquals(0, position.getCastleBlack());
        assertEquals(1, position.getMetaCopy().getFiftyMoveRule());
        assertEquals(51, (int) position.getTurn());

        position = PositionUtil.fen("rnbqkbnr/ppp1pppp/8/3p4/2PP4/8/PP2PPPP/RNBQKBNR b KQkq - 0 2");
        assertFalse(position.isWhite());
        assertEquals(-1, position.getTwoSquarePawn());
        assertEquals(3, position.getCastleWhite());
        assertEquals(3, position.getCastleBlack());
        assertEquals(0, position.getMetaCopy().getFiftyMoveRule());
        assertEquals(3, (int) position.getTurn());

        position = PositionUtil.fen("rnbqkbnr/ppp2ppp/4p3/3p4/2PP4/8/PP2PPPP/RNBQKBNR w KQkq - 0 3");
        assertTrue(position.isWhite());
        assertEquals(-1, position.getTwoSquarePawn());
        assertEquals(3, position.getCastleWhite());
        assertEquals(3, position.getCastleBlack());
        assertEquals(0, position.getMetaCopy().getFiftyMoveRule());
        assertEquals(4, (int) position.getTurn());

        position = PositionUtil.fen("rn1q1rk1/pb2bpp1/2p1p2p/1p1pP3/3P4/1BN2N2/PPP2PPP/R2Q1RK1 w - d6 0 12");
        assertTrue(position.isWhite());
        assertEquals(3, position.getTwoSquarePawn());
        assertEquals(0, position.getCastleWhite());
        assertEquals(0, position.getCastleBlack());
        assertEquals(0, position.getMetaCopy().getFiftyMoveRule());
        assertEquals(22, (int) position.getTurn());

        position = PositionUtil.fen("rnb2rk1/5pp1/2p1p2p/pp6/3P4/2PBPN2/P4PPP/1R2K2R w K - 0 16");
        assertTrue(position.isWhite());
        assertEquals(-1, position.getTwoSquarePawn());
        assertEquals(1, position.getCastleWhite());
        assertEquals(0, position.getCastleBlack());
        assertEquals(0, position.getMetaCopy().getFiftyMoveRule());
        assertEquals(30, (int) position.getTurn());
    }

    @Test
    public void toFEN() {
        Position p = PositionUtil.base();
        assertEquals("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1", PositionUtil.toFEN(p));

        MoveExecutor moveExecutor = new MoveExecutor();
        moveExecutor.forward(p, "e2e4 d7d5 e4e5 f7f5");
        assertEquals("rnbqkbnr/ppp1p1pp/8/3pPp2/8/8/PPPP1PPP/RNBQKBNR w KQkq f6 0 3", PositionUtil.toFEN(p));

        moveExecutor.forward(p, "f1d3 e7e6 g1f3 g7g6 e1g1");
        assertEquals("rnbqkbnr/ppp4p/4p1p1/3pPp2/8/3B1N2/PPPP1PPP/RNBQ1RK1 b kq - 1 5", PositionUtil.toFEN(p));

        moveExecutor.forward(p, "c8d7");
        assertEquals("rn1qkbnr/pppb3p/4p1p1/3pPp2/8/3B1N2/PPPP1PPP/RNBQ1RK1 w kq - 2 6", PositionUtil.toFEN(p));

        moveExecutor.forward(p, "d1e2");
        assertEquals("rn1qkbnr/pppb3p/4p1p1/3pPp2/8/3B1N2/PPPPQPPP/RNB2RK1 b kq - 3 6", PositionUtil.toFEN(p));

        moveExecutor.forward(p, "g8f6 e5f6 h8g8");
        assertEquals("rn1qkbr1/pppb3p/4pPp1/3p1p2/8/3B1N2/PPPPQPPP/RNB2RK1 w q - 1 8", PositionUtil.toFEN(p));
    }

}
