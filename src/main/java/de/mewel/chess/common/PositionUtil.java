package de.mewel.chess.common;

import de.mewel.chess.engine.model.MovePath;
import de.mewel.chess.engine.model.FixedPositionSearchResult;
import de.mewel.chess.model.*;

import java.util.*;

import static de.mewel.chess.model.PieceNotation.*;

public abstract class PositionUtil {

    public static final int[][] DIAGONAL_SPREAD = new int[][]{{1, 1}, {-1, -1}, {-1, 1}, {1, -1}};

    public static final int[][] HV_SPREAD = new int[][]{{1, 0}, {0, 1}, {-1, 0}, {0, -1}};

    /**
     * Creates a new position with an empty board.
     * Its white turn.
     *
     * @return an empty position
     */
    public static Position empty() {
        return new Position();
    }

    /**
     * Creates the base position.
     *
     * @return board with base position.
     */
    public static Position base() {
        Position position = new Position();
        position.base();
        return position;
    }

    public static Position fen(String fen) {
        Position position = new Position();
        position.fen(fen);
        return position;
    }

    public static String toFEN(Position position) {
        StringBuilder fen = new StringBuilder();
        for (int y = 0; y <= 7; y++) {
            int e = 0;
            for (int x = 0; x <= 7; x++) {
                byte pieceByte = position.get(x, 7 - y);
                if (pieceByte == EMPTY) {
                    e++;
                } else {
                    if (e != 0) {
                        fen.append(e);
                        e = 0;
                    }
                    fen.append(PieceUtil.toFEN(pieceByte));
                }
            }
            if (e != 0) {
                fen.append(e);
            }
            fen.append(y != 7 ? "/" : "");
        }
        fen.append(" ").append(position.isWhite() ? "w" : "b").append(" ");
        if (position.getCastleWhite() == 0 && position.getCastleBlack() == 0) {
            fen.append("-");
        } else {
            if (position.getCastleWhite() == 3 || position.getCastleWhite() == 1) {
                fen.append("K");
            }
            if (position.getCastleWhite() == 3 || position.getCastleWhite() == 2) {
                fen.append("Q");
            }
            if (position.getCastleBlack() == 3 || position.getCastleBlack() == 1) {
                fen.append("k");
            }
            if (position.getCastleBlack() == 3 || position.getCastleBlack() == 2) {
                fen.append("q");
            }
        }
        fen.append(" ");
        if (position.getTwoSquarePawn() == -1) {
            fen.append("-");
        } else {
            fen.append((char) (position.getTwoSquarePawn() + 97)).append(position.isWhite() ? "6" : "3");
        }
        fen.append(" ").append(position.getFiftyMoveRule());
        fen.append(" ").append((int) Math.ceil(position.getTurn() / 2.0));
        return fen.toString();
    }

    /**
     * Returns all available move's for the current player.
     *
     * @param position the current position
     * @return list of available positions (this does not include castling!)
     */
    public static List<Move> list(Position position) {
        return listFast(position, false);
    }

    /**
     * List of moves including captures, en passant, promote and checks.
     *
     * @param position the current position
     * @return list of moves
     */
    public static List<Move> listQuiescenceMoves(Position position) {
        return listFast(position, true);
    }

    /**
     * Returns all available move's for the current player. This is a fast moves generator which can
     * return moves which are not valid.
     *
     * @param position            the current position
     * @param onlyQuiescenceMoves true  = quiescence moves - if the king is in check all inCheckMoves are returned
     *                            false = all moves
     * @return list of available positions (this does not include castling!)
     */
    public static List<Move> listFast(Position position, boolean onlyQuiescenceMoves) {
        List<Move> moves = new ArrayList<>();
        boolean white = position.isWhite();
        for (int y = 0; y <= 7; y++) {
            for (int x = 0; x <= 7; x++) {
                byte pieceNumber = position.get(x, y);
                if (pieceNumber == PieceNotation.EMPTY) {
                    continue;
                }
                if (PieceUtil.isOpponent(white, pieceNumber)) {
                    continue;
                }
                Piece piece = PieceUtil.fromByte(pieceNumber, x, y);
                if (piece == null) {
                    throw new ChessException("Cannot get piece " + pieceNumber + " at " + x + "," + y);
                }
                moves.addAll(onlyQuiescenceMoves ? piece.quiescenceMoves(position) : piece.moves(position));
            }
        }
        return moves;
    }

    public static List<Move> listValid(Position position) {
        List<Move> moves = new ArrayList<>();
        King king = getKing(position, position.isWhite());
        Map<Piece, Piece> pinnedPieces = pinnedPieces(position, king);
        boolean white = position.isWhite();
        for (int y = 0; y <= 7; y++) {
            for (int x = 0; x <= 7; x++) {
                byte pieceNumber = position.get(x, y);
                if (pieceNumber == PieceNotation.EMPTY) {
                    continue;
                }
                if (PieceUtil.isOpponent(white, pieceNumber)) {
                    continue;
                }
                Piece piece = PieceUtil.fromByte(pieceNumber, x, y);
                if (piece == null) {
                    throw new ChessException("Cannot get piece " + pieceNumber + " at " + x + "," + y);
                }
                Piece pinningPiece = pinnedPieces.get(piece);
                if (!position.isInCheck()) {
                    if (pinningPiece != null) {
                        moves.addAll(piece.pinnedMoves(position, king, pinningPiece));
                    } else {
                        if (!position.isInCheck()) {
                            moves.addAll(piece.validMoves(position));
                        } else {
                            moves.addAll(piece.inCheckMoves(position));
                        }
                    }
                } else if (pinningPiece == null) {
                    moves.addAll(piece.inCheckMoves(position));
                }
            }
        }
        return moves;
    }

    /**
     * Returns a list of moves for a single piece. Be aware that this is not performant because it calculates the
     * whole position and then filters the moves for the given piece.
     *
     * @param position the position
     * @param piece    the piece
     * @return list of moves of the piece
     */
    public static List<Move> listMovesForPiece(Position position, Piece piece) {
        List<Move> allMoves = listFast(position, false);
        return allMoves.stream()
                .filter(move -> move.getX() == piece.x())
                .filter(move -> move.getY() == piece.y())
                .toList();
    }

    /**
     * Returns a list of horizontal and vertical moves.
     * Used by {@link Queen} and {@link Rook}.
     *
     * @param position
     * @param x
     * @param y
     * @return
     */
    public static List<Move> moveHorizontalAndVertical(Position position, int x, int y) {
        List<Move> moves = new ArrayList<>();
        BitSet dir = new BitSet(4);
        dir.set(0, 4);
        for (int i = 1; i < 8; i++) {
            for (int j = 0; j < 4; j++) {
                if (dir.get(j)) {
                    int toX = x + i * HV_SPREAD[j][0];
                    int toY = y + i * HV_SPREAD[j][1];
                    if (Position.valid(toX, toY)) {
                        if (!move(moves, position, x, y, toX, toY)) {
                            capture(moves, position, x, y, toX, toY);
                            dir.clear(j);
                        }
                    } else {
                        dir.clear(j);
                    }
                }
            }
            if (dir.isEmpty()) {
                break;
            }
        }
        return moves;
    }

    /**
     * Returns a list of diagonal moves.
     * Used by {@link Queen} and {@link Bishop}.
     *
     * @param position
     * @param x
     * @param y
     * @return
     */
    public static List<Move> moveDiagonal(Position position, int x, int y) {
        List<Move> moves = new ArrayList<>();
        BitSet dir = new BitSet(4);
        dir.set(0, 4);
        for (int spreading = 1; spreading <= 7; spreading++) {
            for (int i = 0; i < 4; i++) {
                if (dir.get(i)) {
                    if (!moveDiagonal(position, x, y, moves, spreading * DIAGONAL_SPREAD[i][0], spreading * DIAGONAL_SPREAD[i][1])) {
                        dir.clear(i);
                    }
                }
            }
            if (dir.isEmpty()) {
                break;
            }
        }
        return moves;
    }

    private static boolean moveDiagonal(Position position, int x, int y, List<Move> moves, int spreadingX,
                                        int spreadingY) {
        int toX = x + spreadingX;
        int toY = y + spreadingY;
        if (Position.valid(toX, toY)) {
            if (!move(moves, position, x, y, toX, toY)) {
                capture(moves, position, x, y, toX, toY);
                return false;
            }
        } else {
            return false;
        }
        return true;
    }

    public static List<Move> captureHorizontalAndVertical(Position position, int x, int y) {
        List<Move> moves = new ArrayList<>();
        // x axis
        for (int nx = x + 1; nx <= 7; nx++) {
            if (capture(moves, position, x, y, nx, y) || !position.isEmpty(nx, y)) {
                break;
            }
        }
        for (int nx = x - 1; nx >= 0; nx--) {
            if (capture(moves, position, x, y, nx, y) || !position.isEmpty(nx, y)) {
                break;
            }
        }
        // y axis
        for (int ny = y + 1; ny <= 7; ny++) {
            if (capture(moves, position, x, y, x, ny) || !position.isEmpty(x, ny)) {
                break;
            }
        }
        for (int ny = y - 1; ny >= 0; ny--) {
            if (capture(moves, position, x, y, x, ny) || !position.isEmpty(x, ny)) {
                break;
            }
        }
        return moves;
    }

    public static List<Move> captureDiagonal(Position position, int x, int y) {
        List<Move> moves = new ArrayList<>();
        BitSet dir = new BitSet(4);
        dir.set(0, 4);
        for (int spreading = 1; spreading <= 7; spreading++) {
            for (int i = 0; i < 4; i++) {
                if (dir.get(i)) {
                    if (!captureDiagonal(position, x, y, moves, spreading * DIAGONAL_SPREAD[i][0], spreading * DIAGONAL_SPREAD[i][1])) {
                        dir.clear(i);
                    }
                }
            }
            if (dir.isEmpty()) {
                break;
            }
        }
        return moves;
    }

    private static boolean captureDiagonal(Position position, int x, int y, List<Move> moves, int spreadingX,
                                           int spreadingY) {
        int toX = x + spreadingX;
        int toY = y + spreadingY;
        if (Position.valid(toX, toY)) {
            return !capture(moves, position, x, y, toX, toY) && position.isEmpty(toX, toY);
        } else {
            return false;
        }
    }

    /**
     * Checks if the king at x,y can castle.
     *
     * @param x    king x position
     * @param y    king y position
     * @param side true = short, false = long
     * @return true when the king can castle
     */
    public static boolean checkCastle(Position position, int x, int y, boolean side) {
        if (!checkCastleFast(position, x, y, side)) {
            return false;
        }
        if (checkCastleSides(position, x, y, side)) {
            return false;
        }
        int deepCastleResult = checkCastleDeep(position);
        return deepCastleResult == 0 || deepCastleResult == (side ? 2 : 1);
    }

    public static boolean checkCastleFast(Position position, int x, int y, boolean side) {
        // check can castle
        byte c = position.isWhite() ? position.getCastleWhite() : position.getCastleBlack();
        if (c == 0 || c == 1 && !side || c == 2 && side) {
            return false;
        }
        if (x != 4 || y != (position.isWhite() ? 0 : 7)) {
            return false;
        }
        // check if king at given position
        byte king = position.get(x, y);
        if (!PieceUtil.isKing(king)) {
            return false;
        }
        // check rook at base position
        byte rook = position.get(side ? 7 : 0, y);
        return PieceUtil.isRook(rook);
    }

    private static boolean checkCastleSides(Position position, int x, int y, boolean side) {
        if (side && !checkCastleShort(position, x, y)) {
            return true;
        }
        return !side && !checkCastleLong(position, x, y);
    }

    /**
     * <p>Checks if opponent prevents castling.</p>
     * <ul>
     * <li>0 = doesn't prevents</li>
     * <li>1 = prevents short castle</li>
     * <li>2 = prevents long castle</li>
     * <li>3 = prevents both short and long castle</li>
     * </ul>
     *
     * @param position the position to check
     * @return byte between 0 and 3
     */
    public static byte checkCastleDeep(Position position) {
        boolean white = position.isWhite();
        byte preventsCastling = 0;
        position.setWhite(!white);
        for (int y = 0; y <= 7; y++) {
            for (int x = 0; x <= 7; x++) {
                byte pieceByte = position.get(x, y);
                if (pieceByte == PieceNotation.EMPTY || PieceUtil.isOpponent(!white, pieceByte)) {
                    continue;
                }
                Piece piece = PieceUtil.fromByte(pieceByte, x, y);
                if (piece == null) {
                    throw new ChessException("Cannot get piece " + pieceByte + " at " + x + "," + y);
                }
                preventsCastling |= piece.preventsCastling(position);
                if (preventsCastling == 3) {
                    position.setWhite(white);
                    return preventsCastling;
                }
            }
        }
        position.setWhite(white);
        return preventsCastling;
    }

    /**
     * Just checks if the squares between king and rook are empty.
     *
     * @param p
     * @param x
     * @param y
     * @return
     */
    protected static boolean checkCastleShort(Position p, int x, int y) {
        int king = p.isWhite() ? PieceNotation.W_KING : B_KING;
        return p.get(x, y) == king && p.isEmpty(x + 1, y) && p.isEmpty(x + 2, y);
    }

    /**
     * Just checks if the squares between king and rook are empty.
     *
     * @param position
     * @param x
     * @param y
     * @return
     */
    protected static boolean checkCastleLong(Position position, int x, int y) {
        int king = position.isWhite() ? PieceNotation.W_KING : B_KING;
        return position.get(x, y) == king && position.isEmpty(x - 1, y) && position.isEmpty(x - 2, y) && position.isEmpty(x - 3, y);
    }

    public static byte checkCastleVertical(Position position, int x, int y) {
        byte castle = 0;
        if (x == 0 || x == 7) {
            return castle;
        }
        boolean white = position.isWhite();
        int direction = white ? 1 : -1;
        for (int yn = y + direction; yn >= 0 && yn <= 7; yn += direction) {
            if ((white && yn == 7) || (!white && yn == 0)) {
                castle |= x == 4 ? 3 : (byte) (x < 4 ? 2 : 1);
            }
            if (castle == 3 || position.get(x, yn) != PieceNotation.EMPTY) {
                return castle;
            }
        }
        return castle;
    }

    public static byte checkCastleDiagonal(Position position, int x, int y) {
        boolean white = position.isWhite();
        int xPlusY = x + y;
        int xMinusY = x - y;
        int xPlusYInverted = x + (7 - y);
        int xMinusYInverted = x - (7 - y);
        boolean shortPath1 = white ? (xPlusYInverted >= 4 && xPlusYInverted <= 6) : (xPlusY >= 4 && xPlusY <= 6); // x+
        boolean shortPath2 = white ? (xMinusYInverted >= 4 && xMinusYInverted <= 6) : (xMinusY >= 4 && xMinusY <= 6); // x-
        boolean inRangeShort = shortPath1 || shortPath2;
        boolean longPath1 = white ? (xPlusYInverted >= 1 && xPlusYInverted <= 4) : (xPlusY >= 1 && xPlusY <= 4); // x+
        boolean longPath2 = white ? (xMinusYInverted >= 1 && xMinusYInverted <= 4) : (xMinusY >= 1 && xMinusY <= 4); // x-
        boolean inRangeLong = longPath1 || longPath2;

        if (!inRangeShort && !inRangeLong) {
            return 0;
        }
        boolean pathFree1 = true;
        boolean pathFree2 = true;
        int spreadingTo = white ? (6 - y) : (y - 1);
        for (int spreading = 1; spreading <= spreadingTo; spreading++) {
            int toY = y + (white ? spreading : -spreading);
            int toX1 = x + spreading;
            int toX2 = x - spreading;
            if (pathFree1 && (toX1 > 7 || position.get(toX1, toY) != PieceNotation.EMPTY)) {
                pathFree1 = false;
            }
            if (pathFree2 && (toX2 < 0 || position.get(toX2, toY) != PieceNotation.EMPTY)) {
                pathFree2 = false;
            }
            if (!pathFree1 && !pathFree2) {
                return 0;
            }
        }
        byte castle = (byte) ((shortPath1 && pathFree1) || (shortPath2 && pathFree2) ? 1 : 0);
        castle |= (byte) ((longPath1 && pathFree1) || (longPath2 && pathFree2) ? 2 : 0);
        return castle;
    }

    public static byte checkCastlePawnOrKing(Position position, int x, int y) {
        if ((position.isWhite() && y == 6) || (!position.isWhite() && y == 1)) {
            return (byte) (x == 3 || x == 4 || x == 5 ? 3 : (x < 3 ? 2 : 1));
        }
        return 0;
    }

    /**
     * Checks if the piece at x,y can do an en passant move.
     *
     * @param x
     * @param y
     * @return true if the piece can do it, otherwise false
     */
    public static boolean checkEnPassant(Position position, int x, int y) {
        // if a two square move was made in previous turn?
        byte toX = position.getTwoSquarePawn();
        if (toX == -1) {
            return false;
        }
        // check y pos
        if (y != (position.isWhite() ? 4 : 3)) {
            return false;
        }
        // get the piece
        byte pawn = position.get(x, y);
        if (!PieceUtil.isPawn(pawn)) {
            return false;
        }
        // next to each other?
        if (x + 1 != toX && x - 1 != toX) {
            return false;
        }
        // get the two square pawn
        byte opponentPawn = position.get(toX, y);
        if (!PieceUtil.isPawn(opponentPawn)) {
            return false;
        }
        // check different color -> different -> en passant is possible
        if (pawn == opponentPawn) {
            return false;
        }
        // check if king is in check after en passant capture
        MoveExecutor moveExecutor = new MoveExecutor();
        Position positionCopy = position.copy();
        moveExecutor.forward(positionCopy, positionCopy.enPassant(x, y, toX));
        List<Piece> checkingPieces = PositionUtil.getCheckingPieces(positionCopy);
        return checkingPieces.isEmpty();
    }

    /**
     * Moves the piece to the given position (this includes capturing of opponent
     * pieces). Returns true when the piece was moved. Returns false when the
     * board position is invalid and the piece couldn't be moved.
     *
     * @param moves
     * @param position
     * @param x
     * @param y
     * @param toX
     * @param toY
     * @return
     */
    public static boolean moveOrCapture(Collection<Move> moves, Position position, int x, int y, int toX, int toY) {
        return move(moves, position, x, y, toX, toY) || capture(moves, position, x, y, toX, toY);
    }

    /**
     * Moves the piece to the given position. Returns true when the piece was moved.
     * Returns false when the board position is invalid or the position is not empty.
     *
     * @param moves
     * @param position
     * @param x
     * @param y
     * @param toX
     * @param toY
     * @return
     */
    public static boolean move(Collection<Move> moves, Position position, int x, int y, int toX, int toY) {
        if (position.isEmpty(toX, toY)) {
            moves.add(position.move(x, y, toX, toY));
            return true;
        }
        return false;
    }

    /**
     * Captures the piece at the given position. Returns true when the piece was captured.
     * Returns false when there is no piece, a piece is not an opponent (it's actually yours)
     * or it's outside the board.
     *
     * @param captures
     * @param position
     * @param x
     * @param y
     * @param toX
     * @param toY
     * @return
     */
    public static boolean capture(Collection<Move> captures, Position position, int x, int y, int toX, int toY) {
        byte piece = position.get(toX, toY);
        if (piece != PieceNotation.EMPTY && PieceUtil.isOpponent(position.isWhite(), piece)) {
            captures.add(position.capture(x, y, toX, toY));
            return true;
        }
        return false;
    }

    public static King getKing(Position position, boolean white) {
        byte king = white ? W_KING : B_KING;
        for (int y = 0; y <= 7; y++) {
            for (int x = 0; x <= 7; x++) {
                byte pieceNumber = position.get(x, y);
                if (pieceNumber != king) {
                    continue;
                }
                return new King(white, x, y);
            }
        }
        return null;
    }

    public static List<Piece> getCheckingPieces(Position position) {
        return getCheckingPieces(position, position.isWhite());
    }

    /**
     * Returns the pieces that gives check to the opposite king.
     *
     * @param position the position
     * @return the pieces in list, if no pieces gives check the list is empty
     */
    public static List<Piece> getCheckingPieces(Position position, boolean white) {
        // get king
        List<Piece> checkingPieces = new ArrayList<>();
        King king = PositionUtil.getKing(position, !white);
        if (king == null) {
            return checkingPieces; // hm... well, its technically not in check ;)
        }
        int kingX = king.x();
        int kingY = king.y();
        // check for evil knights
        for (int[] knightPosition : Knight.KNIGHT_POSITIONS) {
            int toX = kingX + knightPosition[0];
            int toY = kingY + knightPosition[1];
            if (Position.valid(toX, toY)) {
                byte knight = position.get(toX, toY);
                if (PieceUtil.isKnight(knight) && PieceUtil.isOpponent(!white, knight)) {
                    checkingPieces.add(PieceUtil.fromByte(knight, toX, toY));
                    break; // no other knight checks possible
                }
            }
        }
        // check diagonal - queen, bishop and pawn
        BitSet dir = new BitSet(4);
        dir.set(0, 4);
        for (int spreading = 1; spreading <= 7; spreading++) {
            for (int i = 0; i < 4; i++) {
                if (dir.get(i)) {
                    int toX = kingX + spreading * DIAGONAL_SPREAD[i][0];
                    int toY = kingY + spreading * DIAGONAL_SPREAD[i][1];
                    if (!addCheckingPiece(checkingPieces, position, white, kingY, toX, toY, true)) {
                        dir.clear(i);
                    }
                }
            }
            if (dir.isEmpty()) {
                break;
            }
        }
        // check horizontal - queen, rook
        dir.set(0, 4);
        for (int i = 1; i < 8; i++) {
            for (int j = 0; j < 4; j++) {
                if (dir.get(j)) {
                    int toX = kingX + i * HV_SPREAD[j][0];
                    int toY = kingY + i * HV_SPREAD[j][1];
                    if (!addCheckingPiece(checkingPieces, position, white, kingY, toX, toY, false)) {
                        dir.clear(j);
                    }
                }
            }
            if (dir.isEmpty()) {
                break;
            }
        }
        return checkingPieces;
    }

    private static boolean addCheckingPiece(List<Piece> checkingPieces, Position position, boolean white,
                                            int kingY, int x, int y, boolean diagonalOrHV) {
        if (!Position.valid(x, y)) {
            return false;
        }
        byte piece = position.get(x, y);
        if (piece == EMPTY) {
            return true;
        } else if (!PieceUtil.isOpponent(!white, piece)) {
            return false;
        }
        if ((diagonalOrHV && (PieceUtil.isBishop(piece) ||
                PieceUtil.isQueen(piece) ||
                (PieceUtil.isPawn(piece) && ((white && (kingY - y == 1)) || (!white && (y - kingY == 1)))))) ||
                (!diagonalOrHV && (PieceUtil.isRook(piece) || PieceUtil.isQueen(piece)))) {
            checkingPieces.add(PieceUtil.fromByte(piece, x, y));
        }
        return false;
    }

    public static Map<Piece, Piece> pinnedPieces(Position position, King king) {
        Map<Piece, Piece> affectedPieces = new HashMap<>();
        if (king != null) {
            for (int[] spread : DIAGONAL_SPREAD) {
                getPinnedPiece(position, king.x(), king.y(), spread[0], spread[1], true)
                        .ifPresent(pin -> affectedPieces.put(pin.pinned, pin.pinnedBy));
            }
            for (int[] spread : HV_SPREAD) {
                getPinnedPiece(position, king.x(), king.y(), spread[0], spread[1], false)
                        .ifPresent(pin -> affectedPieces.put(pin.pinned, pin.pinnedBy));
            }
        }
        return affectedPieces;
    }

    private static Optional<Pin> getPinnedPiece(Position position, int kingX, int kingY, int moveX, int moveY,
                                                boolean diagonal) {
        Piece pinnedPiece = null;
        int x = kingX;
        int y = kingY;
        while ((x + moveX) >= 0 && (x + moveX) <= 7 && (y + moveY) >= 0 && (y + moveY) <= 7) {
            x += moveX;
            y += moveY;
            byte pieceByte = position.get(x, y);
            if (pieceByte == EMPTY) {
                continue;
            }
            boolean isOpponent = PieceUtil.isOpponent(position.isWhite(), pieceByte);
            if (isOpponent && (
                    (diagonal && (PieceUtil.isBishop(pieceByte) || PieceUtil.isQueen(pieceByte))) ||
                            (!diagonal && (PieceUtil.isRook(pieceByte) || PieceUtil.isQueen(pieceByte))))
            ) {
                if (pinnedPiece == null) {
                    return Optional.empty();
                } else {
                    return Optional.of(new Pin(pinnedPiece, PieceUtil.fromByte(pieceByte, x, y)));
                }
            } else if (isOpponent) {
                return Optional.empty();
            } else {
                if (pinnedPiece != null) {
                    return Optional.empty();
                } else {
                    pinnedPiece = PieceUtil.fromByte(pieceByte, x, y);
                }
            }
        }
        return Optional.empty();
    }

    private record Pin(Piece pinned, Piece pinnedBy) {
    }

    /**
     * Checks if there is a free path between from and target.
     *
     * @param position the position
     * @param fromX    from x exclusive
     * @param fromY    from y exclusive
     * @param targetX  to x exclusive
     * @param targetY  to y exclusive
     */
    public static boolean freeHVPath(Position position, int fromX, int fromY, int targetX, int targetY) {
        if (fromX == targetX && fromY == targetY) {
            // pieces are next to each other
            return true;
        }
        boolean horizontal = fromY == targetY;
        int move = horizontal ? (fromX > targetX ? -1 : 1) : (fromY > targetY ? -1 : 1);
        if (horizontal) {
            for (int x = fromX + move; x != targetX; x += move) {
                if (!position.isEmpty(x, targetY)) {
                    return false;
                }
            }
        } else {
            for (int y = fromY + move; y != targetY; y += move) {
                if (!position.isEmpty(targetX, y)) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean horizontalIntersectAvailable(Piece attackingPiece, Piece defendingPiece, King king) {
        return (defendingPiece.y() > Math.min(king.y(), attackingPiece.y())) && (defendingPiece.y() < Math.max(king.y(), attackingPiece.y()));
    }

    public static boolean verticalIntersectAvailable(Piece attackingPiece, Piece defendingPiece, King king) {
        return (defendingPiece.x() > Math.min(king.x(), attackingPiece.x())) && (defendingPiece.x() < Math.max(king.x(), attackingPiece.x()));
    }

    public static void hvIntersectAndCapture(Position position, Piece piece, Piece checkingPiece, King
            king, List<Move> moves) {
        byte checkingPieceByte = checkingPiece.toByte();
        // check for capture
        if ((piece.y() == checkingPiece.y() && PositionUtil.freeHVPath(position, piece.x(), piece.y(), checkingPiece.x(), checkingPiece.y())) ||
                (piece.x() == checkingPiece.x() && PositionUtil.freeHVPath(position, piece.x(), piece.y(), checkingPiece.x(), checkingPiece.y()))) {
            PositionUtil.capture(moves, position, piece.x(), piece.y(), checkingPiece.x(), checkingPiece.y());
        }
        // get king for intersects
        // horizontal/vertical intersects against rook, bishop and queen
        if (PieceUtil.isQueen(checkingPieceByte) || PieceUtil.isRook(checkingPieceByte) || PieceUtil.isBishop(checkingPieceByte)) {
            boolean diagonal = king.x() + king.y() == checkingPiece.x() + checkingPiece.y();
            if (horizontalIntersectAvailable(checkingPiece, piece, king)) {
                int targetX = king.x() == checkingPiece.x() ? king.x() :
                        (piece.x() + (diagonal ? ((king.x() - piece.x()) + (king.y() - piece.y())) : ((king.x() - piece.x()) - (king.y() - piece.y()))));
                int targetY = piece.y();
                if (freeHVPath(position, piece.x(), piece.y(), targetX, targetY)) {
                    move(moves, position, piece.x(), piece.y(), targetX, targetY);
                }
            }
            if (verticalIntersectAvailable(checkingPiece, piece, king)) {
                int targetX = piece.x();
                int targetY = king.y() == checkingPiece.y() ? king.y() :
                        (piece.y() + (diagonal ? ((king.y() - piece.y()) + (king.x() - piece.x())) : ((king.y() - piece.y()) - (king.x() - piece.x()))));
                if (freeHVPath(position, piece.x(), piece.y(), targetX, targetY)) {
                    move(moves, position, piece.x(), piece.y(), targetX, targetY);
                }
            }
        }
    }

    public static void diagonalIntersectAndCapture(Position position, Piece piece, Piece checkingPiece, King
            king, List<Move> moves) {
        // check right square: light or dark square
        byte checkingPieceByte = checkingPiece.toByte();
        // check for capture
        int xOffsetCheckingPiece = piece.x() > checkingPiece.x() ? -1 : 1;
        int yOffsetCheckingPiece = piece.y() > checkingPiece.y() ? -1 : 1;
        int xDistanceCheckingPiece = Math.abs(piece.x() - checkingPiece.x());
        int yDistanceCheckingPiece = Math.abs(piece.y() - checkingPiece.y());
        if (xDistanceCheckingPiece == yDistanceCheckingPiece && piece.x() + xOffsetCheckingPiece * xDistanceCheckingPiece == checkingPiece.x() &&
                piece.y() + yOffsetCheckingPiece * yDistanceCheckingPiece == checkingPiece.y() &&
                PositionUtil.freeDiagonalPath(position, piece.x(), piece.y(), checkingPiece.x(), checkingPiece.y(), xOffsetCheckingPiece, yOffsetCheckingPiece)
        ) {
            PositionUtil.capture(moves, position, piece.x(), piece.y(), checkingPiece.x(), checkingPiece.y());
        }
        // no intersects for pawns and knights
        if (PieceUtil.isPawn(checkingPieceByte) || PieceUtil.isKnight(checkingPieceByte)) {
            return;
        }
        int xOffsetKing = piece.x() > king.x() ? -1 : 1;
        int yOffsetKing = piece.y() > king.y() ? -1 : 1;
        int xDistanceKing = Math.abs(piece.x() - king.x());
        int yDistanceKing = Math.abs(piece.y() - king.y());

        // check for intersect
        if (king.x() == checkingPiece.x() || king.y() == checkingPiece.y()) {
            int minDistanceAttackingPiece = Math.min(xDistanceCheckingPiece, yDistanceCheckingPiece);
            int minDistanceKing = Math.min(xDistanceKing, yDistanceKing);
            if (king.x() == checkingPiece.x()) {
                intersectVertical(position, moves, piece, checkingPiece, checkingPiece, king, xOffsetCheckingPiece, yOffsetCheckingPiece, minDistanceAttackingPiece);
                intersectVertical(position, moves, piece, king, checkingPiece, king, xOffsetKing, yOffsetKing, minDistanceKing);
            } else {
                intersectHorizontal(position, moves, piece, checkingPiece, checkingPiece, king, xOffsetCheckingPiece, yOffsetCheckingPiece, minDistanceAttackingPiece);
                intersectHorizontal(position, moves, piece, king, checkingPiece, king, xOffsetKing, yOffsetKing, minDistanceKing);
            }
        } else {
            // diagonal
            boolean lightSquare = position.isLightSquare(piece.x(), piece.y());
            boolean checkingPieceLightSquare = position.isLightSquare(checkingPiece.x(), checkingPiece.y());
            if (checkingPieceLightSquare == lightSquare) {
                boolean checkingDirection = checkingPiece.x() + checkingPiece.y() != king.x() + king.y(); // a1 -> h8 diagonal
                int move = checkingDirection ? ((checkingPiece.x() - piece.x()) - (checkingPiece.y() - piece.y())) / 2 : ((checkingPiece.x() - piece.x()) + (checkingPiece.y() - piece.y())) / 2;
                int offset = move > 0 ? 1 : -1;
                int targetX = piece.x() + move;
                int targetY = checkingDirection ? piece.y() - move : piece.y() + move;
                if (PieceUtil.inBetween(targetX, targetY, checkingPiece.x(), checkingPiece.y(), king.x(), king.y()) &&
                        PositionUtil.freeDiagonalPath(position, piece.x(), piece.y(), targetX, targetY, offset, checkingDirection ? -offset : offset)) {
                    PositionUtil.move(moves, position, piece.x(), piece.y(), targetX, targetY);
                }
            }
        }
    }

    private static void intersectHorizontal(Position position, List<Move> moves, Piece piece, Piece
            targetPiece, Piece checkingPiece, King king, int xOffsetCheckingPiece, int yOffsetCheckingPiece, int minDistance) {
        int targetX1 = piece.x() + (piece.x() > targetPiece.x() ? -minDistance : minDistance);
        if (PieceUtil.inBetween(targetX1, checkingPiece.x(), king.x()) && PositionUtil.freeDiagonalPath(position, piece.x(), piece.y(), targetX1, targetPiece.y(), xOffsetCheckingPiece, yOffsetCheckingPiece)) {
            PositionUtil.move(moves, position, piece.x(), piece.y(), targetX1, targetPiece.y());
        }
    }

    private static void intersectVertical(Position position, List<Move> moves, Piece piece, Piece
            targetPiece, Piece checkingPiece, King king, int xOffsetCheckingPiece, int yOffsetCheckingPiece, int minDistance) {
        int targetY1 = piece.y() + (piece.y() > targetPiece.y() ? -minDistance : minDistance);
        if (PieceUtil.inBetween(targetY1, checkingPiece.y(), king.y()) && PositionUtil.freeDiagonalPath(position, piece.x(), piece.y(), targetPiece.x(), targetY1, xOffsetCheckingPiece, yOffsetCheckingPiece)) {
            PositionUtil.move(moves, position, piece.x(), piece.y(), targetPiece.x(), targetY1);
        }
    }

    /**
     * Checks if the defending piece can intersect the check.
     *
     * @param position the position
     * @param fromX    defender x
     * @param fromY    defender y
     * @param targetX  x where to put the defending piece in the end to intersect the check
     * @param targetY  y where to put the defending piece in the end to intersect the check
     */
    public static boolean freeDiagonalPath(Position position, int fromX, int fromY, int targetX, int targetY,
                                           int xOffset, int yOffset) {
        int y = fromY + yOffset;
        for (int x = fromX + xOffset; x != targetX; x += xOffset) {
            if (!position.isEmpty(x, y)) {
                return false;
            }
            y += yOffset;
        }
        return true;
    }

    public static boolean blockedByKing(Position position, int targetX, int targetY, int oppositeKingX,
                                        int oppositeKingY) {
        return Math.abs(targetX - oppositeKingX) <= 1 && Math.abs(targetY - oppositeKingY) <= 1;
    }

    public static void printResult(FixedPositionSearchResult result) {
        for (Map.Entry<MovePath, Integer> entry : result.getMoveMap().entrySet()) {
            System.out.println(entry.getValue() + " - " + entry.getKey());
        }
    }
}
