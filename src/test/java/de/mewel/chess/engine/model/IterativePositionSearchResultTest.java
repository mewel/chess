package de.mewel.chess.engine.model;

import de.mewel.chess.common.PositionUtil;
import de.mewel.chess.engine.*;
import de.mewel.chess.engine.evaluator.PiecePositionEvaluator;
import de.mewel.chess.model.Position;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.util.LinkedList;
import java.util.concurrent.CountDownLatch;

public class IterativePositionSearchResultTest {

    @Test
    @Ignore // TODO doesn't work with mvn
    public void getMovePathWithVariance() throws PositionSearcherCanceledException, InterruptedException {
        PiecePositionEvaluator evaluator = new PiecePositionEvaluator();
        IterativePositionSearcher positionSearcher = new IterativePositionSearcher(evaluator, 15);
        Position position = PositionUtil.fen("rnbqk2r/ppp2ppp/3bpn2/3p4/3P4/P2Q1N2/1PP1PPPP/RNB1KB1R w KQkq - 1 4");
        CountDownLatch latch = new CountDownLatch(1);
        TimedTaskRunner taskRunner = new TimedTaskRunner();
        positionSearcher.search(position, new LinkedList<>());
        positionSearcher.setListener(new IterativePositionSearcher.IterativePositionSearcherListener() {
            @Override
            public void onResult(PositionSearchResult result) {
            }

            @Override
            public void onFinished(IterativePositionSearchResult result) {
                for (int i = 0; i < 100; i++) {
                    MovePath movePath = result.getMovePath(15);
                    Assert.assertTrue(movePath.toString().startsWith("b1c3"));
                }
                latch.countDown();
            }
        });
        taskRunner.start(positionSearcher, 1000);
        latch.await();
        taskRunner.shutdown();
    }

}
