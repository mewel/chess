package de.mewel.chess.common;

import java.util.List;
import java.util.Random;

public class OpeningBook {

    private final List<Opening> openings;
    private final Random random;

    public OpeningBook(List<Opening> openings) {
        this.openings = openings;
        this.random = new Random();
    }

    public List<Opening> getPossibleOpenings(List<String> madeMoves) {
        return openings.stream()
                .filter(opening -> opening.getMoves().size() > madeMoves.size() &&
                        opening.getMoves().subList(0, madeMoves.size()).equals(madeMoves))
                .toList();
    }

    public String getRandomMove(List<String> madeMoves) {
        List<Opening> matchingOpenings = getPossibleOpenings(madeMoves);
        if (matchingOpenings.isEmpty()) {
            return null;
        }
        Opening randomOpening = matchingOpenings.get(random.nextInt(matchingOpenings.size()));
        return randomOpening.getMoves().get(madeMoves.size());
    }

}
