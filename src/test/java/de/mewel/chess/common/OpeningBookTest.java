package de.mewel.chess.common;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class OpeningBookTest {

    @Test
    public void getPossibleOpenings() {
        List<Opening> openings = Arrays.asList(
                new Opening("Ruy Lopez", Arrays.asList("e2e4", "e7e5", "g1f3", "b8c6")),
                new Opening("Italian Game", Arrays.asList("e2e4", "e7e5", "g1f3", "b8c6")),
                new Opening("Sicilian Defense", Arrays.asList("e2e4", "c7c5", "g1f3", "d7d6"))
        );

        OpeningBook table = new OpeningBook(openings);

        // Test a move sequence that matches multiple openings
        List<String> madeMoves = Arrays.asList("e2e4", "e7e5");
        List<Opening> possibleOpenings = table.getPossibleOpenings(madeMoves);
        Assert.assertEquals("The next move should be either g1f3 or c7c5", 2, possibleOpenings.size());

        // Test a move sequence that matches a single opening
        madeMoves = Arrays.asList("e2e4", "c7c5");
        possibleOpenings = table.getPossibleOpenings(madeMoves);
        Assert.assertEquals("The next move should be g1f3", "g1f3", possibleOpenings.get(0).get(2));

        // Test a move sequence that matches no openings
        madeMoves = Arrays.asList("e2e4", "e7e6");
        possibleOpenings = table.getPossibleOpenings(madeMoves);
        Assert.assertEquals("The next move should be null as there are no matching openings", 0, possibleOpenings.size());
    }

    @Test
    public void getRandomMove() {
        List<Opening> openings = Arrays.asList(
                new Opening("Ruy Lopez", Arrays.asList("e2e4", "e7e5", "g1f3", "b8c6")),
                new Opening("Italian Game", Arrays.asList("e2e4", "e7e5", "g1f3", "b8c6")),
                new Opening("Sicilian Defense", Arrays.asList("e2e4", "c7c5", "g1f3", "d7d6"))
        );

        OpeningBook table = new OpeningBook(openings);

        // Test a move sequence that matches multiple openings
        List<String> madeMoves = Arrays.asList("e2e4", "e7e5");
        String nextMove = table.getRandomMove(madeMoves);
        Assert.assertTrue("The next move should be either g1f3 or c7c5", nextMove.equals("g1f3") || nextMove.equals("c7c5"));

        // Test a move sequence that matches a single opening
        madeMoves = Arrays.asList("e2e4", "c7c5");
        nextMove = table.getRandomMove(madeMoves);
        Assert.assertEquals("The next move should be g1f3", "g1f3", nextMove);

        // Test a move sequence that matches no openings
        madeMoves = Arrays.asList("e2e4", "e7e6");
        nextMove = table.getRandomMove(madeMoves);
        Assert.assertNull("The next move should be null as there are no matching openings", nextMove);
    }
}