package de.mewel.chess.engine.model;

import java.util.ArrayList;
import java.util.List;

public class IterativePositionSearchResult extends AbstractPositionSearchResult {

    private final List<FixedPositionSearchResult> results;

    public IterativePositionSearchResult(boolean white, int turn) {
        super(white, turn);
        this.results = new ArrayList<>();
    }

    public void add(FixedPositionSearchResult result) {
        this.results.add(result);
    }

    @Override
    public MovePath getBestMovePath() {
        // empty we have no move path
        if (results.isEmpty()) {
            return null;
        }
        // only one result so just return the best we have
        if (results.size() == 1) {
            return results.get(0).getBestMovePath();
        }
        // check if the last one is fully resolved, if yes return it
        PositionSearchResult last = results.get(results.size() - 1);
        PositionSearchResult beforeLast = results.get(results.size() - 2);

        MovePath bestMovePath = last.getBestMovePath();
        MovePath bestBeforeLastMovePath = beforeLast.getBestMovePath();

        // if best move was null, last is invalid and therefore we return the one before
        if (bestMovePath == null) {
            return bestBeforeLastMovePath;
        }
        // if last was not canceled it is for sure the best move
        if (!last.isCanceled()) {
            return bestMovePath;
        }
        // last one was canceled -> we need to compare with best of before last
        // best before last should never have been canceled
        // check if best move of last was fully resolved, if not we cannot trust it
        if (!bestMovePath.isFullyResolved()) {
            return bestBeforeLastMovePath;
        }
        // compare bestMovePath & bestBeforeLastMovePath
        if (isWhite()) {
            return bestMovePath.getValue() > bestBeforeLastMovePath.getValue() ? bestMovePath : bestBeforeLastMovePath;
        } else {
            return bestMovePath.getValue() < bestBeforeLastMovePath.getValue() ? bestMovePath : bestBeforeLastMovePath;
        }
    }

    // TODO rn1qkbnr/ppp2ppp/3p4/4p3/3PP1b1/4BN2/PPP2PPP/RN1QKB1R b KQkq - 2 4
    @Override
    public List<MovePath> listCandidates(int variance) {
        if (results.isEmpty()) {
            return List.of();
        }
        FixedPositionSearchResult last = results.get(results.size() - 1);
        if (!last.isCanceled()) {
            return last.listCandidates(variance);
        }
        MovePath bestMovePath = getBestMovePath();
        List<MovePath> lastCandidates = last.listCandidates(variance);
        List<MovePath> finalCandidates = lastCandidates.stream()
                .filter(MovePath::isFullyResolved)
                .filter(movePath -> Math.abs(movePath.getValue() - bestMovePath.getValue()) <= variance)
                .filter(movePath -> !movePath.equals(bestMovePath))
                .toList();
        List<MovePath> candidates = new ArrayList<>();
        candidates.add(bestMovePath);
        candidates.addAll(finalCandidates);
        return candidates;
    }

}
