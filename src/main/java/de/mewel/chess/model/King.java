package de.mewel.chess.model;

import de.mewel.chess.common.ChessException;
import de.mewel.chess.common.Move;
import de.mewel.chess.common.MoveExecutor;
import de.mewel.chess.common.PositionUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class King extends AbstractPiece {

    public King(boolean white, int x, int y) {
        super(white, x, y);
    }

    public String getName() {
        return "king";
    }

    public short getValue() {
        return 99;
    }

    @Override
    public byte toByte() {
        return isWhite() ? PieceNotation.W_KING : PieceNotation.B_KING;
    }

    @Override
    public List<Move> moves(Position position) {
        // default moves
        List<Move> moves = move(position, true);
        // castling
        castle(position, moves);
        return moves;
    }

    @Override
    public List<Move> validMoves(Position position) {
        // default moves
        List<Move> moves = validMove(position, true);
        // castling
        castle(position, moves);
        return moves;
    }

    private void castle(Position position, List<Move> moves) {
        if (PositionUtil.checkCastle(position, x(), y(), true)) {
            moves.add(position.castle(true));
        }
        if (PositionUtil.checkCastle(position, x(), y(), false)) {
            moves.add(position.castle(false));
        }
    }

    @Override
    public List<Move> captures(Position position) {
        return move(position, false);
    }

    @Override
    public List<Move> pinnedMoves(Position position, King king, Piece pinningPiece) {
        throw new ChessException("a king should never be pinned ;)");
    }

    @Override
    public List<Move> inCheckMoves(Position position) {
        return validMove(position, true);
    }

    private List<Move> move(Position position, boolean moveOrCapture) {
        List<Move> moves = new ArrayList<>();
        King oppositeKing = PositionUtil.getKing(position, !position.isWhite());
        int oppositeKingX = oppositeKing.x();
        int oppositeKingY = oppositeKing.y();
        boolean canOppositeKingBlock = Math.abs(x() - oppositeKingX) <= 2 && Math.abs(y() - oppositeKingY) <= 2;
        for (int xn = -1; xn <= 1; xn++) {
            for (int yn = -1; yn <= 1; yn++) {
                int toX = x() + xn;
                int toY = y() + yn;
                boolean isBlocked = canOppositeKingBlock && PositionUtil.blockedByKing(position, toX, toY, oppositeKingX, oppositeKingY);
                if (Position.valid(toX, toY) && !isBlocked) {
                    if (moveOrCapture) {
                        PositionUtil.moveOrCapture(moves, position, x(), y(), toX, toY);
                    } else {
                        PositionUtil.capture(moves, position, x(), y(), toX, toY);
                    }
                }
            }
        }
        return moves;
    }

    private List<Move> validMove(Position position, boolean moveOrCapture) {
        List<Move> moves = new ArrayList<>();
        King oppositeKing = PositionUtil.getKing(position, !position.isWhite());
        int oppositeKingX = oppositeKing.x();
        int oppositeKingY = oppositeKing.y();
        boolean canOppositeKingBlock = Math.abs(x() - oppositeKingX) <= 2 && Math.abs(y() - oppositeKingY) <= 2;
        for (int xn = -1; xn <= 1; xn++) {
            for (int yn = -1; yn <= 1; yn++) {
                int toX = x() + xn;
                int toY = y() + yn;
                boolean isBlocked = canOppositeKingBlock && PositionUtil.blockedByKing(position, toX, toY, oppositeKingX, oppositeKingY);
                if (Position.valid(toX, toY) && !isBlocked) {
                    if (moveOrCapture) {
                        PositionUtil.moveOrCapture(moves, position, x(), y(), toX, toY);
                    } else {
                        PositionUtil.capture(moves, position, x(), y(), toX, toY);
                    }
                }
            }
        }
        // filter moves which leads to being captured
        MoveExecutor moveExecutor = new MoveExecutor();
        Position positionCopy = position.copy();
        return moves.stream().filter(move -> {
            Move copy = move.copy();
            moveExecutor.forward(positionCopy, copy);
            List<Piece> checkingPieces = PositionUtil.getCheckingPieces(positionCopy);
            moveExecutor.backward(positionCopy, copy);
            return checkingPieces.isEmpty();
        }).collect(Collectors.toList());
    }

    @Override
    public byte preventsCastling(Position position) {
        return PositionUtil.checkCastlePawnOrKing(position, x(), y());
    }

}
