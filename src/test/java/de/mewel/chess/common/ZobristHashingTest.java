package de.mewel.chess.common;

import de.mewel.chess.model.Position;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ZobristHashingTest {

    @Test
    public void move() {
        Position p = PositionUtil.base();
        long hashCode = p.longHashCode();
        assertEquals(ZobristHashing.positionHash(p), hashCode);

        MoveExecutor executor = new MoveExecutor();

        Move move = p.move("d2d4");
        executor.forward(p, move);
        hashCode = ZobristHashing.move(hashCode, move);
        assertEquals(ZobristHashing.positionHash(p), hashCode);
/*
        DOES NOT WORK YET CAUSE EN PASSANT NEEDS TO BE RESET FOR THE MOVE d2d4
        (EN_PASSANT needs a 9th entry for non enpassant moves, which also needs to be tracked)
        executor.forward(p, move = p.move("d7d6"));
        hashCode = ZobristHashing.move(hashCode, move);
        assertEquals(ZobristHashing.positionHash(p), hashCode);*/
    }

}