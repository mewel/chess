package de.mewel.chess.engine.model;

import de.mewel.chess.common.Move;

import java.util.*;
import java.util.stream.Collectors;

public class MovePath {

    private final LinkedList<MoveNode> list;

    private final int maxFullNodes;

    public MovePath(int maxFullNodes) {
        this(null, maxFullNodes);
    }

    public MovePath(MoveNode firstMove, int maxFullNodes) {
        this.list = new LinkedList<>();
        this.maxFullNodes = maxFullNodes;
        if (firstMove != null) {
            this.offer(firstMove);
        }
    }

    public Integer getValue() {
        return list.getFirst().getValue();
    }

    public void offer(MoveNode node) {
        this.list.offerLast(node);
    }

    public void poll() {
        this.list.pollLast();
    }

    public MoveNode tail() {
        return this.list.peekLast();
    }

    public MoveNode head() {
        return this.list.peekFirst();
    }

    public LinkedList<MoveNode> list() {
        return this.list;
    }

    public MoveNode ancestor(int i) {
        if (tail() != null) {
            int headIndex = this.list.indexOf(tail());
            if (headIndex - i >= 0) {
                return this.list.get(headIndex - i);
            }
        }
        return null;
    }

    public Integer size() {
        return this.list.size();
    }

    public boolean isFullyResolved() {
        return size() >= maxFullNodes;
    }

    public List<Move> moves() {
        return list.stream()
                .map(MoveNode::getMove)
                .toList();
    }

    @Override
    public String toString() {
        return list.stream()
                .map(MoveNode::getMove)
                .map(Move::toString)
                .collect(Collectors.joining(" "));
    }

    public static MovePath of(MoveNode node, int maxFullNodes) {
        MovePath path = new MovePath(node, maxFullNodes);
        create(path, node, new HashSet<>());
        return path;
    }

    private static void create(MovePath path, MoveNode node, HashSet<Long> positionHashSet) {
        PositionNode positionNode = node.getPositionNode();
        List<MoveNode> moveNodes = positionNode.getMoveNodes();
        if (moveNodes.isEmpty()) {
            return;
        }
        moveNodes.stream()
                .filter(moveNode -> !moveNode.isIgnored())
                .max(node.getMove().isWhite() ? Comparator.comparingInt(MoveNode::getValue).reversed() : Comparator.comparingInt(MoveNode::getValue))
                .ifPresent(bestNode -> {
                    long positionHash = node.getPositionNode().getPositionHash();
                    if (node.getValue().equals(bestNode.getValue()) && !positionHashSet.contains(positionHash)) {
                        path.offer(bestNode);
                        positionHashSet.add(positionHash);
                        create(path, bestNode, positionHashSet);
                    }
                });
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MovePath movePath = (MovePath) o;
        return maxFullNodes == movePath.maxFullNodes && Objects.equals(list, movePath.list);
    }

    @Override
    public int hashCode() {
        return Objects.hash(list, maxFullNodes);
    }

}
