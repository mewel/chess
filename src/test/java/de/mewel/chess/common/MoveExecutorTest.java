package de.mewel.chess.common;

import de.mewel.chess.model.PieceNotation;
import de.mewel.chess.model.Position;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MoveExecutorTest {

    @Test
    public void backward() {
        MoveExecutor moveExecutor = new MoveExecutor();

        // promote and capture
        Position promotingPosition = PositionUtil
                .empty()
                .set(PieceNotation.W_PAWN, 0, 6)
                .set(PieceNotation.B_ROOK, 1, 7);
        int promotingPositionHashCode = promotingPosition.hashCode();
        Move promotingMove = promotingPosition.promote(0, 6, 1, 7, PieceNotation.W_QUEEN);
        moveExecutor.forward(promotingPosition, promotingMove);
        moveExecutor.backward(promotingPosition, promotingMove);
        assertEquals(promotingPositionHashCode, promotingPosition.hashCode());

        // en passant
        Position enPassantPosition = PositionUtil.empty()
                .set(PieceNotation.W_PAWN, 0, 4)
                .set(PieceNotation.W_PAWN, 2, 4)
                .set(PieceNotation.B_PAWN, 1, 6);
        int enPassantPositionHashCode = enPassantPosition.hashCode();
        Move twoSquareMove = enPassantPosition.move(1, 6, 1, 4);
        Move enPassantCaptureMove = enPassantPosition.enPassant(0, 4, 1);
        moveExecutor.forward(enPassantPosition, twoSquareMove, enPassantCaptureMove);
        moveExecutor.backward(enPassantPosition, twoSquareMove, enPassantCaptureMove);
        assertEquals(enPassantPositionHashCode, enPassantPosition.hashCode());

        // multi check
        Position multiPosition = PositionUtil.base();
        int multiPositionHashCode = multiPosition.hashCode();
        Move e4 = multiPosition.move(4, 1, 4, 3);
        Move g1f3 = multiPosition.move(6, 0, 5, 2);
        Move f1c4 = multiPosition.move(5, 0, 2, 3);
        Move shortCastle = multiPosition.castle(true);
        moveExecutor.forward(multiPosition, e4, g1f3, f1c4, shortCastle);
        moveExecutor.backward(multiPosition, e4, g1f3, f1c4, shortCastle);
        assertEquals(multiPositionHashCode, multiPosition.hashCode());

        // pawn capture black
        Position blackPawnCapturePosition = PositionUtil
                .empty()
                .set(PieceNotation.B_PAWN, 0, 5)
                .set(PieceNotation.B_PAWN, 1, 5)
                .set(PieceNotation.W_ROOK, 0, 4);
        Move captureMove = blackPawnCapturePosition.move(1, 5, 0, 4);
        moveExecutor.forward(blackPawnCapturePosition, captureMove);
        moveExecutor.backward(blackPawnCapturePosition, captureMove);
        assertEquals(PieceNotation.B_PAWN, blackPawnCapturePosition.get(0, 5));
        assertEquals(PieceNotation.B_PAWN, blackPawnCapturePosition.get(1, 5));
        assertEquals(PieceNotation.W_ROOK, blackPawnCapturePosition.get(0, 4));

        // castle
        Position whiteCastlePosition = PositionUtil
                .empty()
                .set(PieceNotation.W_KING, 4, 0)
                .set(PieceNotation.W_ROOK, 0, 0)
                .set(PieceNotation.W_ROOK, 7, 0);
        int whiteCastlePositionHashCode = whiteCastlePosition.hashCode();
        // check castle short
        Move castleShort = whiteCastlePosition.castle(true);
        moveExecutor.forward(whiteCastlePosition, castleShort);
        moveExecutor.backward(whiteCastlePosition, castleShort);
        assertEquals(whiteCastlePositionHashCode, whiteCastlePosition.hashCode());
        // check castle long
        Move castleLong = whiteCastlePosition.castle(false);
        moveExecutor.forward(whiteCastlePosition, castleLong);
        moveExecutor.backward(whiteCastlePosition, castleLong);
        assertEquals(whiteCastlePositionHashCode, whiteCastlePosition.hashCode());
    }

    @Test
    public void forward() {
        MoveExecutor moveExecutor = new MoveExecutor();

        // pawn capture white
        Position whitePawnCapturePosition = PositionUtil
                .empty()
                .set(PieceNotation.W_PAWN, 0, 5)
                .set(PieceNotation.W_PAWN, 1, 5)
                .set(PieceNotation.B_ROOK, 0, 6);
        Move captureMove = whitePawnCapturePosition.move(1, 5, 0, 6);
        moveExecutor.forward(whitePawnCapturePosition, captureMove);
        assertEquals(PieceNotation.W_PAWN, whitePawnCapturePosition.get(0, 5));
        assertEquals(PieceNotation.W_PAWN, whitePawnCapturePosition.get(0, 6));
        assertEquals(PieceNotation.EMPTY, whitePawnCapturePosition.get(1, 5));

        // pawn capture black
        Position blackPawnCapturePosition = PositionUtil
                .empty()
                .set(PieceNotation.B_PAWN, 0, 5)
                .set(PieceNotation.B_PAWN, 1, 5)
                .set(PieceNotation.W_ROOK, 0, 4);
        captureMove = blackPawnCapturePosition.move(1, 5, 0, 4);
        moveExecutor.forward(blackPawnCapturePosition, captureMove);
        assertEquals(PieceNotation.B_PAWN, blackPawnCapturePosition.get(0, 5));
        assertEquals(PieceNotation.B_PAWN, blackPawnCapturePosition.get(0, 4));
        assertEquals(PieceNotation.EMPTY, blackPawnCapturePosition.get(1, 5));

        // promotion
        Position promotionPosition = PositionUtil
                .empty()
                .set(PieceNotation.B_PAWN, 3, 1)
                .set(PieceNotation.W_ROOK, 4, 0);
        Move promotionMove = promotionPosition.promote(3, 1, 4, 0, PieceNotation.B_QUEEN);
        moveExecutor.forward(promotionPosition, promotionMove);
        assertEquals(PieceNotation.B_QUEEN, promotionPosition.get(4, 0));
        assertEquals(PieceNotation.EMPTY, promotionPosition.get(3, 1));
    }

}
