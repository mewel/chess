package de.mewel.chess;

import de.mewel.chess.common.ChessException;
import de.mewel.chess.engine.FixedPositionSearcher;
import de.mewel.chess.engine.PositionSearchCache;
import de.mewel.chess.engine.PositionSearcher;
import de.mewel.chess.engine.PositionSearcherSettings;
import de.mewel.chess.engine.evaluator.DefaultPositionEvaluator;
import de.mewel.chess.engine.evaluator.PiecePositionEvaluator;
import de.mewel.chess.engine.evaluator.PositionEvaluator;
import de.mewel.chess.engine.model.Game;
import de.mewel.chess.engine.model.PositionSearchResult;
import de.mewel.chess.engine.model.PositionSearchResult.GameStatus;
import de.mewel.chess.model.Position;

import java.util.concurrent.*;

public class EngineRunner {

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_RED = "\u001B[31m";

    public RunResult run(Engine engine1, Engine engine2, int games) {

        ExecutorService executorService = Executors.newFixedThreadPool(16);
        CompletionService<GameResult> completionService = new ExecutorCompletionService<>(executorService);

        try {
            boolean white = true;
            RunResult result = new RunResult();
            for (int i = 0; i < games; i++) {
                completionService.submit(new GameRunnerCallable(white, engine1, engine2));
                white = !white;
            }
            for (int i = 0; i < games; i++) {
                try {
                    GameResult gameResult = completionService.take().get();
                    if (gameResult.white) {
                        result.engine1Score.whiteWins += GameStatus.WHITE_WON.equals(gameResult.gameStatus) ? 1 : 0;
                        result.engine1Score.whiteDraws += GameStatus.DRAW.equals(gameResult.gameStatus) ? 1 : 0;
                        result.engine1Score.whiteLoss += GameStatus.BLACK_WON.equals(gameResult.gameStatus) ? 1 : 0;
                        result.engine2Score.blackWins += GameStatus.BLACK_WON.equals(gameResult.gameStatus) ? 1 : 0;
                        result.engine2Score.blackDraws += GameStatus.DRAW.equals(gameResult.gameStatus) ? 1 : 0;
                        result.engine2Score.blackLoss += GameStatus.WHITE_WON.equals(gameResult.gameStatus) ? 1 : 0;
                        System.out.println((GameStatus.WHITE_WON.equals(gameResult.gameStatus) ? "engine 1 won with white" :
                                GameStatus.DRAW.equals(gameResult.gameStatus) ? "engine 1 draws with white" : "engine 1 lost with white") + " - " + positionInformation(gameResult.position));
                    } else {
                        result.engine1Score.blackWins += GameStatus.BLACK_WON.equals(gameResult.gameStatus) ? 1 : 0;
                        result.engine1Score.blackDraws += GameStatus.DRAW.equals(gameResult.gameStatus) ? 1 : 0;
                        result.engine1Score.blackLoss += GameStatus.WHITE_WON.equals(gameResult.gameStatus) ? 1 : 0;
                        result.engine2Score.whiteWins += GameStatus.WHITE_WON.equals(gameResult.gameStatus) ? 1 : 0;
                        result.engine2Score.whiteDraws += GameStatus.DRAW.equals(gameResult.gameStatus) ? 1 : 0;
                        result.engine2Score.whiteLoss += GameStatus.BLACK_WON.equals(gameResult.gameStatus) ? 1 : 0;
                        System.out.println((GameStatus.BLACK_WON.equals(gameResult.gameStatus) ? "engine 1 won with black" :
                                GameStatus.DRAW.equals(gameResult.gameStatus) ? "engine 1 draws with black" : "engine 1 lost with black") + " - " + positionInformation(gameResult.position));
                    }
                } catch (Exception exc) {
                    exc.printStackTrace();
                }
            }
            result.games = games;
            return result;
        } finally {
            executorService.shutdown();
        }
    }

    private String positionInformation(Position position) {
        return "turn: " + position.getTurn();
    }

    public record Engine(String name, PositionSearcherSettings settings, PositionEvaluator evaluator) {
    }

    public static class RunResult {

        int games;

        Score engine1Score = new Score();

        Score engine2Score = new Score();

    }

    private static class Score {

        int whiteWins = 0;

        int whiteDraws = 0;

        int whiteLoss = 0;

        int blackWins = 0;

        int blackDraws = 0;

        int blackLoss = 0;

        @Override
        public String toString() {
            String whiteResult = (whiteWins >= whiteLoss ? ANSI_GREEN : ANSI_RED) + (whiteWins - whiteLoss) + ANSI_RESET + "\n";
            String blackResult = (blackWins >= blackLoss ? ANSI_GREEN : ANSI_RED) + (blackWins - blackLoss) + ANSI_RESET + "\n";
            return "white: " + whiteWins + "/" + whiteDraws + "/" + whiteLoss + " " + whiteResult +
                    "black: " + blackWins + "/" + blackDraws + "/" + blackLoss + " " + blackResult;
        }
    }

    public static class GameResult {

        boolean white;

        GameStatus gameStatus;

        Position position;

    }

    private static class GameRunnerCallable implements Callable<GameResult> {

        private final boolean engine1IsWhite;

        private final Engine engine1;

        private final Engine engine2;

        public GameRunnerCallable(boolean white, Engine engine1, Engine engine2) {
            this.engine1IsWhite = white;
            this.engine1 = engine1;
            this.engine2 = engine2;
        }

        @Override
        public GameResult call() throws Exception {
            PositionSearchCache cache1 = new PositionSearchCache(1_000_000);
            FixedPositionSearcher searcher1 = new FixedPositionSearcher(engine1.settings, engine1.evaluator, cache1);

            PositionSearchCache cache2 = new PositionSearchCache(1_000_000);
            //PositionEvaluator searcher2 = new PositionEvaluator(engine2.settings, engine2.evaluator, cache2);
            //IterativePositionSearcher searcher2 = new IterativePositionSearcher(engine2.evaluator, engine2.settings.variance());
            FixedPositionSearcher searcher2 = new FixedPositionSearcher(engine2.settings, engine2.evaluator, cache2);

            Game game = new Game();
            GameResult gameResult = new GameResult();
            gameResult.white = engine1IsWhite;
            gameResult.position = game.getPosition();
            try {
                for (int i = 0; i < 200; i++) {
                    boolean isWhite = game.getPosition().isWhite() == engine1IsWhite;
                    Engine engine = isWhite ? engine1 : engine2;
                    PositionSearcher evaluator = isWhite ? searcher1 : searcher2;
                    PositionSearchResult result = evaluator.search(game.getPosition(), game.getPositionHashList());
                    game.play(result, engine.settings.variance());
                    if (result.isGameFinished()) {
                        gameResult.gameStatus = result.getGameStatus();
                        return gameResult;
                    }
                }
            } catch (ChessException chessException) {
                throw new ChessException("Error in game:\r\n" + game.movesToString(), chessException);
            }
            gameResult.gameStatus = GameStatus.DRAW;
            return gameResult;
        }
    }

    public static void main(String[] args) {
        long sTime = System.currentTimeMillis();
/*
        EngineRunner engineRunner = new EngineRunner();
        Engine engine1 = new Engine();
        engine1.name = "mewel";
        engine1.settings = new PositionEvaluatorSettings(2, 4).setVariance(10).setAnalyzer(new DefaultPositionAnalyzer());

        Engine engine2 = new Engine();
        engine2.name = "tammi";
        engine2.settings = new PositionEvaluatorSettings(2, 4).setVariance(10).setAnalyzer(new OtherPositionAnalyzer());

        RunResult result = engineRunner.run(engine1, engine2, 5000);
*/

        EngineRunner engineRunner = new EngineRunner();
        Engine engine1 = new Engine("mewel",
                new PositionSearcherSettings(4, 12, 15),
                new DefaultPositionEvaluator());

        Engine engine2 = new Engine("tammi",
                new PositionSearcherSettings(4, 12, 15),
                new PiecePositionEvaluator());

        System.out.println(engine1.name + " (" + engine1.evaluator.getClass().getSimpleName() + ") vs "
                + engine2.name + " (" + engine2.evaluator.getClass().getSimpleName() + ")");

        RunResult result = engineRunner.run(engine1, engine2, 20);

        System.out.println("games: " + result.games);
        System.out.println("Engine1 (" + engine1.name + "):");
        System.out.println(result.engine1Score);
        System.out.println("Engine2 (" + engine2.name + "):");
        System.out.println(result.engine2Score);
        System.out.println("took " + ((System.currentTimeMillis() - sTime) / 1000) + "s");
    }

}
