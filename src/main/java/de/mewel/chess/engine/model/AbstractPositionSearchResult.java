package de.mewel.chess.engine.model;

public abstract class AbstractPositionSearchResult implements PositionSearchResult {

    private long took;

    private GameStatus gameStatus;

    private DrawType drawType;

    private final boolean white;

    private final int turn;

    private boolean canceled;

    public AbstractPositionSearchResult(boolean white, int turn) {
        this.white = white;
        this.gameStatus = GameStatus.ONGOING;
        this.turn = turn;
        this.canceled = false;
    }

    public void took(long took) {
        this.took = took;
    }

    @Override
    public long took() {
        return this.took + 1;
    }

    public void setGameStatus(GameStatus gameStatus) {
        this.gameStatus = gameStatus;
    }

    public void setDrawType(DrawType drawType) {
        this.drawType = drawType;
    }

    @Override
    public GameStatus getGameStatus() {
        return gameStatus;
    }

    @Override
    public DrawType getDrawType() {
        return drawType;
    }

    @Override
    public int getTurn() {
        return turn;
    }

    public void cancel() {
        this.canceled = true;
    }

    @Override
    public boolean isCanceled() {
        return canceled;
    }

    @Override
    public boolean isWhite() {
        return white;
    }

}
