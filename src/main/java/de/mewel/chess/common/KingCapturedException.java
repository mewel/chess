package de.mewel.chess.common;

public class KingCapturedException extends ChessException {

    public KingCapturedException(String message) {
        super(message);
    }

}
