package de.mewel.chess.model;

import de.mewel.chess.common.Move;
import de.mewel.chess.common.PositionUtil;

import java.util.ArrayList;
import java.util.List;

public class Queen extends AbstractPiece {

    public Queen(boolean white, int x, int y) {
        super(white, x, y);
    }

    public String getName() {
        return "queen";
    }

    public short getValue() {
        return 9;
    }

    @Override
    public byte toByte() {
        return isWhite() ? PieceNotation.W_QUEEN : PieceNotation.B_QUEEN;
    }

    @Override
    public List<Move> moves(Position position) {
        List<Move> moves = PositionUtil.moveHorizontalAndVertical(position, x(), y());
        moves.addAll(PositionUtil.moveDiagonal(position, x(), y()));
        return moves;
    }

    @Override
    public List<Move> validMoves(Position position) {
        return moves(position);
    }

    @Override
    public List<Move> captures(Position position) {
        List<Move> moves = PositionUtil.captureHorizontalAndVertical(position, x(), y());
        moves.addAll(PositionUtil.captureDiagonal(position, x(), y()));
        return moves;
    }

    @Override
    public List<Move> pinnedMoves(Position position, King king, Piece pinningPiece) {
        ArrayList<Move> moves = new ArrayList<>();
        // move in pinned piece direction
        int pinnedPieceDiffX = Math.abs(x() - pinningPiece.x());
        int pinnedPieceDiffY = Math.abs(y() - pinningPiece.y());
        int pinnedPieceDiff = Math.max(pinnedPieceDiffX, pinnedPieceDiffY);
        int moveX = Integer.compare(pinningPiece.x(), x());
        int moveY = Integer.compare(pinningPiece.y(), y());
        for (int i = 1; i <= pinnedPieceDiff; i++) {
            PositionUtil.moveOrCapture(moves, position, x(), y(), x() + i * moveX, y() + i * moveY);
        }
        // move in king direction
        int kingDiffX = Math.abs(x() - king.x());
        int kingDiffY = Math.abs(y() - king.y());
        int kingDiff = Math.max(kingDiffX, kingDiffY);
        for (int i = 1; i < kingDiff; i++) {
            if (!PositionUtil.move(moves, position, x(), y(), x() + i * moveX * -1, y() + i * moveY * -1)) {
                break;
            }
        }
        return moves;
    }

    @Override
    public List<Move> inCheckMoves(Position position) {
        List<Move> moves = new ArrayList<>();
        List<Piece> checkingPieces = position.getCheckingPieces();
        if (checkingPieces.size() > 1) {
            return moves;
        }
        Piece checkingPiece = checkingPieces.get(0);
        King king = PositionUtil.getKing(position, position.isWhite());
        PositionUtil.hvIntersectAndCapture(position, this, checkingPiece, king, moves);
        PositionUtil.diagonalIntersectAndCapture(position, this, checkingPiece, king, moves);
        return moves;
    }

    @Override
    public byte preventsCastling(Position position) {
        byte castle = PositionUtil.checkCastleVertical(position, x(), y());
        if (castle == 3) {
            return castle;
        }
        castle |= PositionUtil.checkCastleDiagonal(position, x(), y());
        return castle;
    }

}
