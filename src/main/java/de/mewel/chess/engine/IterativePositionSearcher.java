package de.mewel.chess.engine;

import de.mewel.chess.common.ChessException;
import de.mewel.chess.common.Move;
import de.mewel.chess.common.MoveExecutor;
import de.mewel.chess.engine.evaluator.PositionEvaluator;
import de.mewel.chess.engine.model.FixedPositionSearchResult;
import de.mewel.chess.engine.model.IterativePositionSearchResult;
import de.mewel.chess.engine.model.MovePath;
import de.mewel.chess.engine.model.PositionSearchResult;
import de.mewel.chess.model.Position;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class IterativePositionSearcher implements PositionSearcher, Runnable {

    private final PositionEvaluator evaluator;

    private int variance;

    private final PositionSearchCache cache;

    private FixedPositionSearchResult fallbackResult;

    private IterativePositionSearchResult result;

    private Position position;

    private LinkedList<Long> positionHashList;

    private int nodes;

    private IterativePositionSearcherListener listener;

    public IterativePositionSearcher(final PositionEvaluator evaluator) {
        this(evaluator, 0);
    }

    public IterativePositionSearcher(PositionEvaluator evaluator, int variance) {
        this.evaluator = evaluator;
        this.variance = variance;
        this.cache = new PositionSearchCache(1_000_000);
        this.listener = null;
    }

    @Override
    public IterativePositionSearchResult search(Position position, LinkedList<Long> positionHashList) {
        this.position = position.copy();
        this.positionHashList = positionHashList;
        this.result = new IterativePositionSearchResult(position.isWhite(), position.getTurn());
        this.nodes = 4;
        this.fallbackResult = getFallbackResult(position, positionHashList);
        result.add(fallbackResult);
        if (listener != null) {
            listener.onResult(fallbackResult);
        }
        return result;
    }

    private FixedPositionSearchResult getFallbackResult(Position position, LinkedList<Long> positionHashList) {
        try {
            return new PositionEvaluationTask(this, 2).evaluate(position, positionHashList, new ArrayList<>());
        } catch (PositionSearcherCanceledException e) {
            throw new ChessException("Unable to evaluate fallback position", e);
        }
    }

    public PositionEvaluator getEvaluator() {
        return evaluator;
    }

    public PositionSearchCache getCache() {
        return cache;
    }

    public void setVariance(int variance) {
        this.variance = variance;
    }

    public int getVariance() {
        return variance;
    }

    public void setListener(IterativePositionSearcherListener listener) {
        this.listener = listener;
    }

    @Override
    public void run() {
        long startTime = System.currentTimeMillis();
        try {
            boolean run = true;
            List<Move> lastBestMoves = getLastBestMoves(fallbackResult.getBestMovePath());
            FixedPositionSearchResult previousFixedResult = fallbackResult;
            do {
                PositionEvaluationTask task = new PositionEvaluationTask(this, nodes);
                try {
                    FixedPositionSearchResult fixedResult = task.evaluate(position, positionHashList, lastBestMoves);
                    if (fixedResult.isGameFinished()) {
                        run = false;
                    }
                    lastBestMoves = getLastBestMoves(fixedResult.getBestMovePath());
                    result.add(fixedResult);
                    if (listener != null && !previousFixedResult.equals(fixedResult)) {
                        listener.onResult(fixedResult);
                    }
                    previousFixedResult = fixedResult;
                } catch (PositionSearcherCanceledException e) {
                    result.add((FixedPositionSearchResult) e.getResult());
                    run = false;
                }
                nodes += 1;
            } while (run && !Thread.currentThread().isInterrupted());
        } catch (Exception e) {
            // TODO
            e.printStackTrace();
        } finally {
            result.took(System.currentTimeMillis() - startTime);
            if (listener != null) {
                try {
                    listener.onFinished(result);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private List<Move> getLastBestMoves(MovePath bestMovePath) {
        Position copy = position.copy();
        MoveExecutor moveExecutor = new MoveExecutor();
        List<Move> moves = bestMovePath.moves();
        moveExecutor.forward(copy, moves);
        moves.forEach(Move::clearForwardMeta);
        return moves;
    }

    private static final class PositionEvaluationTask {

        private final FixedPositionSearcher searcher;

        private PositionEvaluationTask(IterativePositionSearcher iterativeSearcher, int fullNodes) {
            int quiescenceNodes = fullNodes * 3;
            PositionSearcherSettings settings = new PositionSearcherSettings(fullNodes, quiescenceNodes, iterativeSearcher.getVariance());
            this.searcher = new FixedPositionSearcher(settings, iterativeSearcher.getEvaluator(), iterativeSearcher.getCache());
        }

        public FixedPositionSearchResult evaluate(Position position, LinkedList<Long> positionHashList, List<Move> lastBestMoves) throws PositionSearcherCanceledException {
            return this.searcher.search(position, positionHashList, lastBestMoves);
        }

        public void cancel() {
            this.searcher.cancel();
        }

    }

    public interface IterativePositionSearcherListener {
        void onResult(PositionSearchResult result);

        void onFinished(IterativePositionSearchResult result);
    }

}
