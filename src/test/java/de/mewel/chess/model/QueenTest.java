package de.mewel.chess.model;

import de.mewel.chess.common.Move;
import de.mewel.chess.common.PieceUtil;
import de.mewel.chess.common.PositionUtil;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static de.mewel.chess.model.PieceNotation.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class QueenTest {

    @Test
    public void inCheckMoves() {
        Position p = PositionUtil.empty();

        Queen queen = set(p, 2, 0, 5, 0, B_QUEEN, 5, 3);
        assertEquals(1, queen.inCheckMoves(p).size());
        assertTrue(queen.inCheckMoves(p).contains(p.move("c1xf4")));

        queen = set(p, 7, 1, 5, 0, B_QUEEN, 5, 3);
        assertEquals(2, queen.inCheckMoves(p).size());
        assertTrue(queen.inCheckMoves(p).contains(p.move("h2xf4")));
        assertTrue(queen.inCheckMoves(p).contains(p.move("h2f2")));

        queen = set(p, 5, 3, 1, 1, B_QUEEN, 5, 5);
        assertEquals(3, queen.inCheckMoves(p).size());
        assertTrue(queen.inCheckMoves(p).contains(p.move("f4xf6")));
        assertTrue(queen.inCheckMoves(p).contains(p.move("f4e5")));
        assertTrue(queen.inCheckMoves(p).contains(p.move("f4d4")));

        queen = set(p, 4, 3, 5, 1, B_QUEEN, 5, 5);
        assertEquals(3, queen.inCheckMoves(p).size());
        assertTrue(queen.inCheckMoves(p).contains(p.move("e4f5")));
        assertTrue(queen.inCheckMoves(p).contains(p.move("e4f4")));
        assertTrue(queen.inCheckMoves(p).contains(p.move("e4f3")));

        queen = set(p, 3, 4, 5, 0, B_QUEEN, 5, 7);
        assertEquals(3, queen.inCheckMoves(p).size());
        assertTrue(queen.inCheckMoves(p).contains(p.move("d5f7")));
        assertTrue(queen.inCheckMoves(p).contains(p.move("d5f5")));
        assertTrue(queen.inCheckMoves(p).contains(p.move("d5f3")));
        p.set(W_PAWN, 4, 5);
        assertEquals(2, queen.inCheckMoves(p).size());
        assertTrue(queen.inCheckMoves(p).contains(p.move("d5f5")));
        assertTrue(queen.inCheckMoves(p).contains(p.move("d5f3")));
        p.set(W_PAWN, 4, 4);
        assertEquals(1, queen.inCheckMoves(p).size());
        assertTrue(queen.inCheckMoves(p).contains(p.move("d5f3")));
        p.set(W_PAWN, 4, 3);
        assertEquals(0, queen.inCheckMoves(p).size());

        p.setWhite(false);
        queen = set(p, 5, 5, 4, 7, W_BISHOP, 5, 6);
        assertEquals(1, queen.inCheckMoves(p).size());
        assertTrue(queen.inCheckMoves(p).contains(p.move("f6xf7")));

        p.setWhite(true);
        queen = set(p, 3, 0, 4, 0, B_QUEEN, 0, 4);
        assertEquals(1, queen.inCheckMoves(p).size());
        assertTrue(queen.inCheckMoves(p).contains(p.move("d1d2")));

        queen = set(p, 3, 0, 4, 0, B_KNIGHT, 3, 2);
        p.set(W_PAWN, 3, 1);
        assertEquals(0, queen.inCheckMoves(p).size());
    }

    @Test
    public void pinnedMoves() {
        // test with queen
        Position p2 = PositionUtil.empty()
                .set(B_QUEEN, 0, 7)
                .set(W_QUEEN, 5, 2)
                .set(PieceNotation.W_KING, 7, 0);
        Queen blackQueen = new Queen(false, 0, 7);
        Queen whiteQueen = new Queen(true, 5, 2);
        King whiteKing = new King(true, 7, 0);
        List<Move> pinnedMoves = whiteQueen.pinnedMoves(p2, whiteKing, blackQueen);
        assertEquals(6, pinnedMoves.size());

        // test with rook
        Position p1 = PositionUtil.empty()
                .set(B_ROOK, 0, 7)
                .set(W_QUEEN, 0, 5)
                .set(PieceNotation.W_KING, 0, 0);
        Rook blackRook = new Rook(false, 0, 7);
        whiteQueen = new Queen(true, 0, 5);
        whiteKing = new King(true, 0, 0);
        pinnedMoves = whiteQueen.pinnedMoves(p1, whiteKing, blackRook);
        assertEquals(6, pinnedMoves.size());
    }

    private Queen set(Position p, int queenX, int queenY, int kingX, int kingY, byte attPiece, int attX, int attY) {
        p.clear();
        setCheckingPiece(p, attPiece, attX, attY);
        p.set(p.isWhite() ? W_KING : B_KING, kingX, kingY);
        return setQueen(p, queenX, queenY);
    }

    private Queen setQueen(Position p, int x, int y) {
        p.set(p.isWhite() ? W_QUEEN : B_QUEEN, x, y);
        return new Queen(p.isWhite(), x, y);
    }

    private void setCheckingPiece(Position p, byte piece, int x, int y) {
        p.set(piece, x, y);
        p.setCheckingPieces(Arrays.asList(Objects.requireNonNull(PieceUtil.fromByte(piece, x, y))));
    }

}
