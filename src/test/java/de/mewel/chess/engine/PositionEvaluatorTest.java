package de.mewel.chess.engine;

import de.mewel.chess.ChessH2;
import de.mewel.chess.EngineUtil;
import de.mewel.chess.common.Move;
import de.mewel.chess.common.MoveExecutor;
import de.mewel.chess.common.PositionUtil;
import de.mewel.chess.engine.evaluator.DefaultPositionEvaluator;
import de.mewel.chess.engine.evaluator.PiecePositionEvaluator;
import de.mewel.chess.engine.model.*;
import de.mewel.chess.model.PieceNotation;
import de.mewel.chess.model.Position;
import de.mewel.chess.renderer.CLIRenderer;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import java.lang.annotation.Repeatable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;

import static de.mewel.chess.model.PieceNotation.B_PAWN;
import static org.junit.jupiter.api.Assertions.*;

public class PositionEvaluatorTest {

    @Test
    public void whiteTest() throws PositionSearcherCanceledException {
        FixedPositionSearcher evaluator = build(2, 4);
        Position position = PositionUtil.empty();
        position.set(B_PAWN, 1, 2)
                .set(B_PAWN, 3, 2)
                .set(PieceNotation.B_KNIGHT, 2, 1)
                .set(PieceNotation.W_KNIGHT, 0, 0)
                .set(PieceNotation.W_KING, 0, 7)
                .set(PieceNotation.B_KING, 7, 7);
        evaluate(position, evaluator);
        assertEquals(PieceNotation.W_KNIGHT, position.get(1, 2));
    }

    @Test
    public void blackTest() throws PositionSearcherCanceledException {
        FixedPositionSearcher evaluator = build(2, 4);
        Position position = PositionUtil.empty();
        position.set(PieceNotation.W_PAWN, 1, 5)
                .set(PieceNotation.W_PAWN, 3, 5)
                .set(PieceNotation.W_KNIGHT, 2, 6)
                .set(PieceNotation.B_KNIGHT, 0, 7)
                .set(PieceNotation.W_KING, 0, 0)
                .set(PieceNotation.B_KING, 7, 0)
                .setWhite(false);
        evaluate(position, evaluator);
        assertEquals(PieceNotation.B_KNIGHT, position.get(1, 5));
    }

    @Test
    public void bugTest() throws Exception {
        FixedPositionSearchResult result;

        FixedPositionSearcher searcher = build(2, 4);
        ChessH2.connect();
        MoveExecutor moveExecutor = new MoveExecutor();
        Position position = PositionUtil.base();
        moveExecutor.forward(position, "e2e4 e7e5 f1b5 d8h4 d1f3 f8b4 g2g3 h4f6 a2a3 b4a5 b2b4 a5b6 c1b2 g8e7 a3a4 a7a6 b5d3 b8c6 b2a3 c6d4 f3e3 d4c2 d3c2 b6e3 d2e3 f6c6 a1a2 c6c4 a2b2 d7d5 b1d2 c4c3 b2a2 c8g4 h2h4 d5e4 c2e4 g4e6 g1e2");
        evaluate(position, searcher);
        ChessH2.close();

        searcher = build(2, 4);
        position = PositionUtil.base();
        moveExecutor.forward(position, "h2h4 g7g6 g2g4 f8g7 e2e4 g8f6 f1d3 b8c6 g4g5 f6h5 f2f3 g7d4 c2c4 c6b4 g1e2 d4e5 c4c5 e5g3 e2g3 h5g3 h1h3 g3h5 e4e5");
        result = evaluate(position, searcher);
        assertEquals("b4d3", result.getBestMovePath().head().getMove().toString());

        searcher = build(4, 12);
        position = PositionUtil.base();
        moveExecutor.forward(position, "d2d4 d7d5 c2c4 d5c4 e2e4 g8f6 e4e5");
        result = searcher.search(position);
        assertTrue(result.getBestMovePath().head().getMove().toString().startsWith("f6"));
    }

    @Test
    public void buildBaseTreeTest() throws PositionSearcherCanceledException {
        FixedPositionSearcher evaluator = build(0, 2);
        // 2 quiescence moves
        Position rookPosition = PositionUtil.empty()
                .set(PieceNotation.W_ROOK, 0, 0)
                .set(PieceNotation.W_ROOK, 7, 7)
                .set(PieceNotation.B_ROOK, 7, 0)
                .set(PieceNotation.B_ROOK, 0, 7);
        FixedPositionSearchResult rootResult = evaluator.search(rookPosition);
        assertEquals(8, rootResult.getRootNode().countMoves());
    }

    @Test
    public void alphaBeta() throws PositionSearcherCanceledException {
        Position p = PositionUtil.base();
        FixedPositionSearcher evaluator1 = build(4, 8);
        MoveExecutor moveExecutor = new MoveExecutor();
        moveExecutor.forward(p, "e2e3 g7g5 d2d4 e7e6 g1f3 f7f6 f1d3 h7h6 e1g1");
        FixedPositionSearchResult result = evaluator1.search(p);
        assertFalse(
                result.getMoveMap().keySet().stream()
                        .map(MovePath::head)
                        .map(MoveNode::getMove)
                        .map(Move::toString)
                        .anyMatch(m -> m.equals("f8a3")),
                "shouldn't contain f8a3 cause it blunders a bishop");

        FixedPositionSearcher evaluator2 = build(4, 26);
        p = PositionUtil.base();
        moveExecutor.forward(p, "e2e4 b8c6 g1f3 f7f6 d2d4 g7g6 b1c3 h7h6 f1b5 a7a5 a2a4 f8g7 e1g1 a8b8 h2h3 e7e5 d4e5 c6a7 c1e3 b7b6 c3d5 c7c6 b5c6");
        CLIRenderer.render(p);
        result = evaluator2.search(p);
        assertFalse(
                result.getMoveMap().keySet().stream()
                        .map(MovePath::head)
                        .map(MoveNode::getMove)
                        .map(Move::toString)
                        .anyMatch(m -> m.equals("f6f5")),
                "shouldn't contain f8a3 cause it blunders a bishop");
    }

    @Test
    public void cacheTest() throws PositionSearcherCanceledException {
        FixedPositionSearcher evaluator1 = build(4, 6);
        Position p1 = PositionUtil.base();
        evaluate(p1, evaluator1);

        FixedPositionSearchResult result1 = evaluate(p1, evaluator1);
        int moves = result1.getRootNode().countMoves();
        int positions = result1.getRootNode().countPositions();

        FixedPositionSearcher evaluator2 = build(4, 6);
        Position p2 = PositionUtil.base();
        evaluate(p2, evaluator2);
        FixedPositionSearchResult result2 = evaluate(p2, evaluator2);

        assertEquals(moves, result2.getRootNode().countMoves());
        assertEquals(positions, result2.getRootNode().countPositions());

        Set<MoveNode> moves1 = result1.getRootNode().descendentMoves();
        Set<MoveNode> moves2 = result2.getRootNode().descendentMoves();
        ArrayList<MoveNode> copyMoves = new ArrayList<>(moves2);
        copyMoves.removeIf(node -> moves1.stream()
                .anyMatch(n -> n.getMove().equals(node.getMove()) && node.getPositionNode().getPositionHash() == n.getPositionNode().getPositionHash()));
        assertEquals(0, copyMoves.size());

        Set<Long> positions1 = result1.getRootNode().descendentPositions();
        Set<Long> positions2 = result2.getRootNode().descendentPositions();
        Set<Long> copyPositions = new HashSet<>(positions1);
        copyPositions.removeIf(positions2::contains);
        assertEquals(0, copyPositions.size());
    }

    /**
     * date         device   time    nodes
     * -------------------------------------
     * 29.11.2024   laptop   1.7s   199652
     * 05.12.2024   laptop   3.0s   774972 (3 tests)
     * 07.12.2024   laptop   2.3s   771892
     *
     * @throws PositionSearcherCanceledException
     */
    //@RepeatedTest(10)
    @Test
    public void performanceTest() throws PositionSearcherCanceledException, InterruptedException {
        Position earlyPosition = PositionUtil.fen("r1bqkb1r/1ppp1pp1/p1n2n1p/4p3/P1B1P3/3P1N2/1PP2PPP/RNBQK2R w KQkq - 0 1");
        Position midPosition = PositionUtil.fen("r3k1nr/1p3ppp/p1n5/2bN4/2B1P3/5P2/PP3P1P/R3K2R w KQkq - 0 13");
        Position latePosition = PositionUtil.fen("8/2r1k1p1/3p3p/4p3/P1B1P1P1/3PK3/2P2P2/8 w - - 0 1");

        PositionSearchResult earlyResult = checkPerformance(earlyPosition, 3);
        assertTrue(earlyResult.getBestMovePath().toString().startsWith("b1c3"));
        // short castle would be slightly better

        PositionSearchResult midResult = checkPerformance(midPosition, 4);
        assertTrue(midResult.getBestMovePath().toString().startsWith("d5c7"));

        PositionSearchResult lateResult = checkPerformance(latePosition, 5);
        assertTrue(lateResult.getBestMovePath().toString().startsWith("a4a5"));

        long took = earlyResult.took() + midResult.took() + lateResult.took();
        System.out.println("total time: " + took + "ms");
    }

    private PositionSearchResult checkPerformance(Position position, int countdownLatch) throws InterruptedException {
        PiecePositionEvaluator evaluator = new PiecePositionEvaluator();
        IterativePositionSearcher positionSearcher = new IterativePositionSearcher(evaluator, 0);
        CountDownLatch latch = new CountDownLatch(countdownLatch);
        AtomicReference<PositionSearchResult> resultRef = new AtomicReference<>();
        TimedTaskRunner taskRunner = new TimedTaskRunner();
        positionSearcher.setListener(new IterativePositionSearcher.IterativePositionSearcherListener() {
            @Override
            public void onResult(PositionSearchResult result) {
                resultRef.set(result);
                latch.countDown();
            }

            @Override
            public void onFinished(IterativePositionSearchResult result) {
            }
        });
        positionSearcher.search(position, new LinkedList<>());
        long startTime = System.currentTimeMillis();
        taskRunner.start(positionSearcher);
        latch.await();
        System.out.println("took: " + (System.currentTimeMillis() - startTime) + "ms");
        System.out.println("nodes: " + FixedPositionSearcher.NODE_COUNTER.get());
        System.out.println("best move: " + resultRef.get().getBestMovePath().toString());
        taskRunner.shutdown();
        return resultRef.get();
    }

    /**
     * One million entries seems to be the best
     * <p>
     * 10m
     * took 38s
     * memory usage 3148MB
     * <p>
     * 1m
     * took 40s
     * memory usage 2699MB
     * <p>
     * 100k
     * took 50s
     * memory usage 1827MB
     * <p>
     * off
     * took 75s
     * memory usage 1918MB
     */
    @Disabled
    @Test
    public void cachePerformanceTest() throws PositionSearcherCanceledException {
        Position position = PositionUtil.base();
        MoveExecutor moveExecutor = new MoveExecutor();
        moveExecutor.forward(position, "e2e4 b8c6 g1f3 f7f6 d2d4 g7g6 b1c3 h7h6 f1b5 a7a5 a2a4 f8g7 e1g1 a8b8 h2h3 e7e5 d4e5 c6a7 c1e3 b7b6 c3d5 c7c6 b5c6");
        CLIRenderer.render(position);

        FixedPositionSearcher evaluator = build(8, 18, 1_000_000);
        long sTime = System.currentTimeMillis();
        evaluate(position, evaluator);
        System.out.println("took " + ((System.currentTimeMillis() - sTime) / 1000) + "s");
        EngineUtil.printMemoryUsage();
    }

    private FixedPositionSearcher build(int fullNodes, int quiescenceNodes) {
        return build(fullNodes, quiescenceNodes, 1_000_000);
    }

    private FixedPositionSearcher build(int fullNodes, int quiescenceNodes, int cacheSize) {
        PositionSearcherSettings settings = new PositionSearcherSettings(fullNodes, quiescenceNodes, 0);
        return new FixedPositionSearcher(settings, new DefaultPositionEvaluator(), new PositionSearchCache(cacheSize));
    }

    private FixedPositionSearchResult evaluate(Position position, FixedPositionSearcher evaluator) throws PositionSearcherCanceledException {
        MoveExecutor moveExecutor = new MoveExecutor();
        FixedPositionSearchResult result = evaluator.search(position);
        Move bestMove = result.getBestMovePath().head().getMove();
        moveExecutor.forward(position, bestMove);
        return result;
    }

}
