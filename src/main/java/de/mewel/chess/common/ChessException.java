package de.mewel.chess.common;

public class ChessException extends RuntimeException {

    public ChessException(String message, Throwable cause) {
        super(message, cause);
    }

    public ChessException(String message) {
        super(message);
    }

}
