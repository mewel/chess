package de.mewel.chess.engine.model;

import de.mewel.chess.common.Move;
import de.mewel.chess.common.MoveExecutor;
import de.mewel.chess.common.PositionUtil;
import de.mewel.chess.model.Position;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class Game {

    private final MoveExecutor moveExecutor;

    private Position position;

    private PositionSearchResult lastResult;

    private LinkedList<Long> positionHashList;

    private List<Move> moveList;

    public Game() {
        this.moveExecutor = new MoveExecutor();
        this.position = PositionUtil.base();
        this.positionHashList = new LinkedList<>();
        this.moveList = new ArrayList<>();
        this.lastResult = null;
    }

    public Position getPosition() {
        return position;
    }

    public MovePath play(PositionSearchResult result, int variance) {
        this.lastResult = result;
        if (!result.isGameFinished()) {
            MovePath movePath = variance == 0 ? result.getBestMovePath() : result.getMovePath(variance);
            MoveNode head = movePath.head();
            move(head.getMove());
            return movePath;
        }
        return null;
    }

    public void play(String line) {
        if (line.length() >= 4 && line.length() <= 6) {
            move(position.move(line));
        } else if (line.length() > 5) {
            moves(position, line);
        }
    }

    private void move(Move move) {
        this.moveExecutor.forward(position, move);
        this.positionHashList.add(position.longHashCode());
        this.moveList.add(move);
    }

    private void moves(Position position, String moves) {
        Arrays.stream(moves.split(" ")).map(position::move).forEach(this::move);
    }

    public PositionSearchResult getLastEvaluatorResult() {
        return lastResult;
    }

    public List<Move> getMoves() {
        return this.moveList;
    }

    public List<String> movesToList() {
        return getMoves().stream()
                .map(Move::toString)
                .collect(Collectors.toList());
    }

    public String movesToString() {
        return getMoves().stream()
                .map(Move::toString)
                .collect(Collectors.joining(" "));
    }

    public LinkedList<Long> getPositionHashList() {
        return positionHashList;
    }

    /**
     * Copy's the game except the lastResult variable.
     *
     * @return copy of this game object
     */
    public Game copy() {
        Game game = new Game();
        game.position = this.position.copy();
        game.lastResult = this.lastResult;
        game.positionHashList = new LinkedList<>(this.positionHashList);
        game.moveList = this.moveList.stream().map(Move::copy).collect(Collectors.toList());
        return game;
    }

    public static Game fen(String fen) {
        Game game = new Game();
        game.position = PositionUtil.fen(fen);
        return game;
    }
}
