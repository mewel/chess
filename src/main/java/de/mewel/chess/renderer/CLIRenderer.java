package de.mewel.chess.renderer;

import de.mewel.chess.model.PieceNotation;
import de.mewel.chess.model.Position;

public class CLIRenderer {

    public static final String[] SYMBOLS = {
            "♔", // white king
            "♕", // white queen
            "♖", // white rook
            "♗", // white bishop
            "♘", // white knight
            "♙", // white pawn
            "♚", // black king
            "♛", // black queen
            "♜", // black rook
            "♝", // black bishop
            "♞", // black knight
            "♟", // black pawn
    };

    public static String get(Position position) {
        StringBuilder s = new StringBuilder();
        for (int y = 7; y >= 0; y--) {
            for (int x = 0; x < 8; x++) {
                byte piece = position.get(x, y);
                String symbol = getSymbol(piece);
                s.append(symbol).append(" ");
            }
            s.append("\n");
        }
        return s.toString();
    }

    public static void render(Position position) {
        System.out.print(get(position));
        System.out.println();
    }

    private static String getSymbol(int piece) {
        if (piece == PieceNotation.EMPTY) {
            return "  ";
        }
        return SYMBOLS[piece];
    }

}
