package de.mewel.chess.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class PositionMeta {

    /**
     * Turn number.
     */
    private int turn;

    /**
     * true = white's turn
     * false = black's turn
     */
    private boolean white;

    /**
     * 0 = white cannot castle
     * 1 = white can castle short
     * 2 = white can castle long
     * 3 = white can castle both sides
     */
    private byte castleWhite;

    /**
     * 0 = black cannot castle
     * 1 = black can castle short
     * 2 = black can castle long
     * 3 = black can castle both sides
     */
    private byte castleBlack;

    /**
     * Defines the x-Axis where a pawn is moved two squares in
     * the previous position.
     * <p>
     * -1 = no pawn moved two squares
     * 0-7 = the pawn on this x-Axis moved two squares (possible en passant)
     */
    private byte twoSquarePawn;

    /**
     * false = not in check
     * true = the other player is in check (if this is whites turn, then black is in check)
     */
    private List<Piece> checkingPieces;

    private byte fiftyMoveRule;

    public PositionMeta() {
        this.white = true;
        this.turn = 1;
        this.castleWhite = 3;
        this.castleBlack = 3;
        this.twoSquarePawn = -1;
        this.checkingPieces = new ArrayList<>();
        this.fiftyMoveRule = 0;
    }

    public void setWhite(boolean white) {
        this.white = white;
    }

    public void setCastleWhite(byte castleWhite) {
        this.castleWhite = castleWhite;
    }

    public void setCastleBlack(byte castleBlack) {
        this.castleBlack = castleBlack;
    }

    public void setTwoSquarePawn(byte twoSquarePawn) {
        this.twoSquarePawn = twoSquarePawn;
    }

    public void setCheckingPieces(List<Piece> checkingPieces) {
        this.checkingPieces = checkingPieces;
    }

    public void setTurn(int turn) {
        this.turn = turn;
    }

    public void setFiftyMoveRule(byte turn) {
        this.fiftyMoveRule = turn;
    }

    public boolean isWhite() {
        return white;
    }

    public int getTurn() {
        return turn;
    }

    public byte getCastleWhite() {
        return castleWhite;
    }

    public byte getCastleBlack() {
        return castleBlack;
    }

    public byte getTwoSquarePawn() {
        return twoSquarePawn;
    }

    public byte getFiftyMoveRule() {
        return fiftyMoveRule;
    }

    public boolean isInCheck() {
        return !this.checkingPieces.isEmpty();
    }

    public List<Piece> getCheckingPieces() {
        return this.checkingPieces;
    }

    public void nextTurn(boolean pawnOrCapture) {
        this.white = !this.white;
        this.turn++;
        if (!pawnOrCapture) {
            this.fiftyMoveRule++;
        } else {
            this.fiftyMoveRule = 0;
        }
    }

    public void previousTurn() {
        this.white = !this.white;
        this.turn--;
    }

    public PositionMeta copy() {
        PositionMeta meta = new PositionMeta();
        meta.white = this.white;
        meta.turn = this.turn;
        meta.castleWhite = this.castleWhite;
        meta.castleBlack = this.castleBlack;
        meta.twoSquarePawn = this.twoSquarePawn;
        meta.checkingPieces = this.checkingPieces;
        meta.fiftyMoveRule = this.fiftyMoveRule;
        return meta;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PositionMeta that = (PositionMeta) o;
        return turn == that.turn &&
                white == that.white &&
                castleWhite == that.castleWhite &&
                castleBlack == that.castleBlack &&
                twoSquarePawn == that.twoSquarePawn &&
                fiftyMoveRule == that.fiftyMoveRule &&
                Objects.equals(checkingPieces, that.checkingPieces);
    }

    @Override
    public int hashCode() {
        return Objects.hash(turn, white, castleWhite, castleBlack, twoSquarePawn, fiftyMoveRule, checkingPieces);
    }

}
