package de.mewel.chess.engine;

import de.mewel.chess.common.PositionUtil;
import de.mewel.chess.engine.evaluator.PiecePositionEvaluator;
import de.mewel.chess.engine.model.*;
import de.mewel.chess.model.Position;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import java.util.LinkedList;
import java.util.concurrent.CountDownLatch;

import static org.junit.jupiter.api.Assertions.*;

public class PositionSearcherTest {

    @Test
    public void iterativeSearch() throws Exception {
        PiecePositionEvaluator evaluator = new PiecePositionEvaluator();
        IterativePositionSearcher positionSearcher = new IterativePositionSearcher(evaluator, 0);
        Position position = PositionUtil.fen("4kb1r/5pp1/5n1p/1qp5/3ppP1P/6PN/3BP3/Q3K2R b Kk - 1 24");
        CountDownLatch latch = new CountDownLatch(1);
        TimedTaskRunner taskRunner = new TimedTaskRunner();
        long startTime = System.currentTimeMillis();
        positionSearcher.search(position, new LinkedList<>());
        positionSearcher.setListener(new IterativePositionSearcher.IterativePositionSearcherListener() {
            @Override
            public void onResult(PositionSearchResult result) {

            }

            @Override
            public void onFinished(IterativePositionSearchResult result) {
                long endTime = System.currentTimeMillis();
                float timeInSec = (endTime - startTime) / 1000f;
                System.out.println(result.getBestMovePath());
                System.out.println((FixedPositionSearcher.NODE_COUNTER.get() / timeInSec) + " N/s");
                System.out.println(("took " + result.took() + "ms"));
                latch.countDown();
            }
        });
        taskRunner.start(positionSearcher, 1000);
        latch.await();
        taskRunner.shutdown();
    }

    @Test
    public void fixedSearch() throws PositionSearcherCanceledException {
        PositionSearcherSettings settings = new PositionSearcherSettings(4, 12, 0);
        PiecePositionEvaluator evaluator = new PiecePositionEvaluator();
        FixedPositionSearcher positionSearcher = new FixedPositionSearcher(settings, evaluator, new PositionSearchCache(1_000_000));
        Position position = PositionUtil.fen("4kb1r/5pp1/5n1p/1qp5/3ppP1P/6PN/3BP3/Q3K2R b Kk - 1 24");
        long startTime = System.currentTimeMillis();
        FixedPositionSearchResult result = positionSearcher.search(position, new LinkedList<>());
        long endTime = System.currentTimeMillis();
        float timeInSec = (endTime - startTime) / 1000f;
        System.out.println(result.getBestMovePath());
        System.out.println((FixedPositionSearcher.NODE_COUNTER.get() / timeInSec) + " N/s");
        System.out.println(("took " + result.took() + "ms"));
    }

    @Test
    public void playTest() throws PositionSearcherCanceledException {
        PositionSearcherSettings settings = new PositionSearcherSettings(4, 12, 0);
        PiecePositionEvaluator evaluator = new PiecePositionEvaluator();
        FixedPositionSearcher positionSearcher = new FixedPositionSearcher(settings, evaluator, new PositionSearchCache(1_000_000));
        long startTime = System.currentTimeMillis();
        Position position = PositionUtil.fen("r1b2b2/1pk3p1/3p4/p2P2N1/2P1P2P/7R/5P2/3K1B2 w - - 0 31");
        FixedPositionSearchResult result = positionSearcher.search(position, new LinkedList<>());
        System.out.println(result.getBestMovePath());
        System.out.println(("took " + (System.currentTimeMillis() - startTime) + "ms"));
    }

    @Test
    public void quiescenceTest() throws PositionSearcherCanceledException {
        PositionSearcherSettings settings = new PositionSearcherSettings(0, 4, 0);
        FixedPositionSearcher positionSearcher = new FixedPositionSearcher(settings, new PiecePositionEvaluator(), new PositionSearchCache(1_000_000));
        Position position = PositionUtil.fen("8/kp6/p5r1/8/8/P3N3/KP4r1/7B w - - 0 1");
        positionSearcher.setCalculateAllRootLevelNodes(true);
        FixedPositionSearchResult result = positionSearcher.search(position, new LinkedList<>());
        assertEquals(2, result.getMoveMap().size());
        MovePath movePath = result.getMovePath(10);
        assertNotNull(movePath);
    }

    @Test
    public void quiescenceTest2() throws PositionSearcherCanceledException {
        PositionSearcherSettings settings = new PositionSearcherSettings(0, 2, 0);
        FixedPositionSearcher positionSearcher = new FixedPositionSearcher(settings, new PiecePositionEvaluator(), new PositionSearchCache(1_000_000));
        Position position = PositionUtil.fen("8/1kpp4/1p2r3/8/5N2/6P1/4PPK1/8 w - - 0 1");
        FixedPositionSearchResult result = positionSearcher.search(position, new LinkedList<>());
        assertEquals("f4e6 d7e6", result.getBestMovePath().toString());
    }

    @Test
    public void quiescenceTest3() throws PositionSearcherCanceledException {
        PositionSearcherSettings settings = new PositionSearcherSettings(0, 2, 0);
        FixedPositionSearcher positionSearcher = new FixedPositionSearcher(settings, new PiecePositionEvaluator(), new PositionSearchCache(1_000_000));
        Position position = PositionUtil.fen("8/1kpp4/1p2p3/8/5N2/6P1/4PPK1/8 w HAha - 0 1");
        FixedPositionSearchResult result = positionSearcher.search(position, new LinkedList<>());
        assertNull(result.getBestMovePath());
    }

    @Test
    public void quiescenceTest4() throws PositionSearcherCanceledException {
        PositionSearcherSettings settings = new PositionSearcherSettings(0, 2, 0);
        FixedPositionSearcher positionSearcher = new FixedPositionSearcher(settings, new PiecePositionEvaluator(), new PositionSearchCache(1_000_000));
        Position position = PositionUtil.fen("8/1kpp4/1p2r3/8/5N2/1Q4P1/4PPK1/8 w HAha - 0 1");
        FixedPositionSearchResult result = positionSearcher.search(position, new LinkedList<>());
        assertEquals("f4e6 d7e6", result.getBestMovePath().toString());
    }

    @RepeatedTest(20)
    public void whiteShouldNeverWin() throws PositionSearcherCanceledException {
        PositionSearcherSettings settings = new PositionSearcherSettings(2, 4, 15);
        PiecePositionEvaluator evaluator = new PiecePositionEvaluator();
        FixedPositionSearcher positionSearcher = new FixedPositionSearcher(settings, evaluator, new PositionSearchCache(1_000_000));
        Game game = Game.fen("8/4p2p/p7/1p6/2k5/P1P4R/1q6/6K1 b - - 9 48");
        for (int i = 0; i < 200; i++) {
            PositionSearchResult result = positionSearcher.search(game.getPosition(), game.getPositionHashList());
            game.play(result, settings.variance());
            if (result.isGameFinished()) {
                if (result.isDraw() || result.hasWhiteWon()) {
                    System.out.println(game.movesToString());
                }
                assertTrue(result.hasBlackWon(), "black should win but " + result.getGameStatus());
                return;
            }
        }
        System.out.println(game.movesToString());
        fail("couldn't mate in 200 turns: " + game.movesToString());
    }

    @Test
    public void threeFoldRepetitionTest() throws PositionSearcherCanceledException {
        Game game = Game.fen("8/4p2p/p7/1p6/2k5/P1P4R/1q6/6K1 b - - 9 48");
        String moves = "b2c1 g1f2 c1f4 f2e2 f4g4 h3f3 c4b3 e2f2 g4h4 f2g1 h4g4 g1f2 g4h4 f2g1";
        game.play(moves);
        PositionSearcherSettings settings = new PositionSearcherSettings(2, 4, 0);
        PiecePositionEvaluator evaluator = new PiecePositionEvaluator();
        FixedPositionSearcher positionSearcher = new FixedPositionSearcher(settings, evaluator, new PositionSearchCache(1_000_000));
        for (int i = 0; i < 200; i++) {
            PositionSearchResult result = positionSearcher.search(game.getPosition(), game.getPositionHashList());
            game.play(result, settings.variance());
            if (result.isGameFinished()) {
                System.out.println(game.movesToString().substring(moves.length()));
                assertTrue(result.hasBlackWon(), "black should win but " + result.getGameStatus());
                return;
            }
        }
        fail("couldn't mate in 200 turns");
    }

    @Test
    public void threeFoldRepetitionTest2() throws PositionSearcherCanceledException {
        Game game = new Game();
        game.play("b1a3 b8c6 d2d4 e7e5 d4e5 f8b4 c1d2 b4a3 b2a3 c6e5 a1b1 d7d5 d2c3 d8d6 f2f4 e5c4 c3g7 d6f4 g7h8 c4e3 g1h3 c8h3 d1d2 f4h4 g2g3 h4e4 h1g1 e3c2 e1f2 c2a3 b1b7 h3f1 g1f1 c7c5 b7c7 c5c4 d2a5 a3c2 a5a4 e8f8 f1b1 a7a5 c7b7 a8e8 a4a5 e4e2 f2g1 e2e3 g1g2 e3e2 g2g1 e2e3 g1g2 e3e2 g2g1");
        PositionSearcherSettings settings = new PositionSearcherSettings(4, 8, 0);
        PiecePositionEvaluator evaluator = new PiecePositionEvaluator();
        FixedPositionSearcher positionSearcher = new FixedPositionSearcher(settings, evaluator, new PositionSearchCache(1_000_000));
        PositionSearchResult result = positionSearcher.search(game.getPosition(), game.getPositionHashList());
        assertTrue(result.isDraw(), "game should be draw after threefold repetition check");
    }

    @Test
    public void threeFoldRepetitionTest3() throws PositionSearcherCanceledException {
        Game game = new Game();
        game.play("b1a3 b8c6 d2d4 e7e5 d4e5 f8b4 c1d2 b4a3 b2a3 c6e5 a1b1 d7d5 d2c3 d8d6 f2f4 e5c4 c3g7 d6f4 g7h8 c4e3 g1h3 c8h3 d1d2 f4h4 g2g3 h4e4 h1g1 e3c2 e1f2 c2a3 b1b7 h3f1 g1f1 c7c5 b7c7 c5c4 d2a5 a3c2 a5a4 e8f8 f1b1 a7a5 c7b7 a8e8 a4a5 e4e2 f2g1 e2e3 g1g2 e3e2 g2g1 e2e3 g1g2 e3e2 g2g1");
        PositionSearcherSettings settings = new PositionSearcherSettings(4, 8, 0);
        PiecePositionEvaluator evaluator = new PiecePositionEvaluator();
        FixedPositionSearcher positionSearcher = new FixedPositionSearcher(settings, evaluator, new PositionSearchCache(1_000_000));
        PositionSearchResult result = positionSearcher.search(game.getPosition(), game.getPositionHashList());
        assertNotNull(result.getBestMovePath());
    }

    @Test
    public void threeFoldRepetitionTest4() throws PositionSearcherCanceledException {
        Game game = new Game();
        game.play("e2e4 e7e5 f1c4 g8f6 g1f3 f6e4 f3e5 d7d5 c4b3 d8g5 b3a4 c7c6 d2d4 g5g2 d1f3 g2f3 e5f3 b7b5 a4b3 c8g4 f3e5 g4h3 b1d2 e4d6 h1g1 d6f5 c2c3 g7g6 b3c2 f8g7 c2f5 h3f5 h2h4 e8g8 b2b4 f8e8 e1d1 g7e5 d4e5 b8d7 f2f4 f7f6 d2f3 f6e5 h4h5 a7a6 f3h4 f5e4 f4f5 d7b6 h5g6 b6c4 d1e1 g8g7 g6h7 g7h7 f5f6 e8g8 c1g5 a8b8 a2a4 b8f8 a4b5 a6b5 a1a7 h7h8 f6f7 g8g5 g1g5 h8h7 a7b7 c4b2 e1d2 b2c4 d2c1 c4d6 b7e7 f8f7 g5e5 h7g7 e7f7 g7f7 e5h5 f7g7 h4g2 d6c4 g2f4 g7f6 h5h2 c4e5 f4e2 e5g4 h2h4 g4e5 e2d4 f6g7 c1b2 g7g6 b2a1 e5f3 h4g4 g6h5 g4g8 f3e5 g8e8 e5f3 d4c6 h5g5 e8f8 f3e1 c6d4 e1c2 a1b2 c2d4 c3d4 e4d3 b2c3 d3e2 f8d8 e2c4 d8f8 g5g6 f8f4 c4e2 f4f8 e2c4 f8f4 c4e2 c3c2 e2c4 f4f2 g6g5 c2c3 g5g4 f2f6 g4g5 f6f7 g5g4 f7f8 g4g5 f8f7 g5g4 f7f8 c4e2 f8f6 e2c4 f6f7 g4g5 f7f3 g5g4 f3f6 g4g5 f6f3 g5g4 f3f2 g4g5 c3d2 g5g4 f2f8 g4g5 f8f7 c4b3 f7f8");
        PositionSearcherSettings settings = new PositionSearcherSettings(4, 8, 0);
        PiecePositionEvaluator evaluator = new PiecePositionEvaluator();
        FixedPositionSearcher positionSearcher = new FixedPositionSearcher(settings, evaluator, new PositionSearchCache(1_000_000));
        PositionSearchResult result = positionSearcher.search(game.getPosition(), game.getPositionHashList());
        assertNotNull(result.getBestMovePath());
    }

    @Test
    public void stalemateTest() throws PositionSearcherCanceledException {
        Game game = Game.fen("5k2/3Q4/6R1/8/8/8/8/1K6 b - - 0 54");
        PositionSearcherSettings settings = new PositionSearcherSettings(2, 4, 0);
        PiecePositionEvaluator evaluator = new PiecePositionEvaluator();
        FixedPositionSearcher positionSearcher = new FixedPositionSearcher(settings, evaluator, new PositionSearchCache(1_000_000));
        PositionSearchResult result = positionSearcher.search(game.getPosition(), game.getPositionHashList());
        assertTrue(result.isDraw());
        assertEquals(PositionSearchResult.DrawType.STALEMATE, result.getDrawType());
    }

    @Test
    public void noStalemateTest() throws PositionSearcherCanceledException {
        Game game = Game.fen("8/Q7/3p4/3P4/2k1B2p/1p5P/1K4P1/8 w - - 0 54");
        PositionSearcherSettings settings = new PositionSearcherSettings(2, 4, 0);
        PiecePositionEvaluator evaluator = new PiecePositionEvaluator();
        FixedPositionSearcher positionSearcher = new FixedPositionSearcher(settings, evaluator, new PositionSearchCache(1_000_000));
        PositionSearchResult result = positionSearcher.search(game.getPosition(), game.getPositionHashList());
        assertFalse(result.getBestMovePath().toString().startsWith("a7b6"));
    }

    @Test
    public void noStalemateTest2() throws PositionSearcherCanceledException {
        Game game = Game.fen("1q6/4p3/p7/k7/8/3q4/6Kp/8 w - - 2 83");
        PositionSearcherSettings settings = new PositionSearcherSettings(2, 4, 0);
        PiecePositionEvaluator evaluator = new PiecePositionEvaluator();
        FixedPositionSearcher positionSearcher = new FixedPositionSearcher(settings, evaluator, new PositionSearchCache(1_000_000));
        PositionSearchResult result = positionSearcher.search(game.getPosition(), game.getPositionHashList());
        assertFalse(result.isGameFinished());
    }

    @Test
    public void whiteIsMatedTest() throws PositionSearcherCanceledException {
        Game game = Game.fen("8/4p2p/p2q4/4K3/5R2/k7/8/1q6 w - - 4 86");
        PositionSearcherSettings settings = new PositionSearcherSettings(2, 4, 0);
        PiecePositionEvaluator evaluator = new PiecePositionEvaluator();
        FixedPositionSearcher positionSearcher = new FixedPositionSearcher(settings, evaluator, new PositionSearchCache(1_000_000));
        PositionSearchResult result = positionSearcher.search(game.getPosition(), game.getPositionHashList());
        assertTrue(result.isGameFinished(), "game should finish but " + result.getGameStatus());
        assertTrue(result.hasBlackWon(), "black should win but " + result.getGameStatus());
    }

    /*

    b2c1 g1f2 c1f4 f2e2 f4g4 h3f3 c4b3 e2f2 g4h4 f2g1 h4g4 g1f2 g4h4 f2g1 h4g5 g1h2 g5d2 h2g1 d2d1 f3f1 d1g4 g1h2 g4e2 h2g1 e2g4 g1h2 g4e2 h2g1 e2e3 g1g2 e3e4 g2g3 e4d3 f1f3 d3g6 g3h2 g6d6 h2g1 d6g6 g1h2 b3a3 c3c4 a3a4 c4c5 g6h5 f3h3 h5c5 h3h4 b5b4 h2h1 c5d5 h1h2 d5e5 h2h3 e5c3 h3g2 c3d2 g2f3 d2c3 f3e2 c3e5 e2d1 h7h5 d1c2 a4b5 c2d2 e5g5 d2d3 g5h4 d3c2 h4g3 c2b2 b4b3 b2c1 h5h4 c1b2 h4h3 b2c1 h3h2 c1b1 g3g2 b1a1 g2a2
    b2c1 g1f2 c1f4 f2e2 f4g4 h3f3 c4b3 e2f2 g4h4 f2g1 h4g4 g1f2 g4h4 f2g1 h4g5 g1h2 g5d2 h2g1 d2d1 f3f1 d1g4 g1h2 g4e2 h2g1 e2g4 g1h2 g4e2 h2g1 e2e3 g1g2 e3e4 g2g3 e4d3 f1f3 d3g6 g3h2 g6d6 h2g1 d6g6 g1h2 b3a3 c3c4 a3a4 c4c5 g6h5 f3h3 h5c5 h3h4 b5b4 h2h1 c5d5 h1h2 d5e5 h2h3 e5c3 h3g2 c3d2 g2f1 d2d3 f1f2 a4a3 h4g4 d3f5 f2g3 b4b3 g4d4 f5e5 d4f4 b3b2 g3f3 e5c3 f3e4 b2b1q e4d5 c3d2 d5e5 d2d6 e5d6


     */

}
