package de.mewel.chess.model;

import de.mewel.chess.common.Move;
import de.mewel.chess.common.PositionUtil;

import java.util.ArrayList;
import java.util.List;

public class Rook extends AbstractPiece {

    public Rook(boolean white, int x, int y) {
        super(white, x, y);
    }

    public String getName() {
        return "rook";
    }

    public short getValue() {
        return 5;
    }

    @Override
    public byte toByte() {
        return isWhite() ? PieceNotation.W_ROOK : PieceNotation.B_ROOK;
    }

    @Override
    public List<Move> moves(Position position) {
        return PositionUtil.moveHorizontalAndVertical(position, x(), y());
    }

    @Override
    public List<Move> validMoves(Position position) {
        return moves(position);
    }


    @Override
    public List<Move> captures(Position position) {
        return PositionUtil.captureHorizontalAndVertical(position, x(), y());
    }

    @Override
    public List<Move> pinnedMoves(Position position, King king, Piece pinningPiece) {
        ArrayList<Move> moves = new ArrayList<>();
        int pinningPieceDiffX = Math.abs(x() - pinningPiece.x());
        int pinningPieceDiffY = Math.abs(y() - pinningPiece.y());
        if (pinningPieceDiffX != 0 && pinningPieceDiffY != 0) {
            return moves; // diagonally pinned
        }
        // horizontally pinned
        // - move in pinning piece direction
        int moveX = Integer.compare(pinningPiece.x(), x());
        int moveY = Integer.compare(pinningPiece.y(), y());
        int pinningPieceDiff = Math.max(pinningPieceDiffX, pinningPieceDiffY);
        for (int i = 1; i <= pinningPieceDiff; i++) {
            PositionUtil.moveOrCapture(moves, position, x(), y(), x() + i * moveX, y() + i * moveY);
        }
        // - move in king direction
        int kingPieceDiffX = Math.abs(x() - king.x());
        int kingPieceDiffY = Math.abs(y() - king.y());
        int kingPieceDiff = Math.max(kingPieceDiffX, kingPieceDiffY);
        for (int i = 1; i < kingPieceDiff; i++) {
            if(!PositionUtil.move(moves, position, x(), y(), x() + i * moveX * -1, y() + i * moveY  * -1)) {
                break;
            }
        }
        return moves;
    }

    @Override
    public List<Move> inCheckMoves(Position position) {
        List<Move> moves = new ArrayList<>();
        List<Piece> checkingPieces = position.getCheckingPieces();
        if (checkingPieces.size() > 1) {
            return moves;
        }
        Piece checkingPiece = checkingPieces.get(0);
        King king = PositionUtil.getKing(position, position.isWhite());
        PositionUtil.hvIntersectAndCapture(position, this, checkingPiece, king, moves);
        return moves;
    }

    @Override
    public byte preventsCastling(Position position) {
        return PositionUtil.checkCastleVertical(position, x(), y());
    }

}
