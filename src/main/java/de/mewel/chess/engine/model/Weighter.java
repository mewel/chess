package de.mewel.chess.engine.model;

public interface Weighter {

    int weight(WeighterMeta meta);

}
