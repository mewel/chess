package de.mewel.chess.common;

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

public class OpeningParserTest {

    @Test
    public void parse() throws IOException {
        OpeningParser parser = new OpeningParser();
        Path path = Path.of("src/test/resources/openings.csv");
        List<Opening> openings = parser.parse(path);
        Assert.assertFalse("The openings list should not be empty", openings.isEmpty());
        Assert.assertEquals("There should be 3 openings", 3, openings.size());
        Assert.assertEquals("The first opening should be Ruy Lopez", "English Opening", openings.get(0).getName());
        Assert.assertEquals("The first opening should have 4 moves", 4, openings.get(0).getMoves().size());
    }


}
