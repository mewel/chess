package de.mewel.chess;

import org.junit.Test;

import java.io.IOException;

public class EngineUCITest {

    @Test
    public void handleLine() throws Exception{
        EngineUCI uci = new EngineUCI();
        uci.handleLine("uci");
        uci.handleLine("ucinewgame");
        uci.handleLine("go wtime 1000 btime 1000");
        Thread.sleep(1000);
    }

    @Test
    public void threeFoldRepetition() throws IOException, InterruptedException {
        EngineUCI uci = new EngineUCI();
        uci.handleLine("position startpos moves e2e4 e7e5 f1c4 g8f6 g1f3 f6e4 f3e5 d7d5 c4b3 d8g5 b3a4 c7c6 d2d4 g5g2 d1f3 g2f3 e5f3 b7b5 a4b3 c8g4 f3e5 g4h3 b1d2 e4d6 h1g1 d6f5 c2c3 g7g6 b3c2 f8g7 c2f5 h3f5 h2h4 e8g8 b2b4 f8e8 e1d1 g7e5 d4e5 b8d7 f2f4 f7f6 d2f3 f6e5 h4h5 a7a6 f3h4 f5e4 f4f5 d7b6 h5g6 b6c4 d1e1 g8g7 g6h7 g7h7 f5f6 e8g8 c1g5 a8b8 a2a4 b8f8 a4b5 a6b5 a1a7 h7h8 f6f7 g8g5 g1g5 h8h7 a7b7 c4b2 e1d2 b2c4 d2c1 c4d6 b7e7 f8f7 g5e5 h7g7 e7f7 g7f7 e5h5 f7g7 h4g2 d6c4 g2f4 g7f6 h5h2 c4e5 f4e2 e5g4 h2h4 g4e5 e2d4 f6g7 c1b2 g7g6 b2a1 e5f3 h4g4 g6h5 g4g8 f3e5 g8e8 e5f3 d4c6 h5g5 e8f8 f3e1 c6d4 e1c2 a1b2 c2d4 c3d4 e4d3 b2c3 d3e2 f8d8 e2c4 d8f8 g5g6 f8f4 c4e2 f4f8 e2c4 f8f4 c4e2 c3c2 e2c4 f4f2 g6g5 c2c3 g5g4 f2f6 g4g5 f6f7 g5g4 f7f8 g4g5 f8f7 g5g4 f7f8 c4e2 f8f6 e2c4 f6f7 g4g5 f7f3 g5g4 f3f6 g4g5 f6f3 g5g4 f3f2 g4g5 c3d2 g5g4 f2f8 g4g5 f8f7 c4b3 f7f8");
        uci.handleLine("isready");
        uci.handleLine("go wtime 114147 btime 142020 movestogo 39");
        Thread.sleep(1000);
    }

}
