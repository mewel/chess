package de.mewel.chess;

import de.mewel.chess.common.Opening;
import de.mewel.chess.common.OpeningBook;
import de.mewel.chess.common.OpeningParser;
import de.mewel.chess.engine.IterativePositionSearcher;
import de.mewel.chess.engine.TimedTaskRunner;
import de.mewel.chess.engine.evaluator.PiecePositionEvaluator;
import de.mewel.chess.engine.evaluator.PositionEvaluator;
import de.mewel.chess.engine.model.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

public class EngineUCI {

    private static final int VARIANCE = 10;

    private static final String VERSION = "0.3.5";

    private final OpeningBook openingBook;

    private Game game;

    private final TimedTaskRunner timedTaskRunner;

    private final IterativePositionSearcher searcher;

    public EngineUCI() throws IOException {
        PositionEvaluator analyzer = new PiecePositionEvaluator();
        this.game = new Game();
        this.searcher = new IterativePositionSearcher(analyzer, VARIANCE);
        this.searcher.setListener(new IterativePositionSearcher.IterativePositionSearcherListener() {
            @Override
            public void onResult(PositionSearchResult result) {
                onInterimSearchResult(result);
            }

            @Override
            public void onFinished(IterativePositionSearchResult result) {
                onSearchFinished(result);
            }
        });
        this.timedTaskRunner = new TimedTaskRunner();
        OpeningParser openingParser = new OpeningParser();
        try (InputStream openingsStream = EngineUCI.class.getResourceAsStream("/openings.csv")) {
            List<Opening> openings = openingParser.parse(openingsStream);
            this.openingBook = new OpeningBook(openings);
        }
    }

    public void run() throws IOException {
        InputStreamReader streamReader = new InputStreamReader(System.in);
        BufferedReader bufferedReader = new BufferedReader(streamReader);
        String line;
        do {
            line = bufferedReader.readLine();
            if (line == null) {
                continue;
            }
            line = handleLine(line);
        } while (line != null && !line.equals("quit"));
    }

    public String handleLine(String line) {
        if (line.equals("uci")) {
            sendIdName();
            sendIdAuthor();
            sendUCIOk();
        } else if (line.equals("isready")) {
            sendReadyOk();
        } else if (line.equals("ucinewgame")) {
            game = new Game();
        } else if (line.startsWith("position")) {
            String[] lineParts = line.split(" ");
            if (lineParts[1].equals("startpos")) {
                game = new Game();
            } else if (lineParts[1].equals("fen")) {
                List<String> uciParameters = getUCIParameters(line);
                String fen = getUCIParameter(uciParameters, "fen", null);
                game = Game.fen(fen);
            }
            if (line.contains("moves")) {
                line = line.substring(line.indexOf("moves") + 6);
                game.play(line);
            }
        } else if (line.startsWith("go")) {
            go(line);
        } else if (line.startsWith("stop")) {
            if (timedTaskRunner.isRunning()) {
                timedTaskRunner.cancel();
            }
        } else if (line.startsWith("quit")) {
            if (timedTaskRunner.isRunning()) {
                timedTaskRunner.cancel();
            }
            timedTaskRunner.shutdown();
        }
        return line;
    }

    private void go(String line) {
        List<String> uciParameters = getUCIParameters(line);
        Long wTime = getLongUCIParameter(uciParameters, "wtime", 1000L);
        Long bTime = getLongUCIParameter(uciParameters, "btime", 1000L);
        Integer turn = game.getPosition().getTurn();
        if (turn <= 4) {
            // use opening book
            String randomMove = this.openingBook.getRandomMove(game.movesToList());
            if (randomMove != null) {
                play(randomMove);
                return;
            }
        }
        // calculate
        long maxTimePerTurn = (game.getPosition().isWhite() ? wTime : bTime) / 25;
        searcher.search(game.getPosition(), game.getPositionHashList());
        timedTaskRunner.start(searcher, maxTimePerTurn);
    }

    void onInterimSearchResult(PositionSearchResult result) {
        MovePath bestMovePath = result.getBestMovePath();
        if(bestMovePath != null) {
            int uciScore = toUCIScore(result.isWhite(), bestMovePath.getValue());
            System.out.println("info score cp " + uciScore + " time " + result.took() + " pv " + bestMovePath);
        } else {
            System.out.println("info string error: best move == null");
        }
    }

    void onSearchFinished(IterativePositionSearchResult result) {
        play(result, searcher.getVariance());
    }

    private List<String> getUCIParameters(String line) {
        return List.of(line.split(" "));
    }

    private String getUCIParameter(List<String> parameters, String name, String defaultValue) {
        int index = parameters.indexOf(name);
        if (index == -1) {
            return defaultValue;
        }
        return parameters.get(index + 1);
    }

    private Long getLongUCIParameter(List<String> parameters, String name, Long defaultValue) {
        int index = parameters.indexOf(name);
        if (index == -1) {
            return defaultValue;
        }
        return Long.parseLong(parameters.get(index + 1));
    }

    private void play(PositionSearchResult result, int variance) {
        MovePath movePath = game.play(result, variance);
        sendMove(result, movePath);
    }

    private void play(String move) {
        game.play(move);
        sendMove(move);
    }

    private void sendIdName() {
        System.out.println("id name mewel" + " " + VERSION);
    }

    private void sendIdAuthor() {
        System.out.println("id author Matthias Eichner");
    }

    private void sendUCIOk() {
        System.out.println("uciok");
    }

    private void sendReadyOk() {
        System.out.println("readyok");
    }

    private void sendMove(PositionSearchResult result, MovePath movePath) {
        MoveNode head = movePath.head();
        Integer cp = head.getPositionNode().getMinMaxValue();
        boolean white = head.getMove().isWhite();
        System.out.println("info score cp " + toUCIScore(white, cp) + " time " + result.took() + " pv " + movePath);
        System.out.println("bestmove " + head.getMove().toString());
    }

    private void sendMove(String move) {
        System.out.println("bestmove " + move);
    }

    private int toUCIScore(boolean white, Integer cp) {
        return white ? cp : -cp;
    }

    public static void main(String[] args) throws Exception {
        new EngineUCI().run();
    }

}
