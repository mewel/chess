package de.mewel.chess.engine.model;

import java.util.*;

public class PositionNode {

    private final long positionHash;

    private boolean fullNode;

    private byte level;

    private Integer positionalValue;

    private Integer minMaxValue;

    private final List<MoveNode> moveNodes;

    public PositionNode(long positionHash, int level, boolean fullNode) {
        this.positionHash = positionHash;
        this.level = (byte) level;
        this.fullNode = fullNode;
        this.moveNodes = new ArrayList<>();
        this.positionalValue = null;
        this.minMaxValue = null;
    }

    public int getLevel() {
        return level;
    }

    public boolean isFullNode() {
        return fullNode;
    }

    public boolean isQuiescenceMoveNode() {
        return !fullNode;
    }

    public long getPositionHash() {
        return positionHash;
    }

    public List<MoveNode> getMoveNodes() {
        return moveNodes;
    }

    public void setLevel(int level) {
        this.level = (byte) level;
    }

    public void setFullNode(boolean fullNode) {
        this.fullNode = fullNode;
    }

    public void setPositionalValue(Integer positionalValue) {
        this.positionalValue = positionalValue;
    }

    public void setMinMaxValue(Integer minMaxValue) {
        this.minMaxValue = minMaxValue;
    }

    public Integer getMinMaxValue() {
        return minMaxValue;
    }

    public Integer getPositionalValue() {
        return positionalValue;
    }

    public int countMoves() {
        return descendentMoves().size();
    }

    public Set<MoveNode> descendentMoves() {
        Set<MoveNode> nodes = new HashSet<>();
        for (MoveNode node : getMoveNodes()) {
            nodes.add(node);
            nodes.addAll(node.getPositionNode().descendentMoves());
        }
        return nodes;
    }

    public int countPositions() {
        return getPositionHashs().size();
    }

    public Set<Long> descendentPositions() {
        Set<Long> nodes = new HashSet<>();
        for (MoveNode node : getMoveNodes()) {
            nodes.add(node.getPositionNode().positionHash);
            nodes.addAll(node.getPositionNode().descendentPositions());
        }
        return nodes;
    }

    private Set<Long> getPositionHashs() {
        Set<Long> positionHashSet = new HashSet<>();
        positionHashSet.add(positionHash);
        for (MoveNode moveNode : moveNodes) {
            positionHashSet.addAll(moveNode.getPositionNode().getPositionHashs());
        }
        return positionHashSet;
    }

    public void reset(byte level) {
        this.getMoveNodes().clear();
        this.setFullNode(true);
        this.setPositionalValue(null);
        this.setMinMaxValue(null);
        this.setLevel(level);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PositionNode that = (PositionNode) o;
        return positionHash == that.positionHash;
    }

    @Override
    public int hashCode() {
        return (int) positionHash;
    }

}
