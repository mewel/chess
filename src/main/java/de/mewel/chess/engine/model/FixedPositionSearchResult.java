package de.mewel.chess.engine.model;

import de.mewel.chess.common.ChessException;

import java.util.*;
import java.util.stream.Collectors;

public class FixedPositionSearchResult extends AbstractPositionSearchResult {

    private final PositionNode rootNode;

    private LinkedHashMap<MovePath, Integer> moveMap;

    public FixedPositionSearchResult(boolean white, int turn, PositionNode rootNode) {
        super(white, turn);
        this.rootNode = rootNode;
        this.moveMap = new LinkedHashMap<>();
    }

    public void add(MovePath movePath) {
        this.moveMap.put(movePath, movePath.getValue());
    }

    public FixedPositionSearchResult sort() {
        this.moveMap = this.moveMap.entrySet().stream()
                .sorted(isWhite() ? Collections.reverseOrder(Map.Entry.comparingByValue()) : Map.Entry.comparingByValue())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
        return this;
    }

    public PositionNode getRootNode() {
        return rootNode;
    }

    public LinkedHashMap<MovePath, Integer> getMoveMap() {
        return moveMap;
    }

    @Override
    public MovePath getBestMovePath() {
        if (moveMap.isEmpty()) {
            return null;
        }
        return moveMap.entrySet().stream()
                .findFirst()
                .map(Map.Entry::getKey)
                .orElseThrow(() -> new ChessException("Unable to get best move!"));
    }

    @Override
    public List<MovePath> listCandidates(int variance) {
        MovePath bestMove = getBestMovePath();
        if (bestMove == null) {
            return Collections.emptyList();
        }
        Integer target = bestMove.getValue();
        return moveMap.entrySet().stream()
                .filter(movePath -> Math.abs(movePath.getValue() - target) <= variance)
                .map(Map.Entry::getKey)
                .toList();
        // 3 candidates
        // rn1qkbnr/ppp2ppp/3p4/4p3/3PP1b1/4BN2/PPP2PPP/RN1QKB1R b KQkq - 2 4
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FixedPositionSearchResult that = (FixedPositionSearchResult) o;
        if (!Objects.equals(rootNode, that.rootNode)) {
            return false;
        }
        if (this.moveMap.size() != that.moveMap.size()) {
            return false;
        }
        for (Map.Entry<MovePath, Integer> entry : this.moveMap.entrySet()) {
            Integer thisValue = entry.getValue();
            Integer thatValue = that.moveMap.get(entry.getKey());
            if (!Objects.equals(thisValue, thatValue)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hash(rootNode, moveMap);
    }

}
