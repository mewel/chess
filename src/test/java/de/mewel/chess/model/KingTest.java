package de.mewel.chess.model;

import de.mewel.chess.common.PieceUtil;
import de.mewel.chess.common.PositionUtil;
import org.junit.Ignore;
import org.junit.Test;

import static de.mewel.chess.model.PieceNotation.*;
import static org.junit.Assert.*;

public class KingTest {

    @Test
    public void moves() {
        Position p = PositionUtil.base();
        King whiteKing = new King(true, 4, 0);

        // cannot move on base position
        assertEquals("King cannot move on base position", 0, whiteKing.moves(p).size());

        // remove pawn in front of king
        p.set(PieceNotation.EMPTY, 4, 1);
        assertEquals("King can move one squares", 1, whiteKing.moves(p).size());

        // place king in center
        p.set(PieceNotation.W_KING, 4, 4);
        assertEquals("king can move all eight squares", 8, whiteKing.square(4, 4).moves(p).size());

        p = PositionUtil.base().empty(4, 0).set(PieceNotation.W_KING, 4, 5);
        assertEquals("king can move to three squares", 5, whiteKing.square(4, 5).moves(p).size());

        p.clear().set(W_KING, 5, 0).set(B_PAWN, 5, 1).set(B_KING, 7, 7);
        assertEquals(5, whiteKing.square(5, 0).moves(p).size());

        p.clear().set(W_KING, 0, 0).set(B_KING, 2, 2);
        assertEquals(2, whiteKing.square(0, 0).moves(p).size());

        p.clear().set(W_KING, 0, 0).set(B_KING, 0, 2);
        assertEquals(1, whiteKing.square(0, 0).moves(p).size());

        p.clear().set(W_KING, 2, 0).set(B_KING, 3, 2);
        assertEquals(3, whiteKing.square(2, 0).moves(p).size());

        p = PositionUtil.fen("b3k2r/2p2p2/7p/1p2P2q/4p3/1QP4N/1KP2PPP/7R b kq - 0 23");
        assertTrue(PositionUtil.list(p).contains(p.move("e8g8")));
        assertFalse(PositionUtil.list(p).contains(p.move("e8c8")));
    }

    @Ignore // TODO ignore for now
    @Test
    public void inCheckMoves() {
        Position p = PositionUtil.empty();
        King king = set(p, 3, 0, B_ROOK, 0, 0);
        p.set(B_KING, 7, 7);
        assertEquals(3, king.inCheckMoves(p).size());
        assertTrue(king.inCheckMoves(p).contains(p.move("d1c2")));
        assertTrue(king.inCheckMoves(p).contains(p.move("d1d2")));
        assertTrue(king.inCheckMoves(p).contains(p.move("d1e2")));
        p.set(B_QUEEN, 4, 2).getCheckingPieces().add(new Queen(false, 4, 2));
        assertEquals(1, king.inCheckMoves(p).size());
        assertTrue(king.inCheckMoves(p).contains(p.move("d1c2")));
        p.empty(4, 2).set(B_QUEEN, 4, 1).getCheckingPieces().remove(1);
        p.getCheckingPieces().add(new Queen(false, 4, 1));
        assertEquals(1, king.inCheckMoves(p).size());
        assertTrue(king.inCheckMoves(p).contains(p.move("d1e2")));
        king = set(p, 3, 0, B_QUEEN, 3, 1);
        p.set(B_ROOK, 3, 2).set(B_KING, 7, 7);
        assertEquals(0, king.inCheckMoves(p).size());
        p.empty(3, 2);
        assertEquals(1, king.inCheckMoves(p).size());
        assertTrue(king.inCheckMoves(p).contains(p.move("d1d2")));
    }

    private King set(Position p, int kingX, int kingY, byte checkingPiece, int checkingPieceX, int checkingPieceY) {
        p.clear();
        p.set(checkingPiece, checkingPieceX, checkingPieceY);
        p.getCheckingPieces().add(PieceUtil.fromByte(checkingPiece, checkingPieceX, checkingPieceY));
        p.set(p.isWhite() ? W_KING : B_KING, kingX, kingY);
        return new King(p.isWhite(), kingX, kingY);
    }


}
