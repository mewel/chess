package de.mewel.chess;

import de.mewel.chess.common.Move;
import de.mewel.chess.engine.model.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public abstract class ChessH2 {

    private static final String POSITION_TABLE = "POS2";

    private static final String MOVE_TABLE = "MOVE2";

    private static Connection CONNECTION;

    public static void connect() throws Exception {
        if (!Settings.DATABASE) {
            return;
        }
        CONNECTION = DriverManager.getConnection("jdbc:h2:~/chess;AUTO_SERVER=TRUE", "sa", "");
    }

    public static void close() throws SQLException {
        if (!Settings.DATABASE) {
            return;
        }
        if (CONNECTION != null) {
            CONNECTION.close();
        }
    }

    public static void write(FixedPositionSearchResult result) {
        if (!Settings.DATABASE) {
            return;
        }
        Statement st = null;
        try {
            st = CONNECTION.createStatement();
            st.execute("drop table if exists " + POSITION_TABLE);
            st.execute("create table " + POSITION_TABLE + "(" +
                    "hash integer," +
                    "fullNode boolean," +
                    "level integer," +
                    "positionalValue integer," +
                    "minMaxValue integer," +
                    "moveNodes varchar" +
                    ")");
            st.execute("drop table if exists " + MOVE_TABLE);
            st.execute("create table " + MOVE_TABLE + "(" +
                    "id integer," +
                    "parentId integer," +
                    "level integer," +
                    "move varchar," +
                    "uci varchar," +
                    "position integer," +
                    "integrity integer" +
                    ")");

            Set<Long> writtenPositions = new HashSet<>();
            writePositionNode(st, result.getRootNode(), writtenPositions);
            MovePath path = new MovePath(-1);
            for (MoveNode moveNode : result.getRootNode().getMoveNodes()) {
                path.offer(moveNode);
                writeMoveNode(st, path, writtenPositions);
                path.poll();
            }
        } catch (Exception exc) {
            exc.printStackTrace();
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (Exception exc) {
                    exc.printStackTrace();
                }
            }
        }
    }

    private static void writeMoveNode(Statement st, MovePath path, Set<Long> writtenPositions) throws SQLException {
        MoveNode node = path.tail();
        MoveNode parentNode = path.ancestor(1);
        PositionNode positionNode = node.getPositionNode();
        Move move = node.getMove();
        st.execute("INSERT INTO " + MOVE_TABLE + " VALUES(" +
                node.getId() + "," +
                (parentNode != null ? parentNode.getId() : null) + "," +
                path.size() + "," +
                "'" + move.toString() + "'," +
                "'" + path + "'," +
                positionNode.getPositionHash() + "," +
                node.getIntegrity() +
                ")");

        writePositionNode(st, positionNode, writtenPositions);
        for (MoveNode moveNode : positionNode.getMoveNodes()) {
            path.offer(moveNode);
            writeMoveNode(st, path, writtenPositions);
            path.poll();
        }
    }

    private static void writePositionNode(Statement st, PositionNode node, Set<Long> writtenPositions) throws SQLException {
        long hash = node.getPositionHash();
        if (writtenPositions.contains(hash)) {
            return;
        } else {
            writtenPositions.add(hash);
        }
        st.execute("INSERT INTO " + POSITION_TABLE + " VALUES(" +
                hash + "," +
                node.isFullNode() + "," +
                node.getLevel() + "," +
                node.getPositionalValue() + "," +
                node.getMinMaxValue() + "," +
                "'" + node.getMoveNodes().stream().map(MoveNode::getId).map(Object::toString).collect(Collectors.joining(" ")) + "'" +
                ")");
    }

}
