package de.mewel.chess.model;

import de.mewel.chess.common.Move;
import de.mewel.chess.common.PieceUtil;
import de.mewel.chess.common.PositionUtil;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static de.mewel.chess.model.PieceNotation.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BishopTest {

    @Test
    public void moves() {
        Position p = PositionUtil.base();
        Bishop whiteBishop = new Bishop(true, 2, 0);

        // cannot move on base position
        assertEquals("Bishop cannot move on base position", 0, whiteBishop.moves(p).size());

        // remove both pawns in front of bishop
        p.set(PieceNotation.EMPTY, 1, 1);
        p.set(PieceNotation.EMPTY, 3, 1);
        assertEquals("Bishop can move seven squares and capture no piece", 7, whiteBishop.moves(p).size());

        // place bishop in center
        p.set(W_BISHOP, 4, 4);
        assertEquals("Bishop can move seven squares and capture two pieces", 9, whiteBishop.square(4, 4).moves(p).size());
    }

    @Test
    public void inCheckMoves() {
        Position p = PositionUtil.empty();

        // vertical checks
        Bishop bishop = set(p, 2, 0, 5, 0, B_QUEEN, 5, 3);
        assertEquals("c1f4", bishop.inCheckMoves(p).get(0).toString());

        bishop = set(p, 7, 1, 5, 0, B_QUEEN, 5, 3);
        assertEquals("h2f4", bishop.inCheckMoves(p).get(0).toString());

        bishop = set(p, 7, 0, 5, 0, B_QUEEN, 5, 3);
        assertEquals("h1f3", bishop.inCheckMoves(p).get(0).toString());

        bishop = set(p, 7, 3, 5, 0, B_QUEEN, 5, 5);
        assertEquals(2, bishop.inCheckMoves(p).size());

        bishop = set(p, 7, 3, 5, 7, B_QUEEN, 5, 3);
        assertEquals("h4f6", bishop.inCheckMoves(p).get(0).toString());

        bishop = set(p, 7, 3, 5, 7, B_QUEEN, 5, 0);
        assertEquals(2, bishop.inCheckMoves(p).size());

        bishop = set(p, 0, 3, 5, 7, B_QUEEN, 5, 0);
        assertEquals(0, bishop.inCheckMoves(p).size());

        bishop = set(p, 7, 3, 5, 7, B_QUEEN, 5, 0);
        p.set(W_PAWN, 6, 2);
        assertEquals(1, bishop.inCheckMoves(p).size());
        p.set(B_PAWN, 6, 4);
        assertEquals(0, bishop.inCheckMoves(p).size());

        // horizontal checks
        bishop = set(p, 3, 0, 0, 1, B_QUEEN, 7, 1);
        assertEquals(2, bishop.inCheckMoves(p).size());

        bishop = set(p, 0, 2, 0, 1, B_QUEEN, 7, 1);
        assertEquals("a3b2", bishop.inCheckMoves(p).get(0).toString());

        bishop = set(p, 0, 4, 0, 1, B_QUEEN, 7, 1);
        assertEquals("a5d2", bishop.inCheckMoves(p).get(0).toString());

        bishop = set(p, 6, 2, 0, 1, B_QUEEN, 7, 1);
        assertTrue(bishop.inCheckMoves(p).contains(p.move("g3xh2")));
        assertTrue(bishop.inCheckMoves(p).contains(p.move("g3f2")));

        bishop = set(p, 6, 7, 0, 1, B_QUEEN, 7, 1);
        assertEquals(0, bishop.inCheckMoves(p).size());

        bishop = set(p, 0, 5, 0, 1, B_QUEEN, 3, 1);
        assertEquals(0, bishop.inCheckMoves(p).size());

        bishop = set(p, 1, 0, 7, 4, B_QUEEN, 3, 4);
        assertEquals(1, bishop.inCheckMoves(p).size());
        p.set(B_PAWN, 4, 3);
        assertEquals(0, bishop.inCheckMoves(p).size());

        // diagonal
        bishop = set(p, 1, 3, 6, 0, B_QUEEN, 1, 5);
        assertEquals("b4c5", bishop.inCheckMoves(p).get(0).toString());

        bishop = set(p, 7, 1, 6, 0, B_QUEEN, 1, 5);
        assertEquals(0, bishop.inCheckMoves(p).size());

        bishop = set(p, 1, 7, 6, 0, B_QUEEN, 1, 5);
        assertEquals(0, bishop.inCheckMoves(p).size());

        bishop = set(p, 7, 2, 6, 0, B_QUEEN, 1, 5);
        assertEquals(0, bishop.inCheckMoves(p).size());

        bishop = set(p, 7, 7, 6, 0, B_QUEEN, 1, 5);
        assertEquals("h8d4", bishop.inCheckMoves(p).get(0).toString());
        p.set(W_PAWN, 5, 5);
        assertEquals(0, bishop.inCheckMoves(p).size());

        bishop = set(p, 2, 4, 6, 6, B_QUEEN, 1, 1);
        assertEquals("c5d4", bishop.inCheckMoves(p).get(0).toString());

        bishop = set(p, 5, 1, 6, 6, B_QUEEN, 1, 1);
        assertEquals("f2d4", bishop.inCheckMoves(p).get(0).toString());

        bishop = set(p, 5, 1, 1, 1, B_QUEEN, 5, 5);
        assertEquals("f2d4", bishop.inCheckMoves(p).get(0).toString());

        bishop = set(p, 7, 3, 1, 1, B_QUEEN, 5, 5);
        assertEquals("h4f6", bishop.inCheckMoves(p).get(0).toString());

        bishop = set(p, 1, 1, 6, 6, B_QUEEN, 3, 3);
        assertEquals("b2d4", bishop.inCheckMoves(p).get(0).toString());
        p.set(B_PAWN, 2, 2);
        assertEquals(0, bishop.inCheckMoves(p).size());
    }

    @Test
    public void pinnedMoves() {
        // test with queen
        Position p2 = PositionUtil.empty()
                .set(B_QUEEN, 0, 7)
                .set(W_QUEEN, 5, 2)
                .set(PieceNotation.W_KING, 7, 0);
        Queen blackQueen = new Queen(false, 0, 7);
        Bishop whiteBishop = new Bishop(true, 5, 2);
        King whiteKing = new King(true, 7, 0);
        List<Move> pinnedMoves = whiteBishop.pinnedMoves(p2, whiteKing, blackQueen);
        assertEquals(6, pinnedMoves.size());

        // test with rook
        Position p1 = PositionUtil.empty()
                .set(B_ROOK, 0, 7)
                .set(W_QUEEN, 0, 5)
                .set(PieceNotation.W_KING, 0, 0);
        Rook blackRook = new Rook(false, 0, 7);
        whiteBishop = new Bishop(true, 0, 5);
        whiteKing = new King(true, 0, 0);
        pinnedMoves = whiteBishop.pinnedMoves(p1, whiteKing, blackRook);
        assertEquals(0, pinnedMoves.size());
    }

    private Bishop set(Position p, int bishopX, int bishopY, int kingX, int kingY, byte attPiece, int attX, int attY) {
        p.clear();
        setCheckingPiece(p, attPiece, attX, attY);
        p.set(W_KING, kingX, kingY);
        return setWhiteBishop(p, bishopX, bishopY);
    }

    private Bishop setWhiteBishop(Position p, int x, int y) {
        p.set(W_BISHOP, x, y);
        return new Bishop(true, x, y);
    }

    private void setCheckingPiece(Position p, byte piece, int x, int y) {
        p.set(piece, x, y);
        p.setCheckingPieces(Arrays.asList(Objects.requireNonNull(PieceUtil.fromByte(piece, x, y))));
    }


}
