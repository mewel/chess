package de.mewel.chess.common;

import de.mewel.chess.model.PieceNotation;
import de.mewel.chess.model.Position;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MoveExecutor {

    public List<Move> forward(Position position, String moves) {
        List<Move> moveList = new ArrayList<>();
        List<String> moveStringList = Arrays.asList(moves.split(" "));
        moveStringList.forEach(moveString -> {
            Move move = position.move(moveString);
            forward(position, move);
            moveList.add(move);
        });
        return moveList;
    }

    public void forward(Position position, List<Move> moves) {
        for (Move move : moves) {
            move.forward(position);
            position.setTwoSquarePawn(-1);
            if (move.isCastle()) {
                forwardCastle(position, move);
            } else {
                position.empty(move.getX(), move.getY());
                if (move.isPromoting()) {
                    forwardPromote(position, move);
                } else if (move.isEnPassantCapture()) {
                    forwardEnPassant(position, move);
                } else {
                    forwardDefault(position, move);
                }
            }
            if (!move.isCastle() && PieceUtil.isKing(move.getCapturedPiece())) {
                throw new KingCapturedException("invalid position. this should never happen. the king cannot be captured: " + move + " -> \n\r" + position + "\n\r" + PositionUtil.toFEN(position));
            }
            position.nextTurn(!move.isCastle() && (move.getCapturedPiece() != PieceNotation.EMPTY || PieceUtil.isPawn(move.getPiece())));
        }
    }

    public void forward(Position position, Move... moves) {
        this.forward(position, Arrays.asList(moves));
    }

    public void backward(Position position, Move... moves) {
        for (int i = moves.length - 1; i >= 0; i--) {
            Move move = moves[i];
            position.setMeta(move.getForwardMetaCopy());
            if (move.isCastle()) {
                backwardCastle(position, move);
            } else {
                if (move.isPromoting()) {
                    backwardPromoting(position, move);
                } else if (move.isEnPassantCapture()) {
                    backwardEnPassant(position, move);
                } else {
                    backwardDefault(position, move);
                }
            }
            move.backward();
        }
    }

    private static void forwardDefault(Position position, Move move) {
        byte piece = move.getPiece();
        setPieceAndCheckForCheck(position, piece, move.getToX(), move.getToY());
        // move pawn two squares?
        if ((PieceUtil.isPawn(piece)) && Math.abs(move.getY() - move.getToY()) == 2) {
            position.setTwoSquarePawn(move.getX());
        }
        // castling
        if (piece == PieceNotation.W_KING) {
            position.setCastleWhite(0);
        } else if (piece == PieceNotation.B_KING) {
            position.setCastleBlack(0);
        } else if (piece == PieceNotation.W_ROOK && position.getCastleWhite() != 0) {
            if (move.getX() == 0) {
                position.setCastleWhite(position.getCastleWhite() == 2 ? 0 : 1);
            } else if (move.getX() == 7) {
                position.setCastleWhite(position.getCastleWhite() == 1 ? 0 : 2);
            }
        } else if (piece == PieceNotation.B_ROOK && position.getCastleBlack() != 0) {
            if (move.getX() == 0) {
                position.setCastleBlack(position.getCastleBlack() == 2 ? 0 : 1);
            } else if (move.getX() == 7) {
                position.setCastleBlack(position.getCastleBlack() == 1 ? 0 : 2);
            }
        }
    }

    private static void forwardEnPassant(Position position, Move move) {
        position.empty(move.getToX(), move.getY());
        setPieceAndCheckForCheck(position, move.getPiece(), move.getToX(), move.getToY());
    }

    private static void forwardPromote(Position position, Move move) {
        setPieceAndCheckForCheck(position, move.getPromotingPiece(), move.getToX(), move.getToY());
    }

    private static void forwardCastle(Position position, Move move) {
        boolean white = move.isWhite();
        int toY = white ? 0 : 7;
        byte king = white ? PieceNotation.W_KING : PieceNotation.B_KING;
        byte rook = white ? PieceNotation.W_ROOK : PieceNotation.B_ROOK;
        boolean side = move.getCastle();
        position.set(king, side ? 6 : 2, toY);
        position.set(PieceNotation.EMPTY, 4, toY);
        position.set(PieceNotation.EMPTY, side ? 7 : 0, toY);
        setPieceAndCheckForCheck(position, rook, side ? 5 : 3, toY);
        if (white) {
            position.setCastleWhite(0);
        } else {
            position.setCastleBlack(0);
        }
    }

    private static void setPieceAndCheckForCheck(Position position, byte pieceByte, int toX, int toY) {
        position.set(pieceByte, toX, toY);
        position.setCheckingPieces(PositionUtil.getCheckingPieces(position));
    }

    private static void backwardDefault(Position position, Move move) {
        position.set(move.getPiece(), move.getX(), move.getY()).set(move.getCapturedPiece(), move.getToX(), move.getToY());
    }

    private static void backwardPromoting(Position position, Move move) {
        byte pawn = move.isWhite() ? PieceNotation.W_PAWN : PieceNotation.B_PAWN;
        byte capturedPiece = move.getCapturedPiece();
        if (capturedPiece == PieceNotation.EMPTY) {
            position.empty(move.getToX(), move.getToY());
        } else {
            position.set(capturedPiece, move.getToX(), move.getToY());
        }
        position.set(pawn, move.getX(), move.getY());
    }

    private static void backwardEnPassant(Position position, Move move) {
        position.empty(move.getToX(), move.getToY())
                .set(move.getPiece(), move.getX(), move.getY())
                .set(move.getCapturedPiece(), move.getToX(), move.getY());
    }

    private static void backwardCastle(Position position, Move move) {
        boolean white = move.isWhite();
        int toY = white ? 0 : 7;
        int king = white ? PieceNotation.W_KING : PieceNotation.B_KING;
        int rook = white ? PieceNotation.W_ROOK : PieceNotation.B_ROOK;
        boolean side = move.getCastle();
        position.set(king, 4, toY);
        position.set(rook, side ? 7 : 0, toY);
        position.set(PieceNotation.EMPTY, side ? 6 : 2, toY);
        position.set(PieceNotation.EMPTY, side ? 5 : 3, toY);
    }

}
