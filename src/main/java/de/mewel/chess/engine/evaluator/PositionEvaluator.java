package de.mewel.chess.engine.evaluator;

import de.mewel.chess.model.Position;

public interface PositionEvaluator {

    int analyze(Position position);

}
