package de.mewel.chess.engine;

import de.mewel.chess.Settings;
import de.mewel.chess.common.*;
import de.mewel.chess.engine.evaluator.PositionEvaluator;
import de.mewel.chess.engine.model.*;
import de.mewel.chess.engine.model.PositionSearchResult.GameStatus;
import de.mewel.chess.model.PieceNotation;
import de.mewel.chess.model.Position;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class FixedPositionSearcher implements PositionSearcher {

    private static final int CHECKMATE = 99999;

    private final PositionSearcherSettings settings;

    private final PositionEvaluator evaluator;

    private final PositionSearchCache cache;

    public static final AtomicInteger NODE_COUNTER = new AtomicInteger(0);

    private NodeContext nodeContext;

    private List<MoveNode> moveNodes;

    private boolean canceled;

    private boolean calculateAllRootLevelNodes;

    private long startTime;

    private Position position;

    private PositionNode rootNode;

    private List<Move> lastBestWhiteMoves;

    private List<Move> lastBestBlackMoves;

    public FixedPositionSearcher(PositionSearcherSettings settings, PositionEvaluator evaluator, PositionSearchCache cache) {
        this.settings = settings;
        this.evaluator = evaluator;
        this.cache = cache;
        this.canceled = false;
        this.calculateAllRootLevelNodes = false;
        this.lastBestBlackMoves = new ArrayList<>();
        this.lastBestWhiteMoves = new ArrayList<>();
    }

    public FixedPositionSearchResult search(Position position) throws PositionSearcherCanceledException {
        return this.search(position, new LinkedList<>());
    }

    public FixedPositionSearchResult search(Position position, LinkedList<Long> positionHashList, List<Move> moves) throws PositionSearcherCanceledException {
        this.lastBestWhiteMoves = moves.stream().filter(Move::isWhite).toList();
        this.lastBestBlackMoves = moves.stream().filter(Move::isBlack).toList();
        return this.search(position, positionHashList);
    }

    @Override
    public FixedPositionSearchResult search(Position position, LinkedList<Long> positionHashList) throws PositionSearcherCanceledException {
        this.startTime = System.currentTimeMillis();
        position.setCheckingPieces(PositionUtil.getCheckingPieces(position, !position.isWhite()));
        this.position = position;
        this.moveNodes = new ArrayList<>();
        this.nodeContext = new NodeContext(position, new LinkedList<>(positionHashList));
        boolean fullNode = settings.maxFullNode() > 0;
        this.rootNode = new PositionNode(position.longHashCode(), 0, fullNode);
        eval(position, settings, evaluator, rootNode, Integer.MIN_VALUE, Integer.MAX_VALUE);
        return buildResult(false);
    }

    private FixedPositionSearchResult buildResult(boolean canceled) {
        FixedPositionSearchResult result = new FixedPositionSearchResult(position.isWhite(), position.getTurn(), rootNode);
        if (canceled) {
            result.cancel();
        }
        try {
            if(position.doesFiftyMoveRuleApply()) {
                result.setGameStatus(GameStatus.DRAW);
                result.setDrawType(PositionSearchResult.DrawType.FIFTY_MOVES_RULE);
                return result;
            } else if(nodeContext.threefoldRepetitionCheck()) {
                result.setGameStatus(GameStatus.DRAW);
                result.setDrawType(PositionSearchResult.DrawType.THREE_FOLD_REPETITION);
                return result;
            } else if(PositionUtil.listValid(position).isEmpty()) {
                result.setGameStatus(GameStatus.DRAW);
                result.setDrawType(PositionSearchResult.DrawType.STALEMATE);
                return result;
            }
            List<MoveNode> finalMoveNodes = moveNodes.stream()
                    .filter(moveNode -> !moveNode.isIgnored())
                    .sorted(position.isWhite() ? Comparator.comparing(MoveNode::getValue).reversed() : Comparator.comparing(MoveNode::getValue))
                    .toList();
            if (finalMoveNodes.isEmpty()) {
                return result;
            }
            MoveNode bestMoveNode = finalMoveNodes.get(0);
            if (Math.abs(bestMoveNode.getValue()) > 5000) {
                result.add(MovePath.of(bestMoveNode, settings.maxFullNode()));
                result.setGameStatus(bestMoveNode.getValue() > 0 ? GameStatus.WHITE_WON : GameStatus.BLACK_WON);
            } else {
                for (MoveNode moveNode : finalMoveNodes) {
                    result.add(MovePath.of(moveNode, settings.maxFullNode()));
                }
            }
            return result;
        } finally {
            result.took(System.currentTimeMillis() - startTime);
        }
    }

    private Integer alphaBeta(Position position, PositionSearcherSettings settings, PositionEvaluator analyzer, PositionNode positionNode, int alpha, int beta) throws PositionSearcherCanceledException {
        List<Move> moves = PositionUtil.list(position);
        sortMoves(position, moves);
        return handleMoves(position, settings, analyzer, alpha, beta, position.isWhite() ? Integer.MIN_VALUE : Integer.MAX_VALUE, positionNode, moves);
    }

    private Integer quiescence(Position position, PositionSearcherSettings settings, PositionEvaluator analyzer, PositionNode positionNode, int alpha, int beta) throws PositionSearcherCanceledException {
        boolean white = position.isWhite();
        int targetEval = evalPositionValue(position, positionNode, analyzer);
        if (white) {
            if (targetEval >= beta) {
                return beta;
            }
            if (alpha < targetEval) {
                alpha = targetEval;
            }
        } else {
            if (targetEval <= alpha) {
                return alpha;
            }
            if (beta > targetEval) {
                beta = targetEval;
            }
        }
        if (positionNode.getLevel() >= settings.maxQuiescenceNode()) {
            return targetEval;
        }
        List<Move> moves = PositionUtil.listQuiescenceMoves(position);
        if (moves.isEmpty()) {
            // check for checkmate
            if (position.isInCheck()) {
                List<Move> allMoves = PositionUtil.listValid(position);
                if (allMoves.isEmpty()) {
                    position.longHashCode();
                    int value = ((CHECKMATE - (positionNode.getLevel() * 100)) * (position.isWhite() ? -1 : 1));
                    positionNode.setPositionalValue(value);
                    return value;
                }
            }
            // return last eval
            return targetEval;
        }
        sortMoves(position, moves);
        return handleMoves(position, settings, analyzer, alpha, beta, targetEval, positionNode, moves);
    }

    private Integer handleMoves(Position position, PositionSearcherSettings settings, PositionEvaluator analyzer, int alpha, int beta, int target, PositionNode positionNode, List<Move> moves) throws PositionSearcherCanceledException {
        int level = positionNode.getLevel() + 1;
        boolean evalChanged = false;
        boolean white = position.isWhite();
        for (Move move : moves) {
            MoveNode moveNode = new MoveNode(move);
            long childNodeHash = nodeContext.forward(moveNode);
            PositionNode childPositionNode = new PositionNode(childNodeHash, level, level < settings.maxFullNode());
            Integer eval = eval(position, settings, analyzer, childPositionNode, alpha, beta);
            if (eval != null) {
                childPositionNode.setMinMaxValue(eval);
                connectNodes(positionNode, moveNode, childPositionNode);
                target = white ? Math.max(target, eval) : Math.min(target, eval);
                evalChanged = true;
                if (white) {
                    if (alpha < eval || (positionNode.getLevel() == 0 && calculateAllRootLevelNodes)) {
                        alpha = eval;
                    } else {
                        moveNode.ignoreMove();
                    }
                } else {
                    if (beta > eval || (positionNode.getLevel() == 0 && calculateAllRootLevelNodes)) {
                        beta = eval;
                    } else {
                        moveNode.ignoreMove();
                    }
                }
            }
            nodeContext.backward(moveNode);
            if (beta <= alpha) {
                break;
            }
        }
        // check for stalemate - each move leads to checkmate
        if (((white && target < -5000) || (!white && target > 5000)) &&
                !position.isInCheck() &&
                PositionUtil.listValid(position).isEmpty()) {
            positionNode.setPositionalValue(0);
            return 0;
        }
        return evalChanged ? target : null;
    }

    private void connectNodes(PositionNode positionNode, MoveNode moveNode, PositionNode childPositionNode) {
        positionNode.getMoveNodes().add(moveNode);
        if (positionNode.getLevel() == 0) {
            this.moveNodes.add(moveNode);
        }
        moveNode.setPositionNode(childPositionNode);
    }

    private Integer eval(Position position, PositionSearcherSettings settings, PositionEvaluator analyzer, PositionNode positionNode, int alpha, int beta) throws PositionSearcherCanceledException {
        checkInterrupted();
        int level = positionNode.getLevel();
        boolean isFullNode = level <= settings.maxFullNode();
        if (isFullNode && (position.doesFiftyMoveRuleApply() || nodeContext.threefoldRepetitionCheck())) {
            positionNode.setPositionalValue(0);
            return 0;
        }
        boolean isChildFullNode = level < settings.maxFullNode();
        try {
            if (isChildFullNode) {
                return alphaBeta(position, settings, analyzer, positionNode, alpha, beta);
            } else {
                return quiescence(position, settings, analyzer, positionNode, alpha, beta);
            }
        } catch (KingCapturedException kingCapturedException) {
            int value = (CHECKMATE - (positionNode.getLevel() * 100)) * (position.isWhite() ? 1 : -1);
            positionNode.setPositionalValue(value);
            return value;
        }
    }

    private void checkInterrupted() throws PositionSearcherCanceledException {
        if (isCanceled() || Thread.currentThread().isInterrupted()) {
            FixedPositionSearchResult result = buildResult(true);
            throw new PositionSearcherCanceledException(result);
        }
    }

    private void sortMoves(final Position position, final List<Move> moves) {
        for (Move move : moves) {
            move.forwardPieces(position);
            if (PieceUtil.isKing(move.getCapturedPiece())) {
                throw new KingCapturedException("king was captured! invalid move!");
            }
        }
        moves.sort(this::compareMoves);
    }

    private int evalPositionValue(Position position, PositionNode positionNode, PositionEvaluator analyzer) {
        if (positionNode.getPositionalValue() == null) {
            Integer eval = this.cache.get(position.longHashCode());
            if (eval == null) {
                eval = analyzer.analyze(position);
                this.cache.put(position.longHashCode(), eval);
            }
            positionNode.setPositionalValue(eval);
        }
        NODE_COUNTER.incrementAndGet();
        return positionNode.getPositionalValue();
    }

    public int compareMoves(Move m1, Move m2) {
        // last  best moves
        List<Move> lastBestMoves = m1.isWhite() ? lastBestWhiteMoves : lastBestBlackMoves;

        boolean containsM1 = lastBestMoves.contains(m1);
        boolean containsM2 = lastBestMoves.contains(m2);

        // Prioritize last best moves
        if (containsM1 && containsM2) return 0;
        if (containsM1) return -1;
        if (containsM2) return 1;

        // Check promotions
        byte promotingPiece1 = m1.getPromotingPiece();
        byte promotingPiece2 = m2.getPromotingPiece();
        if (promotingPiece1 != -1 && promotingPiece2 == -1) return -1;
        if (promotingPiece2 != -1 && promotingPiece1 == -1) return 1;
        if (m1.getPromotingPiece() != -1 && m2.getPromotingPiece() != -1) {
            return Byte.compare(m1.getPromotingPiece(), m2.getPromotingPiece());
        }

        // captures
        boolean m1Captures = m1.getCapturedPiece() != PieceNotation.EMPTY;
        boolean m2Captures = m2.getCapturedPiece() != PieceNotation.EMPTY;

        if (!m1Captures && m2Captures) return 1;
        if (m1Captures && !m2Captures) return -1;

        // Compare capture values
        if (m1Captures) {
            int value1 = pieceValue(m1.getCapturedPiece()) - pieceValue(m1.getPiece());
            int value2 = pieceValue(m2.getCapturedPiece()) - pieceValue(m2.getPiece());
            return Integer.compare(value2, value1);
        }

        // Tie-breaking: Compare positions lexicographically
        int result = Integer.compare(m1.getX(), m2.getX());
        if (result != 0) return result;

        result = Integer.compare(m1.getY(), m2.getY());
        if (result != 0) return result;

        result = Integer.compare(m1.getToX(), m2.getToX());
        if (result != 0) return result;

        result = Integer.compare(m1.getToY(), m2.getToY());
        if (result != 0) return result;

        return Boolean.compare(m1.getCastle(), m2.getCastle());
    }

    public int numPositionsInCache() {
        return cache.size();
    }

    public static int pieceValue(byte piece) {
        if (PieceUtil.isQueen(piece)) {
            return 9;
        } else if (PieceUtil.isRook(piece)) {
            return 5;
        } else if (PieceUtil.isBishop(piece) || PieceUtil.isKnight(piece)) {
            return 3;
        }
        return 1;
    }

    public void setCalculateAllRootLevelNodes(boolean calculateAllRootLevelNodes) {
        this.calculateAllRootLevelNodes = calculateAllRootLevelNodes;
    }

    public boolean isCalculateAllRootLevelNodes() {
        return calculateAllRootLevelNodes;
    }

    public PositionSearcherSettings getSettings() {
        return this.settings;
    }

    public void cancel() {
        this.canceled = true;
    }

    public boolean isCanceled() {
        return this.canceled;
    }

    public static class NodeContext {

        Position position;

        MoveExecutor moveExecutor;

        MovePath movePath;

        long rootHashCode;

        LinkedList<Long> positionHashList;

        NodeContext(Position position, LinkedList<Long> positionHashList) {
            this.position = position;
            this.moveExecutor = new MoveExecutor();
            this.movePath = new MovePath(-1);
            this.rootHashCode = position.longHashCode();
            this.positionHashList = positionHashList;
        }

        public long forward(MoveNode moveNode) {
            movePath.offer(moveNode);
            moveExecutor.forward(position, moveNode.getMove());
            long hash = position.longHashCode();
            positionHashList.addLast(hash);
            return hash;
        }

        public void backward(MoveNode moveNode) {
            if (Settings.INTEGRITY) {
                moveNode.calculateIntegrity(position, movePath);
                if (moveNode.getIntegrity() != this.rootHashCode) {
                    System.out.println("warn invalid hash code on " + movePath);
                    System.exit(1);
                }
            }
            movePath.poll();
            moveExecutor.backward(position, moveNode.getMove());
            positionHashList.pollLast();
        }

        public boolean threefoldRepetitionCheck() {
            if (positionHashList.size() < 10) {
                return false;
            }
            long lastHash = positionHashList.getLast();
            int toCheck = 20; // check max 20 positions
            int count = 1;
            Iterator<Long> it = positionHashList.descendingIterator();
            it.next();
            while (it.hasNext() && count < 3 && toCheck-- >= 0) {
                if (it.next() == lastHash) {
                    count++;
                }
            }
            return count >= 3;
        }

    }

}
