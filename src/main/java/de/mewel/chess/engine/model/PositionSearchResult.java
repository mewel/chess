package de.mewel.chess.engine.model;

import de.mewel.chess.common.ChessException;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public interface PositionSearchResult {

    enum GameStatus {
        ONGOING, WHITE_WON, BLACK_WON, DRAW
    }

    enum DrawType {
        THREE_FOLD_REPETITION, FIFTY_MOVES_RULE, STALEMATE
    }

    MovePath getBestMovePath();

    default MovePath getMovePath(int variance) {
        List<MovePath> candidates = listCandidates(variance);
        if (candidates.isEmpty()) {
            throw new ChessException("Unable to get best move!");
        }
        return candidates.get(ThreadLocalRandom.current().nextInt(candidates.size()));
    }

    List<MovePath> listCandidates(int variance);

    long took();

    GameStatus getGameStatus();

    DrawType getDrawType();

    default boolean isGameFinished() {
        return !this.getGameStatus().equals(PositionSearchResult.GameStatus.ONGOING);
    }

    default boolean hasWhiteWon() {
        return this.getGameStatus().equals(PositionSearchResult.GameStatus.WHITE_WON);
    }

    default boolean hasBlackWon() {
        return this.getGameStatus().equals(PositionSearchResult.GameStatus.BLACK_WON);
    }

    default boolean isDraw() {
        return this.getGameStatus().equals(PositionSearchResult.GameStatus.DRAW);
    }

    int getTurn();

    boolean isCanceled();

    boolean isWhite();

}
