package de.mewel.chess;

public abstract class EngineUtil {

    public static void printMemoryUsage() {
        long mb = (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / (1024 * 1024);
        System.out.println("memory usage " + mb + "MB");
    }

}
