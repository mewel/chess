package de.mewel.chess.model;

import de.mewel.chess.common.Move;
import de.mewel.chess.common.PieceUtil;
import de.mewel.chess.common.PositionUtil;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static de.mewel.chess.model.PieceNotation.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class RookTest {

    @Test
    public void moves() {
        Position p = PositionUtil.base();
        Rook whiteRook = new Rook(true, 0, 0);

        // cannot move on base position
        assertEquals("Rook cannot move on base position", 0, whiteRook.moves(p).size());

        // remove pawn before rook
        p.set(PieceNotation.EMPTY, 0, 1);
        assertEquals("Rook can move five squares and capture one pawn", 6, whiteRook.moves(p).size());

        // place rook in center
        p.set(W_ROOK, 4, 4);
        assertEquals("Rook can move ten squares and capture one  pawn", 11, whiteRook.square(4, 4).moves(p).size());
    }

    @Test
    public void inCheckMoves() {
        Position p1 = PositionUtil.empty()
                .set(PieceNotation.B_QUEEN, 2, 2)
                .set(PieceNotation.W_KING, 5, 5)
                .set(W_ROOK, 1, 1)
                .setCheckingPieces(List.of(new Queen(false, 2, 2)));
        Rook rook = new Rook(true, 1, 1);
        assertEquals(0, rook.inCheckMoves(p1).size());

        p1.empty(1, 1).set(W_ROOK, 4, 3);
        rook = new Rook(true, 4, 3);
        assertEquals(2, rook.inCheckMoves(p1).size());

        p1.empty(4, 3).set(W_ROOK, 7, 2);
        rook = new Rook(true, 7, 2);
        assertEquals(1, rook.inCheckMoves(p1).size());
        assertEquals("h3c3", rook.inCheckMoves(p1).get(0).toString());

        p1.empty(7, 2).set(W_ROOK, 7, 3);
        rook = new Rook(true, 7, 3);
        assertEquals(1, rook.inCheckMoves(p1).size());
        assertEquals("h4d4", rook.inCheckMoves(p1).get(0).toString());

        Position p2 = PositionUtil.empty()
                .set(PieceNotation.B_ROOK, 7, 5)
                .set(PieceNotation.W_KING, 2, 5)
                .set(W_ROOK, 5, 1)
                .setCheckingPieces(List.of(new Rook(false, 7, 5)));
        rook = new Rook(true, 5, 1);
        assertEquals(1, rook.inCheckMoves(p2).size());
        assertEquals("f2f6", rook.inCheckMoves(p2).get(0).toString());

        p2.empty(5, 1).set(W_ROOK, 2, 1);
        rook = new Rook(true, 2, 1);
        assertEquals(0, rook.inCheckMoves(p2).size());

        p2.empty(2, 1).set(W_ROOK, 4, 7);
        rook = new Rook(true, 4, 7);
        assertEquals(1, rook.inCheckMoves(p2).size());

        p2.empty(2, 5).empty(4, 7).set(PieceNotation.W_KING, 7, 1).set(W_ROOK, 0, 3);
        rook = new Rook(true, 0, 3);
        assertEquals(1, rook.inCheckMoves(p2).size());
        assertEquals("a4h4", rook.inCheckMoves(p2).get(0).toString());

        p2.set(PieceNotation.B_PAWN, 4, 3);
        assertEquals(0, rook.inCheckMoves(p2).size());

        p2.set(PieceNotation.W_PAWN, 4, 3);
        assertEquals(0, rook.inCheckMoves(p2).size());

        p2.empty(0, 3).set(W_ROOK, 7, 7);
        rook = new Rook(true, 7, 7);
        assertEquals(1, rook.inCheckMoves(p2).size());
        assertEquals("h8h6", rook.inCheckMoves(p2).get(0).toString());

        p2.setWhite(false);
        rook = set(p2, 7, 7, 3, 7, W_QUEEN, 6, 7);
        assertEquals(1, rook.inCheckMoves(p2).size());
        assertTrue(rook.inCheckMoves(p2).contains(p2.move("h8xg8")));

        p2.setWhite(true);
        rook = set(p2, 7, 0, 3, 0, B_ROOK, 2, 0);
        p2.set(B_ROOK, 2, 5).set(B_BISHOP, 5, 3).set(W_PAWN, 4, 1).set(W_PAWN, 5, 1);
        assertEquals(0, rook.inCheckMoves(p2).size());
    }

    @Test
    public void pinnedMoves() {
        // test with queen
        Position p2 = PositionUtil.empty()
                .set(B_QUEEN, 0, 7)
                .set(W_ROOK, 5, 2)
                .set(PieceNotation.W_KING, 7, 0);
        Queen blackQueen = new Queen(false, 0, 7);
        Rook whiteRook = new Rook(true, 5, 2);
        King whiteKing = new King(true, 7, 0);
        List<Move> pinnedMoves = whiteRook.pinnedMoves(p2, whiteKing, blackQueen);
        assertEquals(0, pinnedMoves.size());

        // test with rook
        Position p1 = PositionUtil.empty()
                .set(B_ROOK, 0, 7)
                .set(W_ROOK, 0, 5)
                .set(PieceNotation.W_KING, 0, 0);
        Rook blackRook = new Rook(false, 0, 7);
        whiteRook = new Rook(true, 0, 5);
        whiteKing = new King(true, 0, 0);
        pinnedMoves = whiteRook.pinnedMoves(p1, whiteKing, blackRook);
        assertEquals(6, pinnedMoves.size());
    }

    private Rook set(Position p, int rookX, int rookY, int kingX, int kingY, byte attPiece, int attX, int attY) {
        p.clear();
        setCheckingPiece(p, attPiece, attX, attY);
        p.set(p.isWhite() ? W_KING : B_KING, kingX, kingY);
        return setRook(p, rookX, rookY);
    }

    private Rook setRook(Position p, int x, int y) {
        p.set(p.isWhite() ? W_ROOK : B_ROOK, x, y);
        return new Rook(p.isWhite(), x, y);
    }

    private void setCheckingPiece(Position p, byte piece, int x, int y) {
        p.set(piece, x, y);
        p.setCheckingPieces(Arrays.asList(Objects.requireNonNull(PieceUtil.fromByte(piece, x, y))));
    }

}
