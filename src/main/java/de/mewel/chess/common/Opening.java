package de.mewel.chess.common;

import java.util.List;

public class Opening {

    private final String name;

    private final List<String> moves;

    public Opening(final String name, final List<String> moves) {
        this.name = name;
        this.moves = moves;
    }

    public String getName() {
        return name;
    }

    public List<String> getMoves() {
        return moves;
    }

    public String get(int move) {
        return moves.get(move);
    }

}
