package de.mewel.chess.engine.model;

import de.mewel.chess.common.Move;
import de.mewel.chess.common.MoveExecutor;
import de.mewel.chess.model.Position;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

public class MoveNode {

    private static final AtomicInteger ID;

    static {
        ID = new AtomicInteger(0);
    }

    private final int id;

    private Long integrity;

    private final Move move;

    private boolean ignore;

    private PositionNode positionNode;

    public MoveNode(Move move) {
        this.move = move;
        this.id = ID.incrementAndGet();
        this.integrity = null;
        this.ignore = false;
    }

    public void setPositionNode(PositionNode positionNode) {
        this.positionNode = positionNode;
    }

    public PositionNode getPositionNode() {
        return positionNode;
    }

    public Move getMove() {
        return move;
    }

    public boolean isLeaf() {
        return positionNode.getMoveNodes().isEmpty();
    }

    public Integer getValue() {
        return positionNode.getMinMaxValue();
    }

    public int getId() {
        return id;
    }

    public Long getIntegrity() {
        return integrity;
    }

    public void resetIntegrity() {
        this.integrity = null;
    }

    public void ignoreMove() {
        this.ignore = true;
    }

    public boolean isIgnored() {
        return this.ignore;
    }

    public void calculateIntegrity(Position position, MovePath path) {
        MoveExecutor moveExecutor = new MoveExecutor();
        Position tempPosition = position.copy();
        MoveNode node = this;
        int i = 0;
        while (node != null && node.move != null) {
            if (node.move.isExecuted()) {
                moveExecutor.backward(tempPosition, node.move.copy());
            }
            node = path.ancestor(++i);
        }
        this.integrity = tempPosition.longHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MoveNode moveNode = (MoveNode) o;
        return ignore == moveNode.ignore && Objects.equals(move, moveNode.move) && Objects.equals(positionNode, moveNode.positionNode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(move, ignore, positionNode);
    }

}
