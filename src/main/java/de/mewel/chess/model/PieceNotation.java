package de.mewel.chess.model;


public abstract class PieceNotation {

    public static final byte EMPTY = -1;

    /*
    public static final byte W_KING = 2;
    public static final byte W_QUEEN = 4;
    public static final byte W_ROOK = 8;
    public static final byte W_BISHOP = 16;
    public static final byte W_KNIGHT = 32;
    public static final byte W_PAWN = 64;
    public static final byte B_KING = (byte) (128 + 2);
    public static final byte B_QUEEN = (byte) (128 + 4);
    public static final byte B_ROOK = (byte) (128 + 8);
    public static final byte B_BISHOP = (byte) (128 + 16);
    public static final byte B_KNIGHT = (byte) (128 + 32);
    public static final byte B_PAWN = (byte) (128 + 64);
*/

    public static final byte W_KING = 0;
    public static final byte W_QUEEN = 1;
    public static final byte W_ROOK = 2;
    public static final byte W_BISHOP = 3;
    public static final byte W_KNIGHT = 4;
    public static final byte W_PAWN = 5;

    public static final byte B_KING = 6;
    public static final byte B_QUEEN = 7;
    public static final byte B_ROOK = 8;
    public static final byte B_BISHOP = 9;
    public static final byte B_KNIGHT = 10;
    public static final byte B_PAWN = 11;

}
