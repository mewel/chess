package de.mewel.chess.engine;

import java.util.concurrent.*;

public class TimedTaskRunner {

    private final ExecutorService taskExecutor = Executors.newSingleThreadExecutor();
    private final ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
    private Future<?> futureTask;
    private ScheduledFuture<?> timeoutFuture;

    public void start(Runnable task) {
        start(task, -1);
    }

    public void start(Runnable task, long timeoutMillis) {
        if (isRunning()) {
            throw new IllegalStateException("Task is already running");
        }
        futureTask = taskExecutor.submit(task);
        if (timeoutMillis > 0) {
            timeoutFuture = scheduler.schedule(this::cancel, timeoutMillis, TimeUnit.MILLISECONDS);
        }
    }

    public void cancel() {
        if (futureTask != null && !futureTask.isDone()) {
            futureTask.cancel(true);
        }
        if (timeoutFuture != null && !timeoutFuture.isDone()) {
            timeoutFuture.cancel(true);
        }
    }

    public boolean isRunning() {
        return futureTask != null && !futureTask.isDone();
    }

    public void shutdown() {
        taskExecutor.shutdownNow();
        scheduler.shutdownNow();
    }

}
