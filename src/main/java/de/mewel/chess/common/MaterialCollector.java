package de.mewel.chess.common;

import de.mewel.chess.model.Piece;
import de.mewel.chess.model.Position;

import java.util.ArrayList;
import java.util.List;

public class MaterialCollector {

    private List<Piece> material;

    public MaterialCollector collect(Position position) {
        material = new ArrayList<>();
        for (int y = 0; y <= 7; y++) {
            for (int x = 0; x <= 7; x++) {
                byte pieceNum = position.get(x, y);
                Piece piece = PieceUtil.fromByte(pieceNum, x, y);
                if (piece != null) {
                    material.add(piece);
                }
            }
        }
        return this;
    }

    public List<Piece> getMaterial() {
        return material;
    }

}
