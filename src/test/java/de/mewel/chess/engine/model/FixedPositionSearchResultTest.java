package de.mewel.chess.engine.model;

import de.mewel.chess.common.PositionUtil;
import de.mewel.chess.engine.FixedPositionSearcher;
import de.mewel.chess.engine.PositionSearchCache;
import de.mewel.chess.engine.PositionSearcherCanceledException;
import de.mewel.chess.engine.PositionSearcherSettings;
import de.mewel.chess.engine.evaluator.PiecePositionEvaluator;
import de.mewel.chess.model.Position;
import org.junit.Assert;
import org.junit.Test;

import java.util.LinkedList;

public class FixedPositionSearchResultTest {

    @Test
    public void getMovePathWithVariance() throws PositionSearcherCanceledException {
        PositionSearcherSettings settings = new PositionSearcherSettings(4, 12, 15);
        FixedPositionSearcher positionSearcher = new FixedPositionSearcher(settings, new PiecePositionEvaluator(), new PositionSearchCache(1_000_000));
        Position position = PositionUtil.fen("rnbqk2r/pp3ppp/1b3n2/3p4/8/P1Q2N2/1P1BPPPP/RN2KB1R b KQkq - 3 8");
        FixedPositionSearchResult result = positionSearcher.search(position, new LinkedList<>());
        for (int i = 0; i < 100; i++) {
            MovePath movePath = result.getMovePath(15);
            Assert.assertTrue("should start with b6f2 but does " + movePath.toString(), movePath.toString().startsWith("b6f2"));
        }
    }

}
