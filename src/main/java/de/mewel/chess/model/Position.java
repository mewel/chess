package de.mewel.chess.model;

import de.mewel.chess.common.*;
import de.mewel.chess.renderer.CLIRenderer;

import java.util.*;

import static de.mewel.chess.model.PieceNotation.*;

public class Position {

    /**
     * Board as two-dimensional array. where x = letters and y = numbers [x][y]
     * <p>
     * Notation:
     * <ul>
     * <li>-1 = empty square</li>
     * <li>0 = white king</li>
     * <li>1 = white queen</li>
     * <li>2 = white rook</li>
     * <li>3 = white bishop</li>
     * <li>4 = white knight</li>
     * <li>5 = white pawn</li>
     *
     * <li>6 = black king</li>
     * <li>7 = black queen</li>
     * <li>8 = black rook</li>
     * <li>9 = black bishop</li>
     * <li>10 = black knight</li>
     * <li>11 = black pawn</li>
     * </ul>
     */
    private final byte[] board;

    /**
     * turn, white, castle, en passant information
     */
    private PositionMeta meta;

    /**
     * Creates a new empty position.
     */
    public Position() {
        this.board = new byte[]{
                PieceNotation.EMPTY, PieceNotation.EMPTY, PieceNotation.EMPTY, PieceNotation.EMPTY, PieceNotation.EMPTY, PieceNotation.EMPTY, PieceNotation.EMPTY, PieceNotation.EMPTY,
                PieceNotation.EMPTY, PieceNotation.EMPTY, PieceNotation.EMPTY, PieceNotation.EMPTY, PieceNotation.EMPTY, PieceNotation.EMPTY, PieceNotation.EMPTY, PieceNotation.EMPTY,
                PieceNotation.EMPTY, PieceNotation.EMPTY, PieceNotation.EMPTY, PieceNotation.EMPTY, PieceNotation.EMPTY, PieceNotation.EMPTY, PieceNotation.EMPTY, PieceNotation.EMPTY,
                PieceNotation.EMPTY, PieceNotation.EMPTY, PieceNotation.EMPTY, PieceNotation.EMPTY, PieceNotation.EMPTY, PieceNotation.EMPTY, PieceNotation.EMPTY, PieceNotation.EMPTY,
                PieceNotation.EMPTY, PieceNotation.EMPTY, PieceNotation.EMPTY, PieceNotation.EMPTY, PieceNotation.EMPTY, PieceNotation.EMPTY, PieceNotation.EMPTY, PieceNotation.EMPTY,
                PieceNotation.EMPTY, PieceNotation.EMPTY, PieceNotation.EMPTY, PieceNotation.EMPTY, PieceNotation.EMPTY, PieceNotation.EMPTY, PieceNotation.EMPTY, PieceNotation.EMPTY,
                PieceNotation.EMPTY, PieceNotation.EMPTY, PieceNotation.EMPTY, PieceNotation.EMPTY, PieceNotation.EMPTY, PieceNotation.EMPTY, PieceNotation.EMPTY, PieceNotation.EMPTY,
                PieceNotation.EMPTY, PieceNotation.EMPTY, PieceNotation.EMPTY, PieceNotation.EMPTY, PieceNotation.EMPTY, PieceNotation.EMPTY, PieceNotation.EMPTY, PieceNotation.EMPTY
        };
        this.meta = new PositionMeta();
    }

    /**
     * Creates a new position with the given board.
     *
     * @param board board as array
     */
    public Position(byte[] board, PositionMeta meta) {
        this.board = new byte[64];
        System.arraycopy(board, 0, this.board, 0, 64);
        this.meta = meta;
    }

    public void base() {
        clear();
        this.meta = new PositionMeta();
        // add king
        this.set(PieceNotation.W_KING, 4, 0);
        this.set(B_KING, 4, 7);
        // add queen
        this.set(PieceNotation.W_QUEEN, 3, 0);
        this.set(PieceNotation.B_QUEEN, 3, 7);
        // add rook's
        this.set(PieceNotation.W_ROOK, 0, 0);
        this.set(PieceNotation.W_ROOK, 7, 0);
        this.set(PieceNotation.B_ROOK, 0, 7);
        this.set(PieceNotation.B_ROOK, 7, 7);
        // add bishop's
        this.set(PieceNotation.W_BISHOP, 2, 0);
        this.set(PieceNotation.W_BISHOP, 5, 0);
        this.set(PieceNotation.B_BISHOP, 2, 7);
        this.set(PieceNotation.B_BISHOP, 5, 7);
        // add knight's
        this.set(PieceNotation.W_KNIGHT, 1, 0);
        this.set(PieceNotation.W_KNIGHT, 6, 0);
        this.set(PieceNotation.B_KNIGHT, 1, 7);
        this.set(PieceNotation.B_KNIGHT, 6, 7);
        // add pawn's
        for (int x = 0; x < 8; x++) {
            this.set(PieceNotation.W_PAWN, x, 1);
            this.set(PieceNotation.B_PAWN, x, 6);
        }
    }

    public void fen(String fen) {
        clear();
        this.meta = new PositionMeta();

        String[] fenParts = fen.trim().split(" ");
        String[] posParts = fenParts[0].split("/");
        for (int y = 0; y < posParts.length; y++) {
            String row = posParts[y];
            int x = 0;
            for (int i = 0; i < row.length(); i++) {
                char c = row.charAt(i);
                if (c >= 48 && c <= 58) {
                    x += (c - 48);
                } else {
                    this.set(PieceUtil.fromFEN(c, x, 7 - y));
                    x++;
                }
            }
        }
        this.setWhite(fenParts[1].equals("w"));
        byte castleWhite = 0;
        byte castleBlack = 0;
        for (int i = 0; i < fenParts[2].length(); i++) {
            char c = fenParts[2].charAt(i);
            if (c == 'K') {
                castleWhite += 1;
            } else if (c == 'Q') {
                castleWhite += 2;
            } else if (c == 'k') {
                castleBlack += 1;
            } else if (c == 'q') {
                castleBlack += 2;
            }
        }
        this.setCastleWhite(castleWhite);
        this.setCastleBlack(castleBlack);
        if (!fenParts[3].equals("-")) {
            this.setTwoSquarePawn(fenParts[3].charAt(0) - 97);
        }
        this.setFiftyMoveRule(Byte.parseByte(fenParts[4]));
        this.setTurn((Integer.parseInt(fenParts[5]) * 2) - (this.isWhite() ? 2 : 1));
    }

    /**
     * Checks if the given field is empty. A field is empty when
     * its a valid field and no piece is placed on it.
     *
     * @return true if the position is empty and a piece can actually
     * move here (its not outside of the board)
     */
    public boolean isEmpty(int x, int y) {
        try {
            return this.board[x + y * 8] == PieceNotation.EMPTY;
        } catch (Exception exc) {
            exc.printStackTrace();
            return false;
        }
    }

    /**
     * Returns a piece of the board. -1 if there is no piece or the position is invalid.
     *
     * @param x 0-7
     * @param y 0-7
     * @return a piece on the given position
     */
    public byte get(int x, int y) {
        return this.board[x + y * 8];
    }

    /**
     * Returns a piece of the board at the given index.
     *
     * @param index 0-63
     * @return the piece as byte
     */
    public byte get(int index) {
        return this.board[index];
    }

    /**
     * Checks if x and y values are between 0 and 7 inclusive.
     *
     * @param x 0-7
     * @param y 0-7
     * @return true if the position is valid
     */
    public static boolean valid(int x, int y) {
        return x >= 0 && x <= 7 && y >= 0 && y <= 7;
    }

    /**
     * Clears the square at the given position.
     *
     * @param x 0-7
     * @param y 0-7
     * @return the same position object
     */
    public Position empty(int x, int y) {
        this.board[x + y * 8] = PieceNotation.EMPTY;
        return this;
    }

    public Position clear() {
        for (int i = 0; i < 64; i++) {
            this.board[i] = PieceNotation.EMPTY;
        }
        return this;
    }

    /**
     * Sets the position for the given piece.
     *
     * @param piece the piece to set
     * @param x     0-7
     * @param y     0-7
     * @return the same position object
     */
    public Position set(byte piece, int x, int y) {
        if (!valid(x, y)) {
            throw new ChessException("Invalid board position x: " + x + " y: " + y + " for piece " + piece);
        }
        this.board[x + y * 8] = piece;
        return this;
    }

    public Position set(int piece, int x, int y) {
        return this.set((byte) piece, x, y);
    }

    public Position set(Piece piece) {
        set(piece.toByte(), piece.x(), piece.y());
        return this;
    }

    public boolean isLightSquare(int x, int y) {
        return !this.isDarkSquare(x, y);
    }

    public boolean isDarkSquare(int x, int y) {
        return y % 2 == 0 ? x % 2 == 0 : x % 2 == 1;
    }

    public byte getCastleBlack() {
        return meta.getCastleBlack();
    }

    public byte getCastleWhite() {
        return meta.getCastleWhite();
    }

    public byte getTwoSquarePawn() {
        return meta.getTwoSquarePawn();
    }

    public void setTwoSquarePawn(int x) {
        this.meta.setTwoSquarePawn((byte) x);
    }

    public void setCastleWhite(int castleWhite) {
        this.meta.setCastleWhite((byte) castleWhite);
    }

    public void setCastleBlack(int castleBlack) {
        this.meta.setCastleBlack((byte) castleBlack);
    }

    public Position setWhite(boolean white) {
        this.meta.setWhite(white);
        return this;
    }

    public Position setCheckingPieces(List<Piece> checkingPieces) {
        this.meta.setCheckingPieces(checkingPieces);
        return this;
    }

    public Position setTurn(int turn) {
        this.meta.setTurn(turn);
        return this;
    }

    public Position setFiftyMoveRule(byte turn) {
        this.meta.setFiftyMoveRule(turn);
        return this;
    }

    public boolean isWhite() {
        return this.meta.isWhite();
    }

    public boolean isInCheck() {
        return this.meta.isInCheck();
    }

    public List<Piece> getCheckingPieces() {
        return this.meta.getCheckingPieces();
    }

    public void nextTurn(boolean pawnMoveOrCapture) {
        this.meta.nextTurn(pawnMoveOrCapture);
    }

    public void previousTurn() {
        this.meta.previousTurn();
    }

    public void setMeta(PositionMeta meta) {
        this.meta = meta;
    }

    public PositionMeta getMetaCopy() {
        return meta.copy();
    }

    public Integer getTurn() {
        return meta.getTurn();
    }

    public boolean doesFiftyMoveRuleApply() {
        return this.meta.getFiftyMoveRule() >= 50;
    }

    public byte getFiftyMoveRule() {
        return this.meta.getFiftyMoveRule();
    }

    /**
     * Same as {@link #move(int, int, int, int)} but with pawn promotion option.
     * This promotes every piece. Be sure to call it with a pawn and when the
     * pawn reached the last line.
     *
     * @param fromX     x position where the piece is located
     * @param fromY     y position where the piece is located
     * @param toX       where to move x-axis
     * @param toY       where to move y-axis
     * @param promotion notation of new piece (e.g. white queen == 1)
     * @return the generated move
     */
    public Move promote(int fromX, int fromY, int toX, int toY, byte promotion) {
        return move(fromX, fromY, toX, toY).promote(promotion)
                .capture(get(toX, toY));
    }

    /**
     * Moves the piece to the given position.
     *
     * @param fromX x position where the piece is located
     * @param fromY y position where the piece is located
     * @param toX   where to move x-axis
     * @param toY   where to move y-axis
     * @return the generated move
     */
    public Move move(int fromX, int fromY, int toX, int toY) {
        return new Move(isWhite(), (byte) fromX, (byte) fromY, (byte) toX, (byte) toY);
    }

    public Move capture(int fromX, int fromY, int toX, int toY) {
        return new Move(isWhite(), (byte) fromX, (byte) fromY, (byte) toX, (byte) toY)
                .capture(get(toX, toY));
    }

    /**
     * Does an en passant move. Its import to do an {@link PositionUtil#checkEnPassant(Position, int, int)} check
     * before. Due performance reason's this is not done here.
     *
     * @param fromX pawn x position
     * @param fromY pawn y position
     * @return the new move
     */
    public Move enPassant(int fromX, int fromY, int toX) {
        return move(fromX, fromY, toX, fromY + (isWhite() ? 1 : -1))
                .enPassant()
                .capture(isWhite() ? B_PAWN : W_PAWN);
    }

    /**
     * Does castling the king. Its import to do an {@link PositionUtil#checkCastle(Position, int, int, boolean)} check
     * before. Due performance reason's this is not done here.
     *
     * @param side true = short, false = long
     * @return the new position
     */
    public Move castle(boolean side) {
        return new Move(isWhite(), side);
    }

    /**
     * UCI notation move.
     *
     * @param move the move as uci
     * @return a new move
     */
    public Move move(String move) {
        int xOffset = (move.contains("x") || move.contains("-")) ? 1 : 0;
        boolean white = isWhite();
        int fromX = move.charAt(0) - 97;
        int fromY = move.charAt(1) - 49;
        int toX = move.charAt(2 + xOffset) - 97;
        int toY = move.charAt(3 + xOffset) - 49;
        if (move.length() == (5 + xOffset)) {
            byte knight = white ? PieceNotation.W_KNIGHT : PieceNotation.B_KNIGHT;
            byte queen = white ? PieceNotation.W_QUEEN : PieceNotation.B_QUEEN;
            return promote(fromX, fromY, toX, toY, move.charAt(4 + xOffset) == 'k' ? knight : queen);
        } else {
            byte piece = get(fromX, fromY);
            if (PieceUtil.isKing(piece) && Math.abs(fromX - toX) > 1) {
                return castle(toX > fromX);
            } else if (PositionUtil.checkEnPassant(this, fromX, fromY) && Math.abs(fromX - toX) == 1) {
                return enPassant(fromX, fromY, toX);
            } else {
                byte capture = get(fromX, fromY);
                if (capture == PieceNotation.EMPTY) {
                    return move(fromX, fromY, toX, toY);
                } else {
                    return capture(fromX, fromY, toX, toY);
                }
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Position position = (Position) o;
        return Arrays.equals(board, position.board) &&
                Objects.equals(meta, position.meta);
    }

    @Override
    public int hashCode() {
        return Objects.hash(Arrays.hashCode(board), meta);
    }

    public long longHashCode() {
        return ZobristHashing.positionHash(this);
    }

    public Position copy() {
        return new Position(this.board, this.getMetaCopy());
    }

    @Override
    public String toString() {
        return CLIRenderer.get(this);
    }

}
