package de.mewel.chess.engine;

import de.mewel.chess.engine.model.PositionSearchResult;
import de.mewel.chess.model.Position;

import java.util.LinkedList;

public interface PositionSearcher {

    PositionSearchResult search(Position position, LinkedList<Long> positionHashList) throws PositionSearcherCanceledException;

}
