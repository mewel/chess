package de.mewel.chess.engine;

import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;

public class PositionSearchCache {

    private final Map<Long, Integer> cache;

    private final ArrayBlockingQueue<Long> positionList;

    private final int maxSize;

    public PositionSearchCache(int maxSize) {
        this.cache = new ConcurrentHashMap<>(maxSize);
        this.positionList = new ArrayBlockingQueue<>(maxSize);
        this.maxSize = maxSize;
    }

    public void put(Long positionHash, Integer evaluation) {
        this.cache.put(positionHash, evaluation);
        this.positionList.add(positionHash);
        if(this.positionList.size() >= maxSize) {
            Long oldestPositionHash = this.positionList.poll();
            this.cache.remove(oldestPositionHash);
        }
    }

    public Integer get(Long positionHash) {
        return this.cache.get(positionHash);
    }

    public int size() {
        return this.cache.size();
    }

    public void clear() {
        this.cache.clear();
        this.positionList.clear();
    }

}
