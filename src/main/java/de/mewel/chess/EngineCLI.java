package de.mewel.chess;

import de.mewel.chess.common.Move;
import de.mewel.chess.common.MoveExecutor;
import de.mewel.chess.common.PositionUtil;
import de.mewel.chess.engine.evaluator.DefaultPositionEvaluator;
import de.mewel.chess.engine.IterativePositionSearcher;
import de.mewel.chess.engine.evaluator.PositionEvaluator;
import de.mewel.chess.engine.model.*;
import de.mewel.chess.model.Position;
import de.mewel.chess.renderer.CLIRenderer;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.stream.Collectors;

public class EngineCLI {

    public static void main(String[] args) throws Exception {
        ChessH2.connect();

        PositionEvaluator analyzer = new DefaultPositionEvaluator();
        IterativePositionSearcher evaluator = new IterativePositionSearcher(analyzer, 15);

        Game game = new Game();
        Position position = game.getPosition();
        InputStreamReader streamReader = new InputStreamReader(System.in);
        BufferedReader bufferedReader = new BufferedReader(streamReader);

        int stopAtTurn = 0;
        System.out.println("press any key or enter move");
        String line = "";
        do {
            CLIRenderer.render(position);
            System.out.println("turn " + position.getTurn() + ": " + (position.isWhite() ? "white" : "black") + " to move");
            if (--stopAtTurn <= 0) {
                line = bufferedReader.readLine();
                PositionSearchResult lastResult = game.getLastEvaluatorResult();
                if (line.equals("e") && lastResult != null) {
                    // PositionUtil.printResult(lastResult);
                    // TODO
                    line = bufferedReader.readLine();
                } else if (line.length() >= 4) {
                    game.play(line);
                } else if (line.equals("-")) {
                    play(game, evaluator);
                } else if (line.equals("m")) {
                    System.out.println(game.movesToString());
                } else if (line.equals("fen")) {
                    System.out.println(PositionUtil.toFEN(position));
                } else if (line.equals("a")) {
                    analyzeBestMove(position, lastResult);
                }
            } else {
                play(game, evaluator);
                System.out.println(game.getMoves().stream().map(Move::toString).collect(Collectors.joining(" ")));
            }
            if (checkGameFinished(game.getLastEvaluatorResult())) {
                stopAtTurn = 0;
            }
        } while (!line.equals("quit"));

        ChessH2.close();
    }

    /**
     * Reanalyzes the best move. Can be used for debugging.
     *
     * @param position        the position
     * @param evaluatorResult the calculated result
     */
    private static void analyzeBestMove(Position position, PositionSearchResult evaluatorResult) {
        MoveExecutor moveExecutor = new MoveExecutor();
        PositionEvaluator analyzer = new DefaultPositionEvaluator();
        LinkedList<MoveNode> list = evaluatorResult.getBestMovePath().list();
        Position copyPosition = position.copy();
        analyzer.analyze(copyPosition);
        for (int i = 1; i < list.size(); i++) {
            MoveNode moveNode = list.get(i);
            moveExecutor.forward(copyPosition, moveNode.getMove());
            analyzer.analyze(copyPosition);
        }
    }

    private static void play(Game game, IterativePositionSearcher searcher) {
        Position position = game.getPosition();
        System.out.println("calculating " + (position.isWhite() ? "white" : "black") + " on turn " + position.getTurn());
        // TODO 2000 timeout
        IterativePositionSearchResult result = searcher.search(position, game.getPositionHashList());
        if (!checkGameFinished(game.getLastEvaluatorResult())) {
            // TODO nodes/ms
            //int positions = evaluator.numPositionsInCache();
            //System.out.println(positions + " nodes in " + result.took() + "ms - " + (positions / result.took()) + "node/ms");
            MovePath movePath = game.play(result, searcher.getVariance());
            System.out.println(movePath.head().getValue() + " - " + movePath);
            // WRITE TO DB
            // ChessH2.write(result);
        }
    }

    private static boolean checkGameFinished(PositionSearchResult result) {
        if (result != null && result.isGameFinished()) {
            System.out.println(result.hasWhiteWon() ? "white won" : result.hasBlackWon() ? "black won" : "draw");
            return true;
        }
        return false;
    }

}
