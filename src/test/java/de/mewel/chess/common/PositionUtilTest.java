package de.mewel.chess.common;

import de.mewel.chess.model.PieceNotation;
import de.mewel.chess.model.Position;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static de.mewel.chess.model.PieceNotation.*;
import static org.junit.Assert.*;

public class PositionUtilTest {

    @Test
    public void checkCastle() {
        MoveExecutor moveExecutor = new MoveExecutor();
        Position p = PositionUtil.base();

        assertFalse("Cannot castle at base position", PositionUtil.checkCastle(p, 4, 0, true));
        assertFalse("Cannot castle at base position", PositionUtil.checkCastle(p, 4, 0, false));

        // remove stuff
        removeBetweenPieces(p);
        assertTrue("Can castle short when everything is removed between king and rook", PositionUtil.checkCastle(p, 4, 0, true));
        assertTrue("Can castle long when everything is removed between king and rook", PositionUtil.checkCastle(p, 4, 0, false));

        // move rook
        moveExecutor.forward(p, p.move(0, 0, 0, 1));
        moveExecutor.forward(p, p.move(7, 0, 7, 1));
        assertFalse("Cannot castle when rook is moved", PositionUtil.checkCastle(p, 4, 0, true));
        assertFalse("Cannot castle when rook is moved", PositionUtil.checkCastle(p, 4, 0, false));

        // check invalid castle position for white -> king or rook was moved before
        p = PositionUtil.base();
        removeBetweenPieces(p);
        p.setCastleWhite(0);
        assertFalse("White cannot castle when other piece is moved", PositionUtil.checkCastle(p, 4, 0, true));
        assertFalse("White cannot castle when other piece is moved", PositionUtil.checkCastle(p, 4, 0, false));

        // but black can castle!
        p.setWhite(false);
        assertTrue("Black can castle", PositionUtil.checkCastle(p, 4, 7, true));
        assertTrue("Black can castle", PositionUtil.checkCastle(p, 4, 7, false));

        // check if some opponents messes up castling
        p = PositionUtil.empty();
        p.set(PieceNotation.W_KING, 4, 0);
        p.set(PieceNotation.W_ROOK, 0, 0);
        p.set(PieceNotation.W_ROOK, 7, 0);
        assertTrue("White can castle short", PositionUtil.checkCastle(p, 4, 0, true));
        assertTrue("White can castle long", PositionUtil.checkCastle(p, 4, 0, false));

        p.set(PieceNotation.B_QUEEN, 3, 3);
        assertFalse("White cannot castle short because of queen", PositionUtil.checkCastle(p, 4, 0, true));
        assertFalse("White cannot castle long because of queen", PositionUtil.checkCastle(p, 4, 0, false));

        p.set(EMPTY, 3, 3).set(PieceNotation.B_QUEEN, 0, 3);
        assertTrue("White can castle short because queen is not interfering", PositionUtil.checkCastle(p, 4, 0, true));
        assertFalse("White cannot castle long because of queen", PositionUtil.checkCastle(p, 4, 0, false));

        p.set(EMPTY, 0, 3).set(PieceNotation.B_QUEEN, 1, 4);
        assertFalse("White cannot castle short because queen is  interfering", PositionUtil.checkCastle(p, 4, 0, true));
        assertFalse("White cannot castle short because queen is  interfering", PositionUtil.checkCastle(p, 4, 0, false));

        p.set(W_PAWN, 1, 1).set(W_PAWN, 4, 1);
        assertTrue("White can castle short because queen is blocked by pawn", PositionUtil.checkCastle(p, 4, 0, true));
        assertTrue("White can castle short because queen is blocked by pawn", PositionUtil.checkCastle(p, 4, 0, false));

        p.set(EMPTY, 1, 4).set(PieceNotation.B_PAWN, 4, 1);
        assertFalse("White cannot castle with pawn in front of king", PositionUtil.checkCastle(p, 4, 0, true));
        assertFalse("White cannot castle with pawn in front of king", PositionUtil.checkCastle(p, 4, 0, false));

        p.set(EMPTY, 4, 1).set(B_PAWN, 3, 1);
        assertFalse("White cannot castle with pawn in front of king", PositionUtil.checkCastle(p, 4, 0, true));
        assertFalse("White cannot castle with pawn in front of king", PositionUtil.checkCastle(p, 4, 0, false));

        p.set(EMPTY, 3, 1).set(B_PAWN, 2, 1);
        assertTrue("White can castle short with pawn on c2", PositionUtil.checkCastle(p, 4, 0, true));
        assertFalse("White cannot castle long with pawn on c2", PositionUtil.checkCastle(p, 4, 0, false));

        p.empty(2, 1).set(B_KNIGHT, 4, 1);
        assertFalse("White cannot castle with knight in front of king", PositionUtil.checkCastle(p, 4, 0, true));
        assertFalse("White cannot castle with knight in front of king", PositionUtil.checkCastle(p, 4, 0, false));

        p.empty(4, 1).set(B_BISHOP, 3, 4).set(B_BISHOP, 4, 4).set(B_ROOK, 0, 7).set(B_ROOK, 7, 7);
        assertTrue("White can castle if rooks are attacked", PositionUtil.checkCastle(p, 4, 0, true));
        assertTrue("White can castle if rooks are attacked", PositionUtil.checkCastle(p, 4, 0, false));

        // check for black
        p = PositionUtil.empty().setWhite(false);
        p.set(PieceNotation.B_KING, 4, 7);
        p.set(PieceNotation.B_ROOK, 0, 7);
        p.set(PieceNotation.B_ROOK, 7, 7);
        assertTrue("Black can castle short", PositionUtil.checkCastle(p, 4, 7, true));
        assertTrue("Black can castle long", PositionUtil.checkCastle(p, 4, 7, false));

        p.set(PieceNotation.W_QUEEN, 3, 4);
        assertFalse("Black cannot castle short because of queen", PositionUtil.checkCastle(p, 4, 7, true));
        assertFalse("Black cannot castle long because of queen", PositionUtil.checkCastle(p, 4, 7, false));

        p.set(EMPTY, 3, 4).set(PieceNotation.W_QUEEN, 0, 4);
        assertTrue("Black can castle short because queen is not interfering", PositionUtil.checkCastle(p, 4, 7, true));
        assertFalse("Black cannot castle long because of queen", PositionUtil.checkCastle(p, 4, 7, false));

        p.set(EMPTY, 0, 4).set(PieceNotation.W_PAWN, 4, 6);
        assertFalse("Black cannot castle with pawn in front of king", PositionUtil.checkCastle(p, 4, 7, true));
        assertFalse("Black cannot castle with pawn in front of king", PositionUtil.checkCastle(p, 4, 7, false));

        p.set(EMPTY, 4, 6).set(W_PAWN, 3, 6);
        assertFalse("Black cannot castle with pawn in front of king", PositionUtil.checkCastle(p, 4, 7, true));
        assertFalse("Black cannot castle with pawn in front of king", PositionUtil.checkCastle(p, 4, 7, false));

        p.set(EMPTY, 3, 6).set(W_PAWN, 2, 6);
        assertTrue("Black can castle short with pawn on c2", PositionUtil.checkCastle(p, 4, 7, true));
        assertFalse("Black cannot castle long with pawn on c2", PositionUtil.checkCastle(p, 4, 7, false));

        p.empty(2, 6).set(W_KNIGHT, 4, 6);
        assertFalse("Black cannot castle with knight in front of king", PositionUtil.checkCastle(p, 4, 7, true));
        assertFalse("Black cannot castle with knight in front of king", PositionUtil.checkCastle(p, 4, 7, false));

        p.empty(4, 6).set(W_BISHOP, 3, 3).set(W_BISHOP, 4, 3).set(W_ROOK, 0, 0).set(W_ROOK, 7, 0);
        assertTrue("Black can castle if rooks are attacked", PositionUtil.checkCastle(p, 4, 7, true));
        assertTrue("Black can castle if rooks are attacked", PositionUtil.checkCastle(p, 4, 7, false));

        p.empty(3, 3).empty(4, 3).empty(0, 0).empty(7, 0).set(W_BISHOP, 7, 4);
        assertFalse("Black can't castle short if king is attacked", PositionUtil.checkCastle(p, 4, 7, true));
        assertFalse("Black can't castle long if king is attacked", PositionUtil.checkCastle(p, 4, 7, false));

        Position position = PositionUtil.base();
        moveExecutor.forward(position, "d2d4 d7d5 b1c3 b8c6 c1f4 e7e6 c3b5 f8b4 c2c3 b4a5 d1d3 a7a6 b5a3 d8f6 f4d2 a5c3 d2c3 f6h6");
        assertFalse("long castle shouldn't be possible", PositionUtil.list(position).contains(position.move("e1c1")));
        assertFalse("short castle shouldn't be possible", PositionUtil.list(position).contains(position.move("e1g1")));
    }

    private void removeBetweenPieces(Position p) {
        p.set(EMPTY, 1, 0);
        p.set(EMPTY, 2, 0);
        p.set(EMPTY, 3, 0);
        p.set(EMPTY, 5, 0);
        p.set(EMPTY, 6, 0);

        p.set(EMPTY, 1, 7);
        p.set(EMPTY, 2, 7);
        p.set(EMPTY, 3, 7);
        p.set(EMPTY, 5, 7);
        p.set(EMPTY, 6, 7);
    }

    @Test
    public void checkEnPassant() {
        Position p = PositionUtil.base();
        assertFalse(PositionUtil.checkEnPassant(p, 0, 1));

        MoveExecutor moveExecutor = new MoveExecutor();
        // b2 - b4
        moveExecutor.forward(p, p.move(1, 1, 1, 3));
        assertFalse(PositionUtil.checkEnPassant(p, 0, 6));
        assertFalse(PositionUtil.checkEnPassant(p, 1, 6));
        assertFalse(PositionUtil.checkEnPassant(p, 2, 6));

        // c7 - c5
        moveExecutor.forward(p, p.move(2, 6, 2, 4));
        assertFalse(PositionUtil.checkEnPassant(p, 1, 3));
        assertFalse(PositionUtil.checkEnPassant(p, 2, 1));
        assertFalse(PositionUtil.checkEnPassant(p, 3, 1));

        // b4 - b5
        moveExecutor.forward(p, p.move(1, 3, 1, 4));
        assertFalse(PositionUtil.checkEnPassant(p, 0, 6));
        assertFalse(PositionUtil.checkEnPassant(p, 1, 6));
        assertFalse(PositionUtil.checkEnPassant(p, 2, 4));

        // a7 - a5
        moveExecutor.forward(p, p.move(0, 6, 0, 4));
        assertFalse(PositionUtil.checkEnPassant(p, 0, 1));
        assertTrue(PositionUtil.checkEnPassant(p, 1, 4));
        assertFalse(PositionUtil.checkEnPassant(p, 2, 1));
    }

    @Ignore // TODO ignore for now
    @Test
    public void inCheckMoves() {
        MoveExecutor moveExecutor = new MoveExecutor();
        Position position = PositionUtil.base();
        moveExecutor.forward(position, position.move("e2e4"));
        moveExecutor.forward(position, position.move("e7e5"));
        moveExecutor.forward(position, position.move("g1f3"));
        moveExecutor.forward(position, position.move("b8c6"));
        moveExecutor.forward(position, position.move("f1b5"));
        moveExecutor.forward(position, position.move("a7a6"));
        moveExecutor.forward(position, position.move("b5a4"));
        moveExecutor.forward(position, position.move("d7d6"));
        moveExecutor.forward(position, position.move("e1g1"));
        moveExecutor.forward(position, position.move("g8f6"));
        moveExecutor.forward(position, position.move("f3e5"));
        moveExecutor.forward(position, position.move("d6e5"));
        moveExecutor.forward(position, position.move("a4c6"));
        List<Move> moves = PositionUtil.list(position);
        assertEquals(5, moves.size());

        position = PositionUtil.base();
        moveExecutor.forward(position, position.move("g1f3"));
        moveExecutor.forward(position, position.move("c7c6"));
        moveExecutor.forward(position, position.move("d2d3"));
        moveExecutor.forward(position, position.move("d8a5"));
        moves = PositionUtil.list(position);
        assertEquals(7, moves.size());
        assertTrue(PositionUtil.list(position).contains(position.move("b1d2")));
        assertTrue(PositionUtil.list(position).contains(position.move("b1c3")));
        assertTrue(PositionUtil.list(position).contains(position.move("b2b4")));
        assertTrue(PositionUtil.list(position).contains(position.move("c2c3")));
        assertTrue(PositionUtil.list(position).contains(position.move("f3d2")));
        assertTrue(PositionUtil.list(position).contains(position.move("d1d2")));
        assertTrue(PositionUtil.list(position).contains(position.move("c1d2")));

        position = PositionUtil.base();
        moveExecutor.forward(position, "f2f3 e7e5 e2e4 f8b4 g1h3 g8f6 b2b3 c7c6 c2c4 d8c7 a2a3 c7a5 a1a2 d7d6 h3f2 c8e6 f3f4 e5f4 d1f3 g7g5 f1e2 b8a6 a2b2 b4c5 g2g3 c5d4 b3b4 a5a4 h1g1 d4b2 c1b2 a4c2 b2f6 c2b1 e2d1 b1c1 f6e5 d6e5 g3f4 c1c4 f4e5 e8f8 g1g5 f8e7 f3f6 e7d7 g5g3 c4d4 g3d3 d4d3 f2d3 h8g8 f6h4 g8g1 e1e2 a8h8 h4f2 e6g4 e2e3 g1d1 f2f7 d7c8 f7g7 h8d8 g7g4 c8c7 g4d1 d8a8 e3f4 a8e8 d1a4 e8f8 f4g5 f8g8 g5h6 g8g2 a4a5 c7d7 h6h7 g2h2 h7g7 h2d2 d3f4 d7e7 b4b5 d2d7 b5a6 e7e8 g7h8 b7a6 f4h5 d7b7 e5e6 b7b5 h5g7 e8e7 a5c7 e7f6 h8g8 b5b3 e6e7 b3b8");
        moves = PositionUtil.list(position);
        assertEquals(7, moves.size());

        position = PositionUtil.empty().set(W_KING, 4, 0).set(B_KING, 2, 0).set(W_ROOK, 7, 0);
        moveExecutor.forward(position, "e1g1");
        moves = PositionUtil.list(position);
        assertEquals(3, moves.size());
        assertTrue(PositionUtil.list(position).contains(position.move("c1b2")));
        assertTrue(PositionUtil.list(position).contains(position.move("c1c2")));
        assertTrue(PositionUtil.list(position).contains(position.move("c1d2")));
    }

    @Test
    public void quietMoves() {
        MoveExecutor moveExecutor = new MoveExecutor();
        Position position = PositionUtil.base();
        moveExecutor.forward(position, position.move("e2e4"));
        moveExecutor.forward(position, position.move("e7e5"));
        moveExecutor.forward(position, position.move("g1f3"));
        moveExecutor.forward(position, position.move("b8c6"));
        moveExecutor.forward(position, position.move("f1b5"));
        moveExecutor.forward(position, position.move("a7a6"));
        moveExecutor.forward(position, position.move("b5a4"));
        moveExecutor.forward(position, position.move("d7d6"));
        moveExecutor.forward(position, position.move("e1g1"));
        moveExecutor.forward(position, position.move("g8f6"));
        moveExecutor.forward(position, position.move("f3e5"));
        moveExecutor.forward(position, position.move("d6e5"));
        List<Move> moves = PositionUtil.listQuiescenceMoves(position);
        assertEquals(1, moves.size());

        position = PositionUtil.base();
        moveExecutor.forward(position, "g2g4 b8a6 f2f4 e7e5 g4g5 b7b5 f1g2 a8b8 e2e3 f8d6 c2c4 e5f4 d1f3 c8b7 c4b5 f4e3 f3b7 e3e2 b5a6");
        moves = PositionUtil.listQuiescenceMoves(position);
        assertEquals(3, moves.size());

        position = PositionUtil.base();
        moveExecutor.forward(position, "f2f4 g7g6 e2e4 b8c6 d2d3 e7e6 g2g4 g8f6 d3d4 f8e7 f1e2 e7b4 c1d2 b4e7 d4d5 e6d5 a2a3 f6e4 g1h3 h8f8 d2c3 e7h4 e1f1 d8e7 a1a2 d5d4 c3d4 e7d6 d4g7 f7f5 d1c1 d6f4 c1f4 e8f7");
        moves = PositionUtil.listQuiescenceMoves(position);
        assertEquals(5, moves.size());
        assertTrue(PositionUtil.listQuiescenceMoves(position).contains(position.move("f4f5")));
        assertTrue(PositionUtil.listQuiescenceMoves(position).contains(position.move("f4e4")));
        assertTrue(PositionUtil.listQuiescenceMoves(position).contains(position.move("f4c7")));
        assertTrue(PositionUtil.listQuiescenceMoves(position).contains(position.move("g4f5")));
        assertTrue(PositionUtil.listQuiescenceMoves(position).contains(position.move("g7f8")));

        position = PositionUtil.fen("rn1Q4/ppb2q2/2p2ppk/2P1p2p/1P2P1bP/P5N1/2Bp1KP1/R1B4R b - - 0 44");
        moves = PositionUtil.listQuiescenceMoves(position);
        assertEquals(5, moves.size());
    }

    @Test
    public void getCheckingPiece() {
        MoveExecutor moveExecutor = new MoveExecutor();
        Position p1 = PositionUtil.empty()
                .set(W_KING, 4, 0).set(B_QUEEN, 0, 4).set(B_PAWN, 1, 3).set(W_PAWN, 7, 1);
        moveExecutor.forward(p1, "e1f1");
        assertFalse("black king not in check", p1.isInCheck());
        moveExecutor.forward(p1, "a5b5");
        assertEquals("black queen give check", Collections.singletonList(PieceUtil.fromByte(B_QUEEN, 1, 4)), p1.getCheckingPieces());
        moveExecutor.forward(p1, "f1g1");
        moveExecutor.forward(p1, "b5g5");
        assertEquals("black queen give check", Collections.singletonList(PieceUtil.fromByte(B_QUEEN, 6, 4)), p1.getCheckingPieces());
        moveExecutor.forward(p1, "g1h1");
        moveExecutor.forward(p1, "g5h5");
        assertFalse("white king not in check cause its protected by pawn", p1.isInCheck());

        Position p2 = PositionUtil.empty()
                .set(W_KING, 2, 0).set(B_QUEEN, 0, 2).set(B_KNIGHT, 1, 1).setWhite(false);
        moveExecutor.forward(p2, "b2d3");
        assertEquals("black queen and black knight give check",
                Arrays.asList(PieceUtil.fromByte(B_KNIGHT, 3, 2), PieceUtil.fromByte(B_QUEEN, 0, 2)),
                p2.getCheckingPieces());

        Position p3 = PositionUtil.empty()
                .set(W_KING, 1, 0).set(B_PAWN, 0, 2).set(B_PAWN, 1, 3).setWhite(false);
        moveExecutor.forward(p3, "a3a2");
        assertEquals("black queen give check", Collections.singletonList(PieceUtil.fromByte(B_PAWN, 0, 1)), p3.getCheckingPieces());
        moveExecutor.forward(p3, "b1a1");
        moveExecutor.forward(p3, "b4b3");
        assertFalse("white king not in check", p3.isInCheck());

        Position p4 = PositionUtil.empty()
                .set(W_KING, 7, 0).set(W_PAWN, 6, 1).set(B_PAWN, 7, 3).set(B_QUEEN, 7, 5);
        moveExecutor.forward(p4, "g2g4");
        moveExecutor.forward(p4, "h4g3");
        assertTrue("white king is in check", p4.isInCheck());

        Position p5 = PositionUtil.empty()
                .set(W_KING, 3, 0).set(B_ROOK, 2, 6).set(B_ROOK, 2, 5).setWhite(false);
        moveExecutor.forward(p5, "c6c2");
        assertFalse("white king is not in check", p5.isInCheck());
    }

    @Test
    public void listValid() {
        Position position = PositionUtil.fen("r1bqkb1r/2p2p2/ppQ2p1p/8/P1B5/2N5/1PP2PPP/R1B1K2R b KQkq - 0 6");
        position.setCheckingPieces(PositionUtil.getCheckingPieces(position, !position.isWhite()));
        List<Move> moves = PositionUtil.listValid(position);
        assertEquals(3, moves.size());
        assertTrue(moves.contains(position.move("c8d7")));
        assertTrue(moves.contains(position.move("d8d7")));
        assertTrue(moves.contains(position.move("e8e7")));
    }


    @Ignore // TODO
    @Test
    public void pinnedPieces() {
        Position p = PositionUtil.empty();
        p.clear().set(W_KING, 3, 0).set(W_KNIGHT, 3, 1).set(B_ROOK, 3, 5).set(B_KING, 6, 4);
        assertEquals(4, PositionUtil.list(p).size());

        p.clear().set(W_KING, 3, 0).set(W_ROOK, 3, 1).set(B_ROOK, 3, 5).set(B_KING, 6, 4);
        assertEquals(8, PositionUtil.list(p).size());
        assertTrue(PositionUtil.list(p).contains(p.move("d1e2")));
        assertTrue(PositionUtil.list(p).contains(p.move("d1e1")));
        assertTrue(PositionUtil.list(p).contains(p.move("d1c1")));
        assertTrue(PositionUtil.list(p).contains(p.move("d1c2")));
        assertTrue(PositionUtil.list(p).contains(p.move("d2d3")));
        assertTrue(PositionUtil.list(p).contains(p.move("d2d4")));
        assertTrue(PositionUtil.list(p).contains(p.move("d2d5")));
        assertTrue(PositionUtil.list(p).contains(p.move("d2d6")));

        p.clear().set(W_KING, 3, 0).set(W_ROOK, 2, 1).set(B_BISHOP, 0, 3).set(B_KING, 7, 7);
        assertEquals(4, PositionUtil.list(p).size());

        p.clear().set(W_KING, 3, 0).set(W_BISHOP, 2, 1).set(B_BISHOP, 0, 3).set(B_KING, 7, 7);
        assertEquals(6, PositionUtil.list(p).size());

        p.clear().set(W_KING, 3, 0).set(W_QUEEN, 2, 1).set(B_BISHOP, 0, 3).set(B_KING, 7, 7);
        assertEquals(6, PositionUtil.list(p).size());

        p.clear().set(W_KING, 3, 0).set(W_PAWN, 2, 1).set(B_BISHOP, 0, 3).set(B_KING, 7, 7);
        assertEquals(4, PositionUtil.list(p).size());

        p.clear().set(W_KING, 3, 0).set(W_KNIGHT, 2, 1).set(B_BISHOP, 0, 3).set(B_KING, 7, 7);
        assertEquals(4, PositionUtil.list(p).size());

        p.clear().set(W_KING, 3, 0).set(W_PAWN, 3, 3).set(B_QUEEN, 3, 4).set(B_KING, 7, 7);
        assertEquals(5, PositionUtil.list(p).size());

        p.clear().set(W_KING, 3, 0).set(W_PAWN, 3, 1).set(B_QUEEN, 3, 4).set(B_KING, 7, 7);
        assertEquals(6, PositionUtil.list(p).size());
        assertTrue(PositionUtil.list(p).contains(p.move("d1e2")));
        assertTrue(PositionUtil.list(p).contains(p.move("d1e1")));
        assertTrue(PositionUtil.list(p).contains(p.move("d1c1")));
        assertTrue(PositionUtil.list(p).contains(p.move("d1c2")));
        assertTrue(PositionUtil.list(p).contains(p.move("d2d3")));
        assertTrue(PositionUtil.list(p).contains(p.move("d2d4")));

        p.clear().set(W_KING, 3, 0).set(W_BISHOP, 3, 2).set(B_QUEEN, 3, 4).set(B_KING, 7, 7);
        assertEquals(5, PositionUtil.list(p).size());

        p.clear().set(W_KING, 4, 0).set(W_PAWN, 3, 1).set(B_QUEEN, 0, 4).set(B_KING, 7, 7);
        assertEquals(4, PositionUtil.list(p).size());

        p.clear().set(W_KING, 4, 1).set(W_PAWN, 5, 1).set(B_ROOK, 7, 1).set(B_KING, 7, 7);
        assertEquals(7, PositionUtil.list(p).size());

        Position position = PositionUtil.base();
        MoveExecutor moveExecutor = new MoveExecutor();
        moveExecutor.forward(position, "d2d4 g8f6 e2e3 g7g6 f1b5 f8g7 g1f3 a7a6 b5d3 d7d5 f3e5 e8g8 b1c3 d8d6 c1d2 b8c6 e5c6 d6c6 d1f3 c8g4 f3g3 g4h5 f2f3 b7b5 g3f4 c6e6 f4c7 g7h6 c7e5 g6g5 e5e6");
        List<Move> moves = PositionUtil.list(position);
        assertTrue(moves.contains(position.move("f7e6")));
    }

}
