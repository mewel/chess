package de.mewel.chess.model;

import de.mewel.chess.common.Move;
import de.mewel.chess.common.PositionUtil;

import java.util.ArrayList;
import java.util.List;

public class Bishop extends AbstractPiece {

    public Bishop(boolean white, int x, int y) {
        super(white, x, y);
    }

    public String getName() {
        return "bishop";
    }

    public short getValue() {
        return 3;
    }

    @Override
    public byte toByte() {
        return isWhite() ? PieceNotation.W_BISHOP : PieceNotation.B_BISHOP;
    }

    @Override
    public List<Move> moves(Position position) {
        return PositionUtil.moveDiagonal(position, x(), y());
    }

    @Override
    public List<Move> validMoves(Position position) {
        return moves(position);
    }

    @Override
    public List<Move> captures(Position position) {
        return PositionUtil.captureDiagonal(position, x(), y());
    }

    @Override
    public List<Move> pinnedMoves(Position position, King king, Piece pinningPiece) {
        ArrayList<Move> moves = new ArrayList<>();
        int pinningDiffX = Math.abs(x() - pinningPiece.x());
        if (pinningDiffX == 0 || Math.abs(y() - pinningPiece.y()) == 0) {
            return moves; // horizontal/vertical
        }
        // move in direction of pinning piece
        int moveX = x() > pinningPiece.x() ? -1 : 1;
        int moveY = y() > pinningPiece.y() ? -1 : 1;
        for (int i = 1; i <= pinningDiffX; i++) {
            PositionUtil.moveOrCapture(moves, position, x(), y(), x() + i * moveX, y() + i * moveY);
        }
        // move in direction of king
        int kingDiffX = Math.abs(x() - king.x());
        for (int i = 1; i < kingDiffX; i++) {
            if (!PositionUtil.move(moves, position, x(), y(), x() + i * moveX * -1, y() + i * moveY * -1)) {
                break;
            }
        }
        return moves;
    }

    @Override
    public List<Move> inCheckMoves(Position position) {
        List<Move> moves = new ArrayList<>();
        List<Piece> checkingPieces = position.getCheckingPieces();
        if (checkingPieces.size() > 1) {
            return moves;
        }
        Piece checkingPiece = checkingPieces.get(0);
        King king = PositionUtil.getKing(position, position.isWhite());
        PositionUtil.diagonalIntersectAndCapture(position, this, checkingPiece, king, moves);
        return moves;
    }

    @Override
    public byte preventsCastling(Position position) {
        return PositionUtil.checkCastleDiagonal(position, x(), y());
    }

}
