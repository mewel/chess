package de.mewel.chess.model;

import de.mewel.chess.common.Move;

import java.util.List;

/**
 * Defines a chess piece. Including King, Queen, Rook, Bishop, Knight and Pawn.
 *
 * @author mewel
 */
public interface Piece {

    String getName();

    short getValue();

    boolean isWhite();

    Piece square(int x, int y);

    int x();

    int y();

    /**
     * Returns the internal notation for this piece.
     *
     * @return the internal notation
     */
    byte toByte();

    /**
     * Returns a collection of positions the piece can stand after moving/capturing.
     * Be aware that this just check for empty fields where
     * the piece can move and not if the move itself is valid
     * (in case the move leaves the king at check).
     *
     * @param position the current position
     * @return list of possible positions
     */
    List<Move> moves(Position position);

    List<Move> validMoves(Position position);

    /**
     * List of capturing moves (includes en passant).
     *
     * @param position the current position
     * @return list of moves
     */
    List<Move> captures(Position position);

    /**
     * List of moves which only including captures, en passant and promotes.
     *
     * @param position the current position
     * @return list of moves
     */
    List<Move> quiescenceMoves(Position position);

    /**
     * List of moves when you are in check.
     *
     * @param position the position
     * @return list of moves
     */
    List<Move> inCheckMoves(Position position);

    /**
     * List of moves if you got pinned by a piece
     *
     * @param position     the position
     * @param king         the king
     * @param pinningPiece piece which is pinning you
     * @return list of valid moves
     */
    List<Move> pinnedMoves(Position position, King king, Piece pinningPiece);

    /**
     * <p>Checks if this piece prevents castling for the opponent.</p>
     *
     * <ul>
     * <li>0 = doesn't prevents</li>
     * <li>1 = prevents short castle</li>
     * <li>2 = prevents long castle</li>
     * <li>3 = prevents both short and long castle</li>
     * </ul>
     *
     * @param position the position to check
     * @return a byte between 0 and 3
     */
    byte preventsCastling(Position position);

}
