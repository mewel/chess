package de.mewel.chess.renderer;

import de.mewel.chess.common.*;
import de.mewel.chess.engine.*;
import de.mewel.chess.engine.evaluator.PiecePositionEvaluator;
import de.mewel.chess.engine.model.*;
import de.mewel.chess.model.Piece;
import de.mewel.chess.model.PieceNotation;
import de.mewel.chess.model.Position;

import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.*;
import java.awt.font.GlyphVector;
import java.awt.geom.AffineTransform;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

public class PositionEvaluatorUI {

    private JFrame frame;

    private JPanel board;

    private JList<MoveEntry> moveList;

    private final Square[][] squares;

    private Position position;

    private Position positionOnCalculate;

    private boolean autoEval;

    private int[] pieceEvaluation;

    private List<Position> history;

    private List<Position> historyOnCalculate;

    private int historyNum;

    private int historyNumOnCalculate;

    private final TimedTaskRunner timedTaskRunner;

    public PositionEvaluatorUI() {
        this.squares = new Square[8][8];
        this.autoEval = true;
        this.history = new ArrayList<>();
        this.historyNum = -1;
        this.timedTaskRunner = new TimedTaskRunner();
    }

    public Future<Void> show() {
        return CompletableFuture.runAsync(() -> {
            this.frame = new JFrame();
            this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            this.frame.setLayout(new BorderLayout());

            JPanel boardPanel = createBoard();
            this.board = createLabeledBoard(boardPanel);

            this.frame.add(board, BorderLayout.CENTER);
            this.frame.add(createRightPanel(), BorderLayout.EAST);
            this.frame.setJMenuBar(buildMenuBar());
            this.frame.setSize(950, 800);
            this.frame.setVisible(true);
            setupKeyBindings();
            center();
        });
    }

    private JPanel createBoard() {
        JPanel board = new JPanel(new GridLayout(8, 8));
        for (int y = 7; y >= 0; y--) {
            for (int x = 0; x < 8; x++) {
                squares[y][x] = new Square(x, y);
                board.add(squares[y][x]);
                squares[y][x].addMouseListener(new MouseAdapter() {
                    @Override
                    public void mousePressed(MouseEvent e) {
                        selected((Square) e.getSource());
                    }
                });
            }
        }
        return board;
    }

    private JPanel createLabeledBoard(JPanel boardPanel) {
        JPanel labeledBoard = new JPanel(new BorderLayout());
        labeledBoard.add(boardPanel, BorderLayout.CENTER);
        // Column labels (a-h) at the top and bottom
        JPanel bottomLabels = new JPanel(new GridLayout(1, 8));
        for (char col = 'a'; col <= 'h'; col++) {
            bottomLabels.add(new JLabel(String.valueOf(col), SwingConstants.CENTER));
        }
        // Row labels (1-8) on the left and right
        JPanel rightLabels = new JPanel(new GridLayout(8, 1));
        for (int row = 8; row >= 1; row--) {
            rightLabels.add(new JLabel(String.valueOf(row), SwingConstants.CENTER));
        }
        labeledBoard.add(bottomLabels, BorderLayout.SOUTH);
        labeledBoard.add(rightLabels, BorderLayout.EAST);
        return labeledBoard;
    }

    private JPanel createRightPanel() {
        JPanel rightPanel = new JPanel();
        rightPanel.setLayout(new BoxLayout(rightPanel, BoxLayout.Y_AXIS));
        rightPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        JTextField maxFullNodes = new JTextField(4);
        JTextField maxQuiescenceNodes = new JTextField(4);
        JTextField variance = new JTextField(4);
        maxFullNodes.setText("4");
        maxQuiescenceNodes.setText("12");
        variance.setText("15");
        JPanel inputPanel = new JPanel();
        inputPanel.add(maxFullNodes);
        inputPanel.add(maxQuiescenceNodes);
        inputPanel.add(variance);
        inputPanel.setMaximumSize(new Dimension(Integer.MAX_VALUE, maxFullNodes.getPreferredSize().height));

        JCheckBox calculateAllRootNodes = new JCheckBox("Calculate All Root Nodes");

        JButton fixedCalculateButton = new JButton("Fixed");
        fixedCalculateButton.addActionListener(e -> {
            PositionSearcherSettings settings = new PositionSearcherSettings(
                    Integer.parseInt(maxFullNodes.getText()),
                    Integer.parseInt(maxQuiescenceNodes.getText()),
                    Integer.parseInt(variance.getText())
            );
            FixedPositionSearcher searcher = new FixedPositionSearcher(settings, new PiecePositionEvaluator(), new PositionSearchCache(1_000_000));
            searcher.setCalculateAllRootLevelNodes(calculateAllRootNodes.isSelected());
            try {
                FixedPositionSearchResult result = searcher.search(position);
                if(!calculateAllRootNodes.isSelected()) {
                    updateMoveList(result, settings.variance());
                } else {
                    Map<MovePath, Integer> moveMap = result.getMoveMap();
                    updateMoveList(moveMap);
                }
            } catch (PositionSearcherCanceledException ex) {
                throw new RuntimeException(ex);
            }
        });
        IterativePositionSearcher iterativeSearcher = new IterativePositionSearcher(new PiecePositionEvaluator());
        iterativeSearcher.setListener(new IterativePositionSearcher.IterativePositionSearcherListener() {
            @Override
            public void onResult(PositionSearchResult result) {
                System.out.println("on result: " + result.getBestMovePath());
            }

            @Override
            public void onFinished(IterativePositionSearchResult result) {
                System.out.println("on finished: " + result.getBestMovePath());
                updateMoveList(result, Integer.parseInt(variance.getText()));
            }
        });

        JButton iterativeCalculateButton = new JButton("Iterative");
        iterativeCalculateButton.addActionListener(e -> {
            if (!timedTaskRunner.isRunning()) {
                iterativeSearcher.setVariance(Integer.parseInt(variance.getText()));
                iterativeCalculateButton.setText("Stop");
                iterativeSearcher.search(position, new LinkedList<>());
                timedTaskRunner.start(iterativeSearcher);
            } else {
                timedTaskRunner.cancel();
                iterativeCalculateButton.setText("Iterative");
            }
        });

        rightPanel.add(calculateAllRootNodes);
        rightPanel.add(inputPanel);
        rightPanel.add(fixedCalculateButton);
        rightPanel.add(iterativeCalculateButton);

        // Initialize the move list
        moveList = new JList<>();
        moveList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        moveList.addListSelectionListener(e -> {
            board.requestFocus();
            if (!e.getValueIsAdjusting()) {
                this.position = positionOnCalculate;
                this.history = new ArrayList<>(historyOnCalculate.stream().map(Position::copy).toList());
                this.historyNum = historyNumOnCalculate;
                MoveEntry selectedEntry = moveList.getSelectedValue();
                if (selectedEntry != null) {
                    MoveExecutor moveExecutor = new MoveExecutor();
                    Position positionCopy = position.copy();
                    for (MoveNode moveNode : selectedEntry.getMovePath().list()) {
                        moveExecutor.forward(positionCopy, moveNode.getMove());
                        this.history.add(positionCopy);
                        positionCopy = positionCopy.copy();
                    }
                }
                firePositionUpdated();
            }
        });
        moveList.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                if (e.isPopupTrigger()) {
                    doPop(e);
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if (e.isPopupTrigger()) {
                    doPop(e);
                }
            }

            private void doPop(MouseEvent e) {
                MoveListPopup menu = new MoveListPopup();
                menu.show(e.getComponent(), e.getX(), e.getY());
            }

            class MoveListPopup extends JPopupMenu {
                final JMenuItem copyLine;

                public MoveListPopup() {
                    copyLine = new JMenuItem("Copy Line");
                    copyLine.addActionListener(e -> {
                        Clipboard systemClipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                        systemClipboard.setContents(new StringSelection(moveList.getSelectedValue().getMovePath().toString()), null);
                    });
                    add(copyLine);
                }
            }

        });
        moveList.setCellRenderer(new DefaultListCellRenderer() {
            @Override
            public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                Component line = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                if (value instanceof MoveEntry moveEntry) {
                    if (!moveEntry.movePath.isFullyResolved()) {
                        line.setBackground(Color.PINK);
                    }
                }
                return line;
            }
        });

        // Add the move list to the right panel, wrapped in a JScrollPane
        JScrollPane moveListScrollPane = new JScrollPane(moveList);
        moveListScrollPane.setPreferredSize(new Dimension(200, 400)); // Adjust size as needed
        rightPanel.add(moveListScrollPane);

        return rightPanel;
    }

    private void updateMoveList(Map<MovePath, Integer> moveMap) {
        resetHistory();
        // Prepare data for the JList
        List<MoveEntry> moveEntries = new ArrayList<>();
        for (Map.Entry<MovePath, Integer> entry : moveMap.entrySet()) {
            MovePath movePath = entry.getKey();
            int evaluation = entry.getValue();
            moveEntries.add(new MoveEntry(movePath, evaluation));
        }
        // Update the JList
        moveList.setListData(moveEntries.toArray(new MoveEntry[0]));
    }

    private void updateMoveList(PositionSearchResult result, int variance) {
        resetHistory();
        List<MovePath> candidates = result.listCandidates(variance);
        List<MoveEntry> moveEntries = candidates.stream()
                .map(movePath -> new MoveEntry(movePath, movePath.getValue()))
                .toList();
        moveList.setListData(moveEntries.toArray(new MoveEntry[0]));
    }

    private void resetHistory() {
        this.positionOnCalculate = position;
        this.historyOnCalculate = new ArrayList<>(history.stream().map(Position::copy).limit(historyNum + 1).toList());
        this.historyNumOnCalculate = historyNum;
    }

    private void setupKeyBindings() {
        InputMap inputMap = frame.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        ActionMap actionMap = frame.getRootPane().getActionMap();

        // DELETE key
        KeyStroke deleteKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0);
        inputMap.put(deleteKeyStroke, "deleteAction");
        actionMap.put("deleteAction", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                deleteSelectedPiece();
            }
        });

        // 0-9 to add PIECES
        for (char keyChar = '0'; keyChar <= '9'; keyChar++) {
            final char pieceChar = keyChar;
            KeyStroke keyStroke = KeyStroke.getKeyStroke(pieceChar);
            String actionName = "setPiece" + pieceChar;
            inputMap.put(keyStroke, actionName);
            actionMap.put(actionName, new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    setSelectedPiece(pieceChar);
                }
            });
        }

        // LEFT and RIGHT arrow keys
        KeyStroke leftKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0);
        KeyStroke rightKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0);

        inputMap.put(leftKeyStroke, "leftArrowAction");
        inputMap.put(rightKeyStroke, "rightArrowAction");

        actionMap.put("leftArrowAction", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                handleLeftArrow();
            }
        });
        actionMap.put("rightArrowAction", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                handleRightArrow();
            }
        });
    }


    private JMenuBar buildMenuBar() {
        JMenuBar menuBar = new JMenuBar();
        JMenu positionMenu = new JMenu("Position");

        // NEW
        JMenuItem newGame = new JMenuItem("New Game");
        newGame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setPosition(PositionUtil.base());
            }
        });

        // set FEN
        JMenuItem setFen = new JMenuItem("Set FEN");
        setFen.addActionListener(e -> {
            String fen = JOptionPane.showInputDialog("Input FEN");
            if (fen != null && !fen.isEmpty()) {
                Position position = PositionUtil.fen(fen);
                position.setWhite(!position.isWhite());
                position.setCheckingPieces(PositionUtil.getCheckingPieces(position));
                position.setWhite(!position.isWhite());
                setPosition(position);
            }
        });
        // copy FEN
        JMenuItem copyFen = new JMenuItem("Copy FEN");
        copyFen.addActionListener(e -> {
            Clipboard systemClipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
            systemClipboard.setContents(new StringSelection(PositionUtil.toFEN(position)), null);
        });
        positionMenu.add(newGame);
        positionMenu.add(setFen);
        positionMenu.add(copyFen);
        menuBar.add(positionMenu);
        return menuBar;
    }

    public void setPosition(Position position) {
        this.position = position;
        this.firePositionUpdated();
        // update history
        if (this.historyNum + 1 < this.history.size()) {
            this.history = this.history.subList(0, this.historyNum);
        }
        this.history.add(position);
        this.historyNum = this.history.size() - 1;
    }

    private void updateUI() {
        this.renderPosition();
        if (this.autoEval) {
            this.pieceEvaluation = this.autoEvaluate();
        }
        int eval = this.renderEvaluation();
        this.frame.setTitle("Position Evaluator UI # eval=" + eval);
    }

    private void renderPosition() {
        for (int y = 0; y < 8; y++) {
            for (int x = 0; x < 8; x++) {
                byte b = position.get(x, y);
                squares[y][x].setPiece(PieceUtil.fromByte(b, x, y));
                squares[y][x].repaint();
            }
        }
    }

    private int renderEvaluation() {
        int value = 0;
        for (int y = 0; y < 8; y++) {
            for (int x = 0; x < 8; x++) {
                byte piece = position.get(x, y);
                int pieceEvaluationValue = pieceEvaluation[x + y * 8];
                squares[y][x].setEvaluation(pieceEvaluationValue);
                squares[y][x].repaint();
                value += PieceUtil.isWhite(piece) ? pieceEvaluationValue : -pieceEvaluationValue;
            }
        }
        return value;
    }

    private List<Square> allSquares() {
        List<Square> squareList = new ArrayList<>();
        for (int y = 0; y < 8; y++) {
            for (int x = 0; x < 8; x++) {
                squareList.add(this.squares[y][x]);
            }
        }
        return squareList;
    }

    private int[] autoEvaluate() {
        PiecePositionEvaluator analyzer = new PiecePositionEvaluator();
        return analyzer.analyzePosition(position);
    }

    private void selected(Square square) {
        allSquares().forEach(s -> {
            s.setDot(false);
            s.repaint();
        });
        if (square.isSelected()) {
            square.setSelected(false);
            square.repaint();
            return;
        }
        Square oldSelected = getSelected();
        if (oldSelected != null) {
            if (square.piece != null && PieceUtil.isKing(square.piece.toByte())) {
                return;
            }
            oldSelected.setSelected(false);
            oldSelected.repaint();
            if (oldSelected.piece == null) {
                square.setSelected(true);
                square.repaint();
                return;
            }
            Position newPosition = position.copy();
            newPosition.set(PieceNotation.EMPTY, oldSelected.x, oldSelected.y);
            newPosition.set(oldSelected.piece.toByte(), square.x, square.y);
            newPosition.setWhite(!position.isWhite());
            setPosition(newPosition);
            firePositionUpdated();
        } else {
            square.setSelected(true);
            square.repaint();
            if (square.piece != null) {
                position.setWhite(square.piece.isWhite());
                List<Move> moves = PositionUtil.listMovesForPiece(position, square.piece);
                for (Move move : moves) {
                    Square dotSquare = squares[move.getToY()][move.getToX()];
                    dotSquare.dot = true;
                    dotSquare.repaint();
                }
            }
        }
    }

    private void printPosition() {
        System.out.println("position " + PositionUtil.toFEN(position));
    }

    private Square getSelected() {
        for (int y = 0; y < 8; y++) {
            for (int x = 0; x < 8; x++) {
                if (squares[y][x].isSelected()) {
                    return squares[y][x];
                }
            }
        }
        return null;
    }

    private void deleteSelectedPiece() {
        Square selected = this.getSelected();
        if (selected == null || selected.piece == null || PieceUtil.isKing(selected.piece.toByte())) {
            return;
        }
        Position newPosition = position.copy();
        newPosition.set(PieceNotation.EMPTY, selected.x, selected.y);
        setPosition(newPosition);
        firePositionUpdated();
    }

    private void setSelectedPiece(char keyChar) {
        Square selected = this.getSelected();
        if (selected == null) {
            return;
        }
        byte piece = -1;
        switch (keyChar) {
            case '1' -> piece = PieceNotation.W_PAWN;
            case '2' -> piece = PieceNotation.W_KNIGHT;
            case '3' -> piece = PieceNotation.W_BISHOP;
            case '4' -> piece = PieceNotation.W_ROOK;
            case '5' -> piece = PieceNotation.W_QUEEN;
            case '6' -> piece = PieceNotation.B_PAWN;
            case '7' -> piece = PieceNotation.B_KNIGHT;
            case '8' -> piece = PieceNotation.B_BISHOP;
            case '9' -> piece = PieceNotation.B_ROOK;
            case '0' -> piece = PieceNotation.B_QUEEN;
            default -> {
                return;
            }
        }
        Position newPosition = position.copy();
        newPosition.set(piece, selected.x, selected.y);
        setPosition(newPosition);
        selected.setSelected(false);
        firePositionUpdated();
    }

    private void handleLeftArrow() {
        if (historyNum >= 1) {
            historyNum--;
            position = history.get(historyNum);
            firePositionUpdated();
        }
    }

    private void handleRightArrow() {
        if (historyNum + 1 < history.size()) {
            historyNum++;
            position = history.get(historyNum);
            firePositionUpdated();
        }
    }

    private void firePositionUpdated() {
        updateUI();
        printPosition();
    }

    public void close() {
        this.frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
    }

    public void center() {
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
        int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
        frame.setLocation(x, y);
    }

    private void setAutoEval(boolean autoEval) {
        this.autoEval = autoEval;
        if (this.autoEval) {
            this.pieceEvaluation = autoEvaluate();
        } else {
            this.pieceEvaluation = new int[64];
        }
        this.updateUI();
    }

    private void setEvaluation(String evalString) {
        this.autoEval = false;
        this.pieceEvaluation = Arrays.stream(evalString.split(","))
                .mapToInt(Integer::parseInt)
                .toArray();
        if (this.pieceEvaluation.length != 64) {
            throw new ChessException("Require 64 comma separated evaluation values, but got " + this.pieceEvaluation.length);
        }
        this.updateUI();
    }

    public static class Square extends JComponent {

        private final int x;
        private final int y;

        private Piece piece;

        private int evaluation;

        private boolean selected;

        private boolean dot;

        public Square(int x, int y) {
            this.x = x;
            this.y = y;
            this.piece = null;
            this.evaluation = 0;
            this.selected = false;
            this.dot = false;
        }

        public void setPiece(Piece piece) {
            this.piece = piece;
        }

        public void setEvaluation(int evaluation) {
            this.evaluation = evaluation;
        }

        public void setSelected(boolean selected) {
            this.selected = selected;
        }

        public boolean isSelected() {
            return this.selected;
        }

        public void setDot(boolean dot) {
            this.dot = dot;
        }

        public boolean isDot() {
            return dot;
        }

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            if (!(g instanceof Graphics2D g2d)) {
                return;
            }
            AffineTransform baseTransform = g2d.getTransform();
            if (!selected) {
                g2d.setColor((x + y) % 2 == 0 ? new Color(232, 235, 239) : new Color(125, 135, 150));
            } else {
                g2d.setColor(new Color(186, 202, 68));
            }
            g2d.fillRect(0, 0, getWidth(), getHeight());
            if (piece != null) {
                g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                        RenderingHints.VALUE_ANTIALIAS_ON);
                g2d.setRenderingHint(RenderingHints.KEY_RENDERING,
                        RenderingHints.VALUE_RENDER_QUALITY);

                // piece
                int fontSize = (int) (getWidth() / 1.3f);
                Font font = new Font("SansSerif", Font.PLAIN, fontSize);
                g2d.setFont(font);
                String symbol = CLIRenderer.SYMBOLS[piece.toByte()];
                GlyphVector glyphVector = font.createGlyphVector(g2d.getFontRenderContext(), symbol);
                Shape textShape = glyphVector.getOutline();
                g2d.setStroke(new BasicStroke(getWidth() / 25.0f));
                g2d.translate(getWidth() / 2 - (fontSize / 2), getHeight() / 2 + (fontSize / 3));

                g2d.setColor(piece.isWhite() ? Color.BLACK : Color.WHITE);
                g2d.draw(textShape);
                g2d.setColor(piece.isWhite() ? new Color(242, 242, 242) : Color.BLACK);
                g2d.drawString(symbol, 0, 0);

                // evaluation value
                if (evaluation != 0) {
                    fontSize = (int) (getWidth() / 4f);
                    font = new Font("SansSerif", Font.PLAIN, fontSize);
                    g2d.setFont(font);
                    g2d.setColor(Color.red);
                    g2d.setBackground(Color.BLACK);
                    g2d.drawString(String.valueOf(evaluation), -10, fontSize - 2);
                }
            }
            g2d.setTransform(baseTransform);
            if (dot) {
                g2d.setColor(new Color(1f, 0f, 0f, .3f));
                int width = getWidth() / 3;
                int height = getHeight() / 3;
                g2d.fillOval(getWidth() / 2 - width / 2, getHeight() / 2 - height / 2, width, height);
            }

        }

    }

    private static class MoveEntry {
        private final MovePath movePath;
        private final int evaluation;

        public MoveEntry(MovePath movePath, int evaluation) {
            this.movePath = movePath;
            this.evaluation = evaluation;
        }

        public MovePath getMovePath() {
            return movePath;
        }

        public int getEvaluation() {
            return evaluation;
        }

        @Override
        public String toString() {
            return "Ev (" + evaluation + "): " + movePath;
        }
    }

    public static void main(String[] args) {
        PositionEvaluatorUI ui = new PositionEvaluatorUI();
        try {
            ui.show().get();

            Position position = new Position();
            position.fen("rnbqkb1r/pp2pppp/3p1n2/2p5/4P3/2N2N2/PPPP1PPP/R1BQKB1R w KQkq - 2 4");
            ui.setPosition(position);

            InputStreamReader streamReader = new InputStreamReader(System.in);
            BufferedReader bufferedReader = new BufferedReader(streamReader);
            String line;
            do {
                line = bufferedReader.readLine();
                if (line == null) {
                    continue;
                }
                if (line.startsWith("set fen")) {
                    position.fen(line.substring(8));
                    ui.firePositionUpdated();
                } else if (line.equals("set auto eval false")) {
                    ui.setAutoEval(false);
                } else if (line.equals("set auto eval true")) {
                    ui.setAutoEval(true);
                } else if (line.startsWith("set eval")) {
                    ui.setEvaluation(line.substring(9));
                }
            } while (line != null && !line.equals("quit") && !line.equals("exit"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        ui.close();
    }

}
