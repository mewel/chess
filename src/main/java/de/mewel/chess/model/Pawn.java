package de.mewel.chess.model;

import de.mewel.chess.common.Move;
import de.mewel.chess.common.PieceUtil;
import de.mewel.chess.common.PositionUtil;

import java.util.ArrayList;
import java.util.List;

public class Pawn extends AbstractPiece {

    public Pawn(boolean white, int x, int y) {
        super(white, x, y);
    }

    public String getName() {
        return "pawn";
    }

    public short getValue() {
        return 1;
    }

    @Override
    public byte toByte() {
        return isWhite() ? PieceNotation.W_PAWN : PieceNotation.B_PAWN;
    }

    @Override
    public List<Move> moves(Position position) {
        List<Move> moves = new ArrayList<>();
        int x = x();
        int y = y();

        // moves
        int yOneOffset = isWhite() ? 1 : -1;
        if (position.isEmpty(x, y + yOneOffset)) {
            int yTwoOffset = isWhite() ? 2 : -2;
            if (y == (isWhite() ? 6 : 1)) {
                promote(position, moves, x, y + yOneOffset);
            } else {
                moves.add(position.move(x, y, x, y + yOneOffset));
                // pawn is on base position and can move two fields
                if (canMoveTwoFields(position)) {
                    PositionUtil.move(moves, position, x, y, x, y + yTwoOffset);
                }
            }
        }

        captures(position, moves);

        return moves;
    }

    @Override
    public List<Move> validMoves(Position position) {
        return moves(position);
    }

    @Override
    public List<Move> captures(Position position) {
        List<Move> captures = new ArrayList<>();
        captures(position, captures);
        return captures;
    }

    @Override
    public List<Move> quiescenceMoves(Position position) {
        List<Move> quiescenceMoves = new ArrayList<>();
        captures(position, quiescenceMoves);
        int targetY = y() + (isWhite() ? 1 : -1);
        if (targetY == (isWhite() ? 7 : 0) && position.isEmpty(x(), targetY)) {
            promote(position, quiescenceMoves, x(), targetY);
        }
        return quiescenceMoves;
    }

    @Override
    public List<Move> pinnedMoves(Position position, King king, Piece pinningPiece) {
        ArrayList<Move> moves = new ArrayList<>();
        if (king.y() == y()) {
            //horizontal pin
            return moves;
        }
        int xDistance = Math.abs(x() - pinningPiece.x());
        int yDistance = Math.abs(y() - pinningPiece.y());
        if ((xDistance != 0 && yDistance != 0)) {
            // diagonal pin
            if (xDistance == 1 && yDistance == 1 && y() + (isWhite() ? 1 : -1) == pinningPiece.y()) {
                if (pinningPiece.y() == (isWhite() ? 7 : 0)) {
                    promote(position, moves, pinningPiece.x(), pinningPiece.y());
                } else {
                    PositionUtil.capture(moves, position, x(), y(), pinningPiece.x(), pinningPiece.y());
                }
            }
            return moves;
        }
        // handle vertical pin
        int yOneOffset = isWhite() ? 1 : -1;
        if (position.isEmpty(x(), y() + yOneOffset)) {
            int yTwoOffset = isWhite() ? 2 : -2;
            moves.add(position.move(x(), y(), x(), y() + yOneOffset));
            // pawn is on base position and can move two fields
            if (canMoveTwoFields(position)) {
                PositionUtil.move(moves, position, x(), y(), x(), y() + yTwoOffset);
            }
        }
        return moves;
    }

    @Override
    public List<Move> inCheckMoves(Position position) {
        List<Move> moves = new ArrayList<>();
        List<Piece> checkingPieces = position.getCheckingPieces();
        if (checkingPieces.size() > 1) {
            return moves;
        }
        Piece checkingPiece = checkingPieces.get(0);
        byte piece = checkingPiece.toByte();
        boolean white = isWhite();
        // check captures
        inCheckCaptures(position, moves, checkingPiece, white);
        // intersects
        int target1Y = y() + (white ? 1 : -1);
        if (!position.isEmpty(x(), target1Y)) {
            return moves;
        }
        int target2Y = y() + (white ? 2 : -2);
        King king = PositionUtil.getKing(position, white);
        // diagonal intersects
        if ((PieceUtil.isBishop(piece) || PieceUtil.isQueen(piece)) && checkingPiece.x() != king.x() && checkingPiece.y() != king.y()) {
            boolean diagonal = king.x() + king.y() == checkingPiece.x() + checkingPiece.y();
            boolean target1LightSquare = position.isLightSquare(x(), target1Y);
            boolean target2LightSquare = !target1LightSquare;
            boolean checkingPieceLightSquare = position.isLightSquare(checkingPiece.x(), checkingPiece.y());
            if (target1LightSquare == checkingPieceLightSquare &&
                    (diagonal ? king.x() + king.y() == x() + target1Y : king.x() - king.y() == x() - target1Y) &&
                    PieceUtil.inBetween(x(), target1Y, king.x(), king.y(), checkingPiece.x(), checkingPiece.y())) {
                move(position, moves, x(), target1Y);
            } else if (target2LightSquare == checkingPieceLightSquare &&
                    (diagonal ? king.x() + king.y() == x() + target2Y : king.x() - king.y() == x() - target2Y) &&
                    PieceUtil.inBetween(x(), target2Y, king.x(), king.y(), checkingPiece.x(), checkingPiece.y())) {
                move(position, moves, x(), target2Y);
            }
        }
        // horizontal intersects
        if ((PieceUtil.isRook(piece) || PieceUtil.isQueen(piece)) && king.y() == checkingPiece.y() && PieceUtil.inBetween(x(), checkingPiece.x(), king.x())) {
            if (target1Y == king.y()) {
                move(position, moves, x(), target1Y);
            } else if (canMoveTwoFields(position) && target2Y == king.y()) {
                move(position, moves, x(), target2Y);
            }
        }
        return moves;
    }

    private void inCheckCaptures(Position position, List<Move> moves, Piece attackingPiece, boolean white) {
        if ((white && attackingPiece.y() - y() == 1) || (!white && y() - attackingPiece.y() == 1)) {
            if (attackingPiece.x() - x() == 1) {
                capture(position, moves, x() + 1, y() + (white ? 1 : -1));
            } else if (x() - attackingPiece.x() == 1) {
                capture(position, moves, x() - 1, y() + (white ? 1 : -1));
            }
        }
    }

    private void captures(Position position, List<Move> captures) {
        int x = x();
        int y = y();

        // default
        int toY = isWhite() ? y + 1 : y - 1;
        if (Position.valid(x - 1, toY)) {
            capture(position, captures, x - 1, toY);
        }
        if (Position.valid(x + 1, toY)) {
            capture(position, captures, x + 1, toY);
        }

        // en passant
        if (PositionUtil.checkEnPassant(position, x, y)) {
            captures.add(position.enPassant(x, y, position.getTwoSquarePawn()));
        }
    }

    private void move(Position position, List<Move> moves, int toX, int toY) {
        if (toY == (isWhite() ? 7 : 0)) {
            promote(position, moves, toX, toY);
        } else {
            moves.add(position.move(x(), y(), toX, toY));
        }
    }

    private void capture(Position position, List<Move> captures, int toX, int toY) {
        byte piece = position.get(toX, toY);
        if (piece == PieceNotation.EMPTY || !PieceUtil.isOpponent(position.isWhite(), piece)) {
            return;
        }
        if (toY == (isWhite() ? 7 : 0)) {
            promote(position, captures, toX, toY);
        } else {
            captures.add(position.capture(x(), y(), toX, toY));
        }
    }

    private void promote(Position position, List<Move> moves, int toX, int toY) {
        byte queen = isWhite() ? PieceNotation.W_QUEEN : PieceNotation.B_QUEEN;
        byte knight = isWhite() ? PieceNotation.W_KNIGHT : PieceNotation.B_KNIGHT;
        moves.add(position.promote(x(), y(), toX, toY, queen));
        moves.add(position.promote(x(), y(), toX, toY, knight));
    }

    public byte preventsCastling(Position position) {
        return PositionUtil.checkCastlePawnOrKing(position, x(), y());
    }

    private boolean canMoveTwoFields(Position position) {
        return y() == (isWhite() ? 1 : 6) && position.isEmpty(x(), y() + (isWhite() ? 2 : -2));
    }

}
