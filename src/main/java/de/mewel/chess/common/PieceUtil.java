package de.mewel.chess.common;

import de.mewel.chess.model.*;

import java.util.HashMap;
import java.util.Map;

import static de.mewel.chess.model.PieceNotation.*;

/**
 * @author mewel
 */
public abstract class PieceUtil {

    private static final Map<Character, Byte> FEN_MAP;

    static {
        FEN_MAP = new HashMap<>(12);
        FEN_MAP.put('k', B_KING);
        FEN_MAP.put('K', W_KING);
        FEN_MAP.put('q', B_QUEEN);
        FEN_MAP.put('Q', W_QUEEN);
        FEN_MAP.put('r', B_ROOK);
        FEN_MAP.put('R', W_ROOK);
        FEN_MAP.put('b', B_BISHOP);
        FEN_MAP.put('B', W_BISHOP);
        FEN_MAP.put('n', B_KNIGHT);
        FEN_MAP.put('N', W_KNIGHT);
        FEN_MAP.put('p', B_PAWN);
        FEN_MAP.put('P', W_PAWN);
    }

    /**
     * Gets the piece from the given notation. Returns null if the
     * piece cannot be resolved.
     *
     * @param piece notation
     * @return the piece object or null
     */
    public static Piece fromByte(byte piece, int x, int y) {
        if (piece == EMPTY) {
            return null;
        }
        return switch (piece) {
            case W_KING -> new King(true, x, y);
            case W_QUEEN -> new Queen(true, x, y);
            case W_ROOK -> new Rook(true, x, y);
            case W_BISHOP -> new Bishop(true, x, y);
            case W_KNIGHT -> new Knight(true, x, y);
            case W_PAWN -> new Pawn(true, x, y);
            case B_KING -> new King(false, x, y);
            case B_QUEEN -> new Queen(false, x, y);
            case B_ROOK -> new Rook(false, x, y);
            case B_BISHOP -> new Bishop(false, x, y);
            case B_KNIGHT -> new Knight(false, x, y);
            case B_PAWN -> new Pawn(false, x, y);
            default -> throw new ChessException("unknown piece byte " + piece);
        };
    }

    public static String toFEN(byte pieceByte) {
        return FEN_MAP.entrySet()
                .stream().filter(e -> e.getValue() == pieceByte)
                .map(Map.Entry::getKey)
                .findFirst()
                .map(Object::toString)
                .orElse(null);
    }

    public static Piece fromFEN(char fenChar, int x, int y) {
        return fromByte(FEN_MAP.get(fenChar), x, y);
    }

    public static boolean isWhite(byte piece) {
        return piece >= W_KING && piece <= W_PAWN;
    }

    /**
     * Checks if the piece is an opponent to the color.
     *
     * @param white is it's whites turn
     * @param piece the piece to check
     * @return true if an opponent is on this field
     */
    public static boolean isOpponent(boolean white, byte piece) {
        if (piece == EMPTY) {
            return false;
        }
        return white == (piece > PieceNotation.W_PAWN);
    }

    public static boolean isKing(byte piece) {
        return piece == W_KING || piece == B_KING;
    }

    public static boolean isPawn(byte piece) {
        return piece == W_PAWN || piece == B_PAWN;
    }

    public static boolean isKnight(byte piece) {
        return piece == W_KNIGHT || piece == B_KNIGHT;
    }

    public static boolean isBishop(byte piece) {
        return piece == W_BISHOP || piece == B_BISHOP;
    }

    public static boolean isRook(byte piece) {
        return piece == W_ROOK || piece == B_ROOK;
    }

    public static boolean isQueen(byte piece) {
        return piece == W_QUEEN || piece == B_QUEEN;
    }

    public static boolean inBetween(int inBetween, int p1, int p2) {
        return inBetween >= 0 && inBetween <= 7 && ((inBetween > p1 && inBetween < p2) || (inBetween > p2 && inBetween < p1));
    }

    public static boolean inBetween(int inBetweenX, int inBetweenY, int p1X, int p1Y, int p2X, int p2Y) {
        return inBetween(inBetweenX, p1X, p2X) && inBetween(inBetweenY, p1Y, p2Y);
    }

    //
    // /**
    // * Sets the position for the given piece.
    // * @param p
    // * @param x
    // * a-h
    // * @param y
    // * 1-8
    // */
    // public void set(Piece p, String x, String y) {
    // try {
    // set(p, xToNumeric(x), yToNumeric(y));
    // } catch (Exception exc) {
    // throw new IllegalArgumentException("Invalid board position x: " + x + " y: " + y + " for piece " + p.getName());
    // }
    // }
    //
    // public static int xToNumeric(String x) {
    // return x.charAt(0) - 97;
    // }
    //
    // public static int yToNumeric(String y) {
    // return Integer.valueOf(y).intValue() - 1;
    // }
    //
    // public static String xToAlgebraicNotation(int x) {
    // return String.valueOf(Character.getNumericValue(x + 97));
    // }
    //
    // public static String yToAlgebraicNotation(int y) {
    // return String.valueOf(y + 1);
    // }
    //
    // public static String toAlgebraicNotation(int x, int y) {
    // return xToAlgebraicNotation(x) + yToAlgebraicNotation(y);
    // }

}
