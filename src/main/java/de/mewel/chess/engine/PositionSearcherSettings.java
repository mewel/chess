package de.mewel.chess.engine;


/**
 * 0 = no variance -> choose best move
 */
public record PositionSearcherSettings(int maxFullNode, int maxQuiescenceNode, int variance) {

}
