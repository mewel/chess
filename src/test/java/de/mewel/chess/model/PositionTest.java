package de.mewel.chess.model;

import de.mewel.chess.common.MoveExecutor;
import de.mewel.chess.common.PositionUtil;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PositionTest {

    @Test
    public void moves() {
        MoveExecutor moveExecutor = new MoveExecutor();
        Position p = PositionUtil.base();

        // move pawn one square
        moveExecutor.forward(p, p.move(0, 1, 0, 2));
        assertEquals("there should be no pawn after moving", PieceNotation.EMPTY, p.get(0, 1));
        assertEquals("the white pawn was moved one square", PieceNotation.W_PAWN, p.get(0, 2));

        // move pawn two squares
        moveExecutor.forward(p, p.move(1, 1, 1, 3));
        assertEquals(PieceNotation.EMPTY, p.get(1, 1));
        assertEquals(PieceNotation.EMPTY, p.get(1, 2));
        assertEquals(PieceNotation.W_PAWN, p.get(1, 3));
        assertEquals(PieceNotation.W_QUEEN, p.getTwoSquarePawn());

        // castle
        p = PositionUtil.base();
        assertEquals(3, p.getCastleWhite());

        p.set(PieceNotation.EMPTY, 0, 1);
        moveExecutor.forward(p, p.move(0, 0, 0, 3));
        assertEquals(1, p.getCastleWhite());

        p.set(PieceNotation.EMPTY, 7, 1);
        moveExecutor.forward(p, p.move(7, 0, 7, 3));
        assertEquals(0, p.getCastleWhite());

        p = PositionUtil.base();
        p.set(PieceNotation.EMPTY, 4, 1);
        moveExecutor.forward(p, p.move(4, 0, 4, 1));
        assertEquals(0, p.getCastleWhite());
    }

    @Test
    public void promote() {
        MoveExecutor moveExecutor = new MoveExecutor();
        Position p = PositionUtil.empty();
        p.set(PieceNotation.W_PAWN, 0, 6);
        moveExecutor.forward(p, p.promote(0, 6, 0, 7, (byte) PieceNotation.W_QUEEN));
        assertEquals(PieceNotation.W_QUEEN, p.get(0, 7));
        assertEquals(PieceNotation.EMPTY, p.get(0, 6));
    }

    @Test
    public void enPassant() {
        MoveExecutor moveExecutor = new MoveExecutor();
        Position p1 = PositionUtil.empty();
        p1.set(PieceNotation.W_PAWN, 0, 4);
        p1.set(PieceNotation.B_PAWN, 1, 4);
        p1.setTwoSquarePawn(1);
        moveExecutor.forward(p1, p1.enPassant(0, 4, 1));
        assertEquals(PieceNotation.W_PAWN, p1.get(1, 5));
        assertEquals(PieceNotation.EMPTY, p1.get(1, 4));

        Position p2 = PositionUtil.empty()
                .set(PieceNotation.W_PAWN, 0, 1)
                .set(PieceNotation.W_PAWN, 1, 2)
                .set(PieceNotation.B_PAWN, 1, 3);
        moveExecutor.forward(p2, p2.move("a2a4"));
        moveExecutor.forward(p2, p2.move("b4a3"));
        assertEquals(1, PositionUtil.list(p2).size());
    }

    @Test
    public void castle() {
        MoveExecutor moveExecutor = new MoveExecutor();
        Position p = PositionUtil.base();
        removeBetweenKingAndRook(p);
        moveExecutor.forward(p, p.castle(true));
        assertEquals(PieceNotation.W_KING, p.get(6, 0));
        assertEquals(PieceNotation.W_ROOK, p.get(5, 0));

        p = PositionUtil.base();
        removeBetweenKingAndRook(p);
        moveExecutor.forward(p, p.castle(false));
        assertEquals(PieceNotation.W_KING, p.get(2, 0));
        assertEquals(PieceNotation.W_ROOK, p.get(3, 0));
    }

    private void removeBetweenKingAndRook(Position p) {
        p.set(PieceNotation.EMPTY, 1, 0);
        p.set(PieceNotation.EMPTY, 2, 0);
        p.set(PieceNotation.EMPTY, 3, 0);
        p.set(PieceNotation.EMPTY, 5, 0);
        p.set(PieceNotation.EMPTY, 6, 0);
    }

}
