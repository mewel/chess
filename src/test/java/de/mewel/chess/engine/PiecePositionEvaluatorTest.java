package de.mewel.chess.engine;

import de.mewel.chess.common.Move;
import de.mewel.chess.common.MoveExecutor;
import de.mewel.chess.common.PositionUtil;
import de.mewel.chess.engine.evaluator.PiecePositionEvaluator;
import de.mewel.chess.engine.evaluator.PositionEvaluator;
import de.mewel.chess.model.Position;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class PiecePositionEvaluatorTest {

    @Test
    public void test() {
        MoveExecutor moveExecutor = new MoveExecutor();

        Position position = PositionUtil.base();
        PositionEvaluator analyzer = new PiecePositionEvaluator();

        Assert.assertEquals(0, analyzer.analyze(position));

        List<Move> moves = moveExecutor.forward(position, "e2e4");
        int e2e4 = analyzer.analyze(position);
        Assert.assertTrue(e2e4 > 0);
        moveExecutor.backward(position, moves.toArray(new Move[0]));

        moves = moveExecutor.forward(position, "b1c3");
        int b1c3 = analyzer.analyze(position);
        Assert.assertTrue(b1c3 > 0);
        moveExecutor.backward(position, moves.toArray(new Move[0]));

        Assert.assertTrue(e2e4 > b1c3);
    }

}
