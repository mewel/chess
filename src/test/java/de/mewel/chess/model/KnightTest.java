package de.mewel.chess.model;

import de.mewel.chess.common.PieceUtil;
import de.mewel.chess.common.PositionUtil;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static de.mewel.chess.model.PieceNotation.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class KnightTest {

    @Test
    public void moves() {
        Position p = PositionUtil.base();
        Knight whiteKnight = new Knight(true, 1, 0);

        // cannot move on base position
        assertEquals("Knight can jump to two squares from base position", 2, whiteKnight.moves(p).size());

        // remove pawn right of knight
        p.set(PieceNotation.EMPTY, 3, 1);
        assertEquals("Knight can jump to three squares when pawn is removed", 3, whiteKnight.moves(p).size());

        // place knight in center
        p.set(PieceNotation.W_KNIGHT, 4, 4);
        assertEquals("Knight can jump to six squares and capture two pieces", 8, whiteKnight.square(4, 4).moves(p).size());
    }

    @Test
    public void inCheckMoves() {
        Position p = PositionUtil.empty();

        Knight knight = set(p, true, 3, 4, 0, 2, B_QUEEN, 4, 2);
        assertTrue(knight.inCheckMoves(p).contains(p.move("d5xe3")));
        assertTrue(knight.inCheckMoves(p).contains(p.move("d5c3")));

        knight = set(p, true, 3, 4, 0, 3, B_QUEEN, 4, 3);
        assertEquals(1, knight.inCheckMoves(p).size());
        assertTrue(knight.inCheckMoves(p).contains(p.move("d5b4")));

        knight = set(p, true, 3, 4, 0, 5, B_QUEEN, 4, 5);
        assertEquals(1, knight.inCheckMoves(p).size());
        assertTrue(knight.inCheckMoves(p).contains(p.move("d5b6")));

        knight = set(p, true, 3, 4, 0, 5, B_QUEEN, 7, 5);
        assertEquals(2, knight.inCheckMoves(p).size());
        assertTrue(knight.inCheckMoves(p).contains(p.move("d5b6")));
        assertTrue(knight.inCheckMoves(p).contains(p.move("d5f6")));

        knight = set(p, true, 3, 4, 0, 6, B_QUEEN, 4, 2);
        assertEquals(2, knight.inCheckMoves(p).size());
        assertTrue(knight.inCheckMoves(p).contains(p.move("d5xe3")));
        assertTrue(knight.inCheckMoves(p).contains(p.move("d5b6")));

        knight = set(p, true, 3, 4, 0, 6, B_QUEEN, 6, 0);
        assertTrue(knight.inCheckMoves(p).contains(p.move("d5e3")));
        assertTrue(knight.inCheckMoves(p).contains(p.move("d5b6")));

        knight = set(p, true, 6, 1, 0, 6, B_QUEEN, 6, 0);
        assertEquals(1, knight.inCheckMoves(p).size());
        assertTrue(knight.inCheckMoves(p).contains(p.move("g2e3")));

        knight = set(p, true, 5, 0, 0, 6, B_QUEEN, 6, 0);
        assertEquals(1, knight.inCheckMoves(p).size());
        assertTrue(knight.inCheckMoves(p).contains(p.move("f1e3")));

        knight = set(p, true, 4, 0, 0, 6, B_QUEEN, 6, 0);
        assertEquals(0, knight.inCheckMoves(p).size());

        knight = set(p, true, 3, 0, 0, 6, B_QUEEN, 6, 0);
        assertEquals(2, knight.inCheckMoves(p).size());
        assertTrue(knight.inCheckMoves(p).contains(p.move("d1f2")));
        assertTrue(knight.inCheckMoves(p).contains(p.move("d1e3")));

        knight = set(p, true, 2, 0, 0, 6, B_QUEEN, 6, 0);
        assertEquals(0, knight.inCheckMoves(p).size());

        knight = set(p, true, 1, 0, 0, 0, B_QUEEN, 7, 7);
        assertEquals(1, knight.inCheckMoves(p).size());
        assertTrue(knight.inCheckMoves(p).contains(p.move("b1c3")));

        knight = set(p, true, 4, 3, 0, 0, B_QUEEN, 7, 7);
        assertEquals(2, knight.inCheckMoves(p).size());
        assertTrue(knight.inCheckMoves(p).contains(p.move("e4c3")));
        assertTrue(knight.inCheckMoves(p).contains(p.move("e4f6")));

        knight = set(p, true, 5, 2, 0, 0, B_QUEEN, 7, 7);
        assertEquals(2, knight.inCheckMoves(p).size());
        assertTrue(knight.inCheckMoves(p).contains(p.move("f3e5")));
        assertTrue(knight.inCheckMoves(p).contains(p.move("f3d4")));

        knight = set(p, true, 2, 3, 0, 0, B_QUEEN, 7, 7);
        assertEquals(2, knight.inCheckMoves(p).size());
        assertTrue(knight.inCheckMoves(p).contains(p.move("c4b2")));
        assertTrue(knight.inCheckMoves(p).contains(p.move("c4e5")));

        knight = set(p, true, 2, 5, 0, 0, B_QUEEN, 7, 7);
        assertEquals(2, knight.inCheckMoves(p).size());
        assertTrue(knight.inCheckMoves(p).contains(p.move("c6d4")));
        assertTrue(knight.inCheckMoves(p).contains(p.move("c6e5")));

        knight = set(p, true, 2, 5, 2, 6, B_QUEEN, 1, 5);
        assertEquals(0, knight.inCheckMoves(p).size());

        knight = set(p, true, 2, 5, 2, 6, B_QUEEN, 3, 5);
        assertEquals(0, knight.inCheckMoves(p).size());

        knight = set(p, true, 2, 5, 2, 6, B_QUEEN, 4, 4);
        assertEquals(1, knight.inCheckMoves(p).size());
        assertTrue(knight.inCheckMoves(p).contains(p.move("c6xe5")));
    }

    private Knight set(Position p, boolean white, int knightX, int knightY, int kingX, int kingY, byte attPiece, int attX, int attY) {
        p.clear();
        setCheckingPiece(p, attPiece, attX, attY);
        p.set(white ? W_KING : B_KING, kingX, kingY);
        return setKnight(p, white, knightX, knightY);
    }

    private Knight setKnight(Position p, boolean white, int x, int y) {
        p.set(white ? W_KNIGHT : B_KNIGHT, x, y);
        return new Knight(white, x, y);
    }

    private void setCheckingPiece(Position p, byte piece, int x, int y) {
        p.set(piece, x, y);
        p.setCheckingPieces(Arrays.asList(Objects.requireNonNull(PieceUtil.fromByte(piece, x, y))));
    }

}
