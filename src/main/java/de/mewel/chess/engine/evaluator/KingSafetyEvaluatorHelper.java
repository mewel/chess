package de.mewel.chess.engine.evaluator;

import de.mewel.chess.model.King;
import de.mewel.chess.model.PieceNotation;
import de.mewel.chess.model.Position;

public class KingSafetyEvaluatorHelper {

    // short
    private static final int[][] K_6_0 = {{1, 1, 1, 15}, {1, 1, 2, 30}, {1, 1, 3, 20}, {1, 2, 1, 30}, {1, 2, 2, 15}, {1, 2, 3, 20}};

    private static final int[][] K_7_0 = {{1, 1, 20}, {1, 2, 30}};

    // long
    private static final int[][] K_1_0 = {{1, 1, 1, 15}, {2, 1, 1, 30}, {3, 1, 1, 20}, {1, 2, 1, 30}, {2, 2, 1, 15}, {3, 2, 1, 20}};

    private static final int[][] K_0_0 = {{1, 1, 20}, {2, 1, 30}};

    public static int evaluate(King king, Position position) {
        int x = king.x();
        int y = king.y();
        boolean white = king.isWhite();

        boolean firstOrSecondRank = white ? (y == 0 || y == 1) : (y == 6 || y == 7);
        if (!firstOrSecondRank) {
            return 0;
        }
        byte castleRight = (white ? position.getCastleWhite() : position.getCastleBlack());
        boolean kingCanCastleShort = castleRight == 1 || castleRight == 3;
        boolean kingCanCastleLong = castleRight == 2 || castleRight == 3;
        if (kingCanCastleShort || kingCanCastleLong) {
            return evaluateCastleIntegrity(king, position, kingCanCastleShort, kingCanCastleLong);
        }
        boolean kingIsShort = (x == 6 || x == 7);
        boolean kingIsLong = (x == 0 || x == 1);
        if (!kingIsShort && !kingIsLong) {
            return 0;
        }
        // SHORT
        // 6 0
        if (x == 6 && (white ? y == 0 : y == 7)) {
            return evaluateShort(king, position, K_6_0);
        }
        // 7 0
        if (x == 7 && (white ? y == 0 : y == 7)) {
            return evaluateShort(king, position, K_7_0);
        }
        // LONG
        // 1 0
        if (x == 1 && (white ? y == 0 : y == 7)) {
            return evaluateLong(king, position, K_1_0);
        }
        // 0 0
        if (x == 0 && (white ? y == 0 : y == 7)) {
            return evaluateLong(king, position, K_0_0);
        }

        return 0;
    }

    private static int evaluateCastleIntegrity(King king, Position position, boolean canCastleShort, boolean canCastleLong) {
        int value = 0;
        if (canCastleShort) {
            value += evaluateShort(king, position, K_6_0) / 2;
        }
        if (canCastleLong) {
            value += evaluateLong(king, position, K_1_0) / 2;
        }
        return value;
    }

    private static int evaluateShort(King king, Position position, int[][] pawnFormations) {
        boolean white = king.isWhite();
        byte pawn = white ? PieceNotation.W_PAWN : PieceNotation.B_PAWN;
        for (int[] pawnFormation : pawnFormations) {
            int baseX = 9 - pawnFormation.length;
            boolean found = true;
            for (int i = 0; i < pawnFormation.length - 1; i++) {
                int pawnX = baseX + i;
                int pawnY = white ? pawnFormation[i] : 7 - pawnFormation[i];
                if (position.get(pawnX, pawnY) != pawn) {
                    found = false;
                    break;
                }
            }
            if (found) {
                return pawnFormation[pawnFormation.length - 1];
            }
        }
        return 0;
    }

    private static int evaluateLong(King king, Position position, int[][] pawnFormations) {
        boolean white = king.isWhite();
        byte pawn = white ? PieceNotation.W_PAWN : PieceNotation.B_PAWN;
        for (int[] pawnFormation : pawnFormations) {
            boolean found = true;
            for (int i = 0; i < pawnFormation.length - 1; i++) {
                int pawnY = white ? pawnFormation[i] : 7 - pawnFormation[i];
                if (position.get(i, pawnY) != pawn) {
                    found = false;
                    break;
                }
            }
            if (found) {
                return pawnFormation[pawnFormation.length - 1];
            }
        }
        return 0;
    }

}
