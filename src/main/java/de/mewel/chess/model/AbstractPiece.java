package de.mewel.chess.model;


import de.mewel.chess.common.Move;

import java.util.List;
import java.util.Objects;

public abstract class AbstractPiece implements Piece {

    private int x, y;

    private final boolean white;

    public AbstractPiece(boolean white, int x, int y) {
        this.white = white;
        this.x = x;
        this.y = y;
    }

    public boolean isWhite() {
        return white;
    }

    @Override
    public Piece square(int x, int y) {
        this.x = x;
        this.y = y;
        return this;
    }

    @Override
    public int x() {
        return x;
    }

    @Override
    public int y() {
        return y;
    }

    @Override
    public List<Move> quiescenceMoves(Position position) {
        return captures(position);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbstractPiece that = (AbstractPiece) o;
        return x == that.x &&
                y == that.y &&
                white == that.white;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y, white);
    }

}
