package de.mewel.chess.model;

import de.mewel.chess.common.Move;
import de.mewel.chess.common.PieceUtil;
import de.mewel.chess.common.PositionUtil;

import java.util.ArrayList;
import java.util.List;

public class Knight extends AbstractPiece {

    public final static int[][] KNIGHT_POSITIONS;

    static {
        KNIGHT_POSITIONS = new int[][]{{-2, -1}, {-2, 1}, {-1, -2}, {-1, 2}, {2, -1}, {2, 1}, {1, -2}, {1, 2}};
    }

    public Knight(boolean white, int x, int y) {
        super(white, x, y);
    }

    public String getName() {
        return "knight";
    }

    public short getValue() {
        return 3;
    }

    @Override
    public byte toByte() {
        return isWhite() ? PieceNotation.W_KNIGHT : PieceNotation.B_KNIGHT;
    }

    @Override
    public List<Move> moves(Position position) {
        return moves(position, true);
    }

    @Override
    public List<Move> validMoves(Position position) {
        return moves(position);
    }

    @Override
    public List<Move> captures(Position position) {
        return moves(position, false);
    }

    @Override
    public List<Move> quiescenceMoves(Position position) {
        return captures(position);
    }

    @Override
    public List<Move> pinnedMoves(Position position, King king, Piece pinningPiece) {
        return new ArrayList<>(); // we can't move the knight
    }

    @Override
    public List<Move> inCheckMoves(Position position) {
        List<Move> moves = new ArrayList<>();
        List<Piece> checkingPieces = position.getCheckingPieces();
        if (checkingPieces.size() > 1) {
            return moves;
        }
        // capture
        Piece checkingPiece = checkingPieces.get(0);
        int xDiff = checkingPiece.x() - x();
        int yDiff = checkingPiece.y() - y();
        int xDiffAbs = Math.abs(xDiff);
        int yDiffAbs = Math.abs(yDiff);
        if (xDiffAbs == 2 && yDiffAbs == 1 || xDiffAbs == 1 && yDiffAbs == 2) {
            PositionUtil.capture(moves, position, x(), y(), checkingPiece.x(), checkingPiece.y());
        }
        King king = PositionUtil.getKing(position, position.isWhite());
        if (checkingPiece.y() == king.y() && (yDiffAbs == 1 || yDiffAbs == 2)) {
            // intersect horizontal
            int targetY = y() + yDiff;
            if (targetY == checkingPiece.y()) {
                if (yDiffAbs == 1) {
                    inCheckMove(position, moves, checkingPiece, king, true, x() - 2, targetY);
                    inCheckMove(position, moves, checkingPiece, king, true, x() + 2, targetY);
                } else {
                    inCheckMove(position, moves, checkingPiece, king, true, x() - 1, targetY);
                    inCheckMove(position, moves, checkingPiece, king, true, x() + 1, targetY);
                }
            }
        } else if (checkingPiece.x() == king.x() && (xDiffAbs == 1 || xDiffAbs == 2)) {
            // intersect vertical
            int targetX = x() + xDiff;
            if (targetX == checkingPiece.x()) {
                if (xDiffAbs == 1) {
                    inCheckMove(position, moves, checkingPiece, king, false, targetX, y() - 2);
                    inCheckMove(position, moves, checkingPiece, king, false, targetX, y() + 2);
                } else {
                    inCheckMove(position, moves, checkingPiece, king, false, targetX, y() - 1);
                    inCheckMove(position, moves, checkingPiece, king, false, targetX, y() + 1);
                }
            }
        } else {
            // diagonal
            byte checkingPieceByte = checkingPiece.toByte();
            if (PieceUtil.isQueen(checkingPieceByte) || PieceUtil.isBishop(checkingPieceByte) &&
                    checkingPiece.x() != king.x() && checkingPiece.y() != king.y()) {
                boolean lightSquare = position.isLightSquare(x(), y());
                boolean checkingPieceLightSquare = position.isLightSquare(checkingPiece.x(), checkingPiece.y());
                if (checkingPieceLightSquare != lightSquare) {
                    boolean checkingDirection = checkingPiece.x() + checkingPiece.y() != king.x() + king.y();
                    int distance = checkingDirection ? (king.y() - y()) + (x() - king.x()) : (king.y() - y()) - (x() - king.x());
                    if (distance == -3 || distance == 3 || distance == -1 || distance == 1) {
                        int offset = ((distance == -3 || distance == 3) ^ distance < 0) ? 1 : -1;
                        int targetX1 = x() + ((distance < 0 ^ checkingDirection) ? -2 : 2);
                        int targetY1 = y() + offset;
                        int targetX2 = x() + (checkingDirection ? -offset : offset);
                        int targetY2 = y() + ((distance < 0) ? -2 : 2);
                        inCheckMove(position, moves, king, checkingPiece, targetX1, targetY1, checkingDirection);
                        inCheckMove(position, moves, king, checkingPiece, targetX2, targetY2, checkingDirection);
                    }
                }
            }
        }

        return moves;
    }

    private void inCheckMove(Position position, List<Move> moves, King king, Piece checkingPiece, int targetX, int targetY, boolean checkingDirection) {
        if (targetX >= 0 && targetX <= 7 && targetY >= 0 && targetY <= 7 &&
                position.isEmpty(targetX, targetY) &&
                PieceUtil.inBetween(targetX, targetY, king.x(), king.y(), checkingPiece.x(), checkingPiece.y()) &&
                ((checkingDirection && (targetX - targetY == king.x() - king.y())) || (!checkingDirection && (targetX + targetY == king.x() + king.y())))) {
            moves.add(position.move(x(), y(), targetX, targetY));
        }
    }

    private void inCheckMove(Position position, List<Move> moves, Piece checkingPiece, King king, boolean checkX, int targetX, int targetY) {
        if (checkX ? PieceUtil.inBetween(targetX, king.x(), checkingPiece.x()) : PieceUtil.inBetween(targetY, king.y(), checkingPiece.y())) {
            moves.add(position.move(x(), y(), targetX, targetY));
        }
    }

    @Override
    public byte preventsCastling(Position position) {
        boolean white = position.isWhite();
        if ((white && y() <= 4) || (!white && y() >= 3)) {
            return 0;
        }
        byte castle = 0;
        for (int[] knightPosition : KNIGHT_POSITIONS) {
            int toX = x() + knightPosition[0];
            int toY = y() + knightPosition[1];
            if (((white && toY == 7) || (!white && toY == 0)) && toX != 0 || toX != 7) {
                castle |= toX == 4 ? 3 : (toX < 4 ? 2 : 1);
                if (castle == 3) {
                    break;
                }
            }
        }
        return castle;
    }

    private List<Move> moves(Position position, boolean moveOrCapture) {
        List<Move> moves = new ArrayList<>();
        for (int[] knightPosition : KNIGHT_POSITIONS) {
            int toX = x() + knightPosition[0];
            int toY = y() + knightPosition[1];
            if (Position.valid(toX, toY)) {
                if (moveOrCapture) {
                    PositionUtil.moveOrCapture(moves, position, x(), y(), toX, toY);
                } else {
                    PositionUtil.capture(moves, position, x(), y(), toX, toY);
                }
            }
        }
        return moves;
    }

}
